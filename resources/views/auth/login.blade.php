@extends('layouts.app')
@push('styles')
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
    
    <script type="text/javascript">
        var widgetId1;
        var verifyCallback = function(response) {
            $('#recaptcha-token').val(response);
      };
  var onloadCallback = function() {
    {{--  alert("grecaptcha is ready!");  --}}
    grecaptcha.render('recaptcha', {
          'sitekey' : '{!! env('RECAPTCHA_KEY') !!}',
          'callback' : verifyCallback,
        });
       
  };
</script>
@endpush
@section('content')
<section class="section bg-light-grey login">
<div class="container h-100">
    <div class="row align-items-center h-100">
        <div class="col-md-5 mx-auto">
            <div class="card card-default">
                <div class="card-header">Masuk ke IOC</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-md-12">
                            <label for="email" class=" text-md-right">Alamat Email</label>
                            
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                            <label for="password" class="text-md-right">Kata Sandi</label>

                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Ingat saya
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <a class="dark-color accent-color " href="{{ route('password.request') }}">
                                    Lupa Kata Sandi?
                                </a>
                            </div>
                        </div>
                           @if ($errors->has('recaptcha'))
                                    <span class="invalid-feedback" style="display: block">
                                        <strong>Recaptcha Tidak Valid</strong>
                                    </span>
                                @endif
                        
                        <div class="g-recaptcha" id="recaptcha" data-sitekey="6LdX820UAAAAANtsMsf9pYIU2jJ3of9xd2IIYzTZ" name="recaptcha"></div>
                        <br>

                        <input type="hidden" class={{ $errors->has('recaptcha') ? ' is-invalid' : '' }} id="recaptcha-token" name="recaptcha" value="">
                       
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-8">
                                <button type="submit" class="btn btn-block btn-small btn-accent">
                                    Masuk
                                </button>
                            </div>
                        </div>
                        <div class="form-group row mt-1">
                            <div class="col-md-12">
                                <a class="dark-color dark-color font-italic" href="{{ route('register') }}" style="text-decoration: underline;">Belum memiliki akun?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    
@endpush