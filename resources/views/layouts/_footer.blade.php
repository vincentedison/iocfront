<footer class="footer">
	<section class="container footer-in">
		<div class="row">
			<div class="col-md-4">
				<h5 class="sub-title">Tentang IOC</h5>
				<p class="content">Dikenal dengan lingkungan hijau dan semangat edukasinya, IOC berkomitmen untuk memberikan pengalaman menyenangkan bagi Anda dalam mempererat hubungan dengan alam dan grup Anda. </p>
				<p class="content mb-0"><span class="accent-color">Telp:</span> 0888 0155 5921</p>
				<p class="content mb-0" style="margin-left:35px"> 0888 0155 5912</p>
				<p class="content mb-0"><span class="accent-color">Email:</span> utc_trawas@yahoo.com</p>
				<p class="content footerem"> utc@ubaya.ac.id</p>
				<p class="content"><span class="accent-color">Alamat:</span> Jl. Udayana, Trawas, Jawa Timur</p>
			</div>
			<div class="col-md-4">
				<h5 class="sub-title">Artikel</h5>
				<ul class="content list-unstyled">                          
    				@foreach($latest as $latest_news)
					<li>
	                	<a class="accent-color" href="{{route('news.show',['id'=>$latest_news->slug]) }}" title="{{ $latest_news->title }}">{{ $latest_news->title}}</a>
	                </li>					
					@endforeach
				</ul>
			</div>
			<div class="col-md-4">
				<h5 class="sub-title">Info Terbaru</h5>

				<p class="content">Jangan lewatkan penawaran menarik dari IOC dengan mendaftarkan email Anda pada kolom di bawah ini. </p>
				<div class="content">
					<!-- Begin MailChimp Signup Form -->
					<div id="mc_embed_signup">
						<form action="https://ubaya.us18.list-manage.com/subscribe/post?u=032071854c6a9a9766a9d2908&amp;id=3a7d791e8e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
							<div id="mc_embed_signup_scroll">

								<div class="mc-field-group row justify-content-between">
									<div class=" col-sm-7 col">
									<input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="Alamat Email">
									</div>
									<div class=" col-sm-5 col">
									<input type="submit" value="Kirim" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-accent btn-block" style="text-transform: capitalize;font-size: 14px;">
									</div>
								</div>

								<div id="mce-responses" class="clear">
									<div class="response" id="mce-error-response" style="display:none"></div>
									<div class="response" id="mce-success-response" style="display:none"></div>
								</div>    
								<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_032071854c6a9a9766a9d2908_3a7d791e8e" tabindex="-1" value=""></div>
								
							</div>
						</form>
					</div>

					<!--End mc_embed_signup-->
				</div>
			</div>
		</div>		
	</section>
	<section class="footbot">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="footer-navi">
						<span class="accent-color">Integrated Outdoor Campus </span> © Copyright 2018	
					</div>
				</div>
				<div class="col-md-6">
					<div class="footer-navi floatright">
						Developed by Luxodev	
					</div>
				</div>
			</div>
		</div>
	</section>
</footer>


<!-- Jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{--  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>  --}}
<!-- Bootstrap-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b836495afc2c34e96e7ec90/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

@stack('scripts')