<script>
    $('.unitformfilter, .personformfilter').on('keyup change', function(e){
        var unit=isNaN(parseInt($('.unitformfilter').val())) ? 1: parseInt($('.unitformfilter').val());
        var person=isNaN(parseInt($('.personformfilter').val())) ? 1: parseInt($('.personformfilter').val());
        if(unit>person){
            $('.searchbutton').attr("disabled", true);
        }
        else{
            $('.searchbutton').removeAttr("disabled");
        }
    });
</script>