<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  @include("layouts._head")
</head>
<body >
  @include("layouts._navbar")
  @yield('content')
  @include('layouts._footer')
</body>
</html>
