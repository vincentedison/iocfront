@extends('layouts.app')
@push('styles')
<style>
	body{
		background-color: rgb(230,230,230);
	}
	.navbar{
		background-color: rgb(400,4000,400);
	}
	.container{
		margin-top:30px;
	}
	.box {
		margin-bottom:10px;
		width:100%;
	    background: #fff;
	    border-radius: 4px;
	    -webkit-box-sizing: border-box;
	    box-sizing: border-box;
	    display: inline-block;
		position: relative;
		    padding: 27px 25px 34px;
	    border-radius: 3px;
	    border: 1px solid #dedede;
	    border-left-width: 2px;
	}
	.pr{
		margin-bottom:0px;
	}
	.info-booking {
	    line-height: 1.5;
	    font-weight: 300;
	    margin-top: 7px;
	    font-size: 12px;
	    color: #bbbaba;
	}
/*._1Hwni {
    background-color: rgba(0,0,0,.12);
    border: none;
    height: 1px;
    margin: 0;
    margin-top: -1px;
}*/
/*.cqAhQ {
    padding: 16px 32px;
    background-color: #fafafa;
}*/

/*.chk{
	margin-left:20px;
	margin-top:30px;
}*/
.booking-price {
	text-align: right;
    vertical-align: top;
	
}
/*._1TPg2 .booking-price, ._1TPg2 .Xnbsb {
    vertical-align: top;
    font-weight: 500;
}*/
</style>
@endpush
@section('content')
{{--  {{ Auth::user()}}  --}}
<section class="title-section bg-light-grey0">
	<div class="container">
		<div class="row justify-content-md-center text-center">
			<div class="col-md-9">
				<h2 class="title dark-color">Booking Orders</h2>
			</div>  
		</div>
	</div>
</section>
<section class="content-section booking-order">
	<div class="container" >
	<div class="row">
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12">
				<h3>Isi Data</h3>
				
				<div class="box" style="border-left-color: rgb(27, 160, 226);">
				{{--  {{ json_encode(Auth::user()) }}  --}}
					<div class="box-header">
					</div>
					<div class="box-body">
						{!! Form::model($customer, ['route' => ['orders.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
						
					<div class="row">
						<div class="col-md-12">
							<label>Nama Peminjam</label>
							{!! Form::text('guest_name', $customer->name ? $customer->name :Auth::user()->name , ['class'=> 'form-control','required' => 'required']) !!}
							<div class=""><div class="info-booking"></div><div class="info-booking">As in Passport/Official ID Card (without title/special characters)</div></div>
						</div>
						<br>
						<br>
						<div class="col-md-12">
							<label>Phone Number</label>
							{!! Form::number('guest_phone', $customer->phone ? $customer->phone :Auth::user()->phone, ['class'=> 'form-control','required' => 'required']) !!}
							<div class=""><div class="info-booking"></div><div class="info-booking">e.g. +62812345678, for Country Code (+62) and Mobile No. 0812345678</div></div>
						</div>
						<br>
						<br>
						<div class="form-group col-md-12">
							{{ Form::label('description','Catatan') }}
							{{ Form::text('description',"", ['class' => 'form-control']) }}
							<div class=""><div class="info-booking"></div><div class="info-booking">Please write in English or in villas local language.</div></div>							
							@if ($errors->has('description'))
								<div class="help-block text-red">
									{{ $errors->first('description') }}
								</div>
							@endif
						</div>
						
						
					</div>
					</div>
					<br>
						
				</div>
			</div>
			<br>
			<div class="col-md-12">
					<h3>Price</h3>
					<div class="box pr"  style="border-left-color: rgb(27, 160, 226);">
					<div class="box-header">
					
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<h3> {{$facility->facilityType->name}}</h3>
									</div>
								</div>
								<div class="row">								
									<div class="col-md-6">
										<h5> {{ $facility->name."(".$value['duration']." Night)"}} </h5>
									</div>
									<div class="col-md-6  booking-price">
										<h5>{{ H::rupiah($facility->calPrice($value['startdate'],$value['duration'],$value['person'])) }}</h5>										
									</div>
									<div class="col-md-6">
										<h5> {{ "Tax"}} </h5>
									</div>
									<div class="col-md-6  booking-price">
										<h5>{{ H::rupiah($tax) }}</h5>										
									</div>
									
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="box">
						<div class="row">
							<div class="col-md-6">
								<h4>Total</h4>
							</div>
							<div class="col-md-6 booking-price">
								<h4>{{ H::rupiah($facility->calPrice($value['startdate'],$value['duration'],$value['person'])+$tax) }}</h4>
							</div>
						</div>
					</div>
			</div>


		</div>

	</div>
	<br>
	<br>
	<div class="col-md-4" style="margin-top:43px">
		<div class="box" >
			<div class="box-header">
				<center><h3>Booking Detail</h3></center>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">	
								<senter><img width="100%"src="{{ env('BACKEND_HOST').$facility->villa_image }}"/></senter>						
							</div>
							<div class="col-md-6">	
								<h6>{{ $facility->name}}</h6>
							</div>
						</div>
					</div>
					<hr>
					<br>
					<div class="col-md-12">
						<div class="row">
							{{--  <div class="col-md-12"><center><h3>{{$facility->name}}</h3></center></div>  --}}
					<div class="col-md-6 pull-left">Duration of Stay</div>
					<div class="col-md-6 pull-right" style="text-align: right;">{{$value['duration']}} Night</div>
					<div class="col-md-6 pull-left">Check In</div>
					<div class="col-md-6 pull-right" style="text-align: right;">{{$value['startdate']}}</div>
					<div class="col-md-6 pull-left">Check Out</div>
					<div class="col-md-6 pull-right" style="text-align: right;">{{$enddate->format("Y-m-d")}}</div>
					<div class="col-md-6 pull-left">Room Type</div>
					<div class="col-md-6 pull-right" style="text-align: right;">{{$facility->facilityType->name}}</div>
					<div class="col-md-6 pull-left">Guest per Room</div>
					<div class="col-md-6 pull-right" style="text-align: right;">{{$facility->capacity}} Guests</div>
					
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	</div>
	
	<br>
	<br>
	<div class="row">
		<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">				
							{{ Form::submit('Lanjutkan', ['class' => 'btn btn-primary','style'=>'width:100%']) }}
						</div>
					</div>
				</div>
			{{ Form::close() }}
		</div>
	<br>
	<br>
	<br>
	<br>
	</div>
</section>
@stop
