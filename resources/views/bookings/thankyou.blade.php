@extends('layouts.app')
@push('styles')
  
<style>
	
</style>
@endpush
@section('content')
{{--  {{ Auth::user()}}  --}}
<section class="title-section bg-light-grey0">
    <div class="container">
        <div class="row justify-content-md-center text-center">
            <div class="col-md-9">
                <h2 class="title dark-color">Booking Orders</h2>
            </div>  
        </div>
    </div>
</section>
<section class="content-section booking-order">
    <div class="container">
        <h3>Order Details</h3>
    	<div class="row">
        	<div class="col-md-7">		
        		<table class="table table-bordered table-order">
                    <thead>
                        <th class="text-uppercase">Product</th>
                        <th class="text-uppercase text-right">Total</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $facility->name." (".$value['duration']." night) "}}</td>
                            <td class="text-right">{{ H::rupiah($facility->calPrice($value['startdate'],$value['duration'],$value['person'])) }}</td>
                        </tr>
                        <tr>
                            <td>Tax</td>
                            <td class="text-right">{{ H::rupiah($tax) }}</td>
                        </tr>
                        <tr>
                            <td>Payment Method</td>
                            <td class="text-right">Credit Card via DOKU ???</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">Total</td>
                            <td class="text-right font-weight-bold">{{ H::rupiah($facility->calPrice($value['startdate'],$value['duration'],$value['person'])+$tax) }}</td>
                        </tr>
                    </tbody>
                </table>
        	</div>

        	<div class="col-md-5">
                <div class="well">
                    <p class="accent-color">Thank you. Your order has been received.</p>
                    <ul>
                        <li>Order number: <span class="font-weight-normal">17145</span></li>
                        <li>Date: <span class="font-weight-normal">May 7, 2018</span></li>
                        <li>Total: <span style="color: black; font-weight: 500;">{{ H::rupiah($facility->calPrice($value['startdate'],$value['duration'],$value['person'])+$tax) }}</span></li>
                        <li>Payment Method: <span class="font-weight-normal">Credit Card via DOKU ???</span></li>
                    </ul>
                </div>
        	</div>
        </div>
    </div>
</section>
@stop

@push('scripts')
<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 

// Set the date we're counting down to
var countDownDate = new Date("{{ Carbon\Carbon::now()->addDays(H::globs('expiry'))->format("M j, Y H:i:s") }}").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = "Complete your payment within "+days + " days " + hours + " hours "
    + minutes + " minutes " + seconds + " seconds ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }
}, 1000);
});
</script>
@endpush