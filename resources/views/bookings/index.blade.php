@extends('layouts.app')
@push('styles')
<style>
</style>
@endpush

@section('content')
<section class="title-section bg-light-grey0">
	<div class="container">
		<div class="row justify-content-md-center text-center">
			<div class="col-md-9">
				<h2 class="title dark-color"> Tersedia untuk Anda</h2>
			</div>  
		</div>
	</div>
</section>
<section class="content-section booking-list">
	<div class="container" >
		@if(!$request->has('id'))
		@php($list=$nlist)
		@php($prices=$nprices)
			<!-- <center> <h1>Available {{$ident}}</h1></center>-->
		@endif

		@if(count($list)==0 && count($nlist)>0)
			<!-- <center> <h1>Available {{$ident}}</h1></center>	 -->
		@elseif($request->has('id'))
			<!-- <center> <h1>Available {{$ident}}</h1></center>	 -->
		@endif	

		@if(count($list)==0 && count($nlist)==0) <!-- Alternatif tdk ada juga-->
			<center> <h4>Maaf, saat ini tidak ada {{ strtolower($ident) }} yang dapat dipesan untuk {{ $person }} orang dalam {{ $unit }} {{$ident}}</h4></center>	
			<center> <h5><a href={{ route('villas.index',['type'=>2]) }}>Mohon mengganti kriteria pencarian anda.</a></h5></center>	
		@elseif(count($list)==0)
			<center> <h4>Maaf, saat ini {{ strtolower($ident) }} yang anda pilih tidak tersedia pada waktu dan durasi yang anda cari.</h4></center>					
			<center> <h5><a href={{ route('villas.index',['type'=>2]) }}>Mohon mengganti kriteria pencarian anda.</a></h5></center>	
			<center> <h5>atau anda dapat memilih rekomendasi {{ strtolower($ident) }} lain dari kami dibawah ini: </h5></center>						
		@endif

		@php($i=0)
		@foreach($list as $keys => $l)
			<div class="row justify-content-md-center align-items-center mb-5" >
				@php($i++)	
				<div class="col-12 col-md-4">
					<img class="img-fluid" src="{{ env('BACKEND_HOST').$l->villa_image }}"  class="center-cropped">
				</div>
				<div class="col-12 col-md-4 content">

					<h4 class="facility-name dark-color text-uppercase">{{$l->name}}</h4>
					<h3 class="facility-price dark-color">{{H::rupiah($prices[$keys])}}/hari</h3>
					@if($l->capacity>0)
					<p class="facility-guest grey-color">{{$l->capacity}} orang</p>
					@endif
					<a href="{{route('orders.booknow', ['data' => $request->all(),'facility' => $l->id_morph ,'type'=>$type ,'price'=>$prices[$keys]])}}" class="btn btn-small btn-dark">Pesan Sakarang
					</a>
				</div>
				<div class="col-12 col-md-2 content">
					<a class="view-more dark-color" href="{{route('villas.show',['villa'=>$l->slug])}}">Informasi Lebih lanjut</a>
				</div>
			</div>
		@endforeach
</div>
</section>

@if(count($nlist)>0 && $request->has('id'))
<section class="content-section bg-light-grey2 booking-list">	

	<div class="container" >
		<div class="row">
			
			<div class="col-md-12">
				@if(count($list)==0)		
				<!-- <center> <h1>Available {{$ident}}</h1></center>	 -->
				@else
				<!-- <center> <h1>Also Available {{$ident}}</h1></center>	 -->
				@endif
				@php($i=0)
				@foreach($nlist as $keys => $l)
				<div class="row justify-content-md-center align-items-center mb-5" >
				@php($i++)	
				<div class="col-12 col-md-4">
					<img class="img-fluid" src="{{ env('BACKEND_HOST').$l->villa_image }}"  class="center-cropped">
				</div>
				<div class="col-12 col-md-4 content">

					<h4 class="facility-name dark-color text-uppercase">{{$l->name}}</h4>
					<h3 class="facility-price dark-color">{{H::rupiah($nprices[$keys])}}/hari</h3>
					@if($l->capacity>0)
					<p class="facility-guest grey-color">{{$l->capacity}} orang</p>
					@endif
					<a href="{{route('orders.booknow', ['data' => $request->all(),'facility' => $l->id_morph ,'type'=>$type ,'price'=>$nprices[$keys]])}}" class="btn btn-small btn-dark">Pesan Sakarang
					</a>
				</div>
				<div class="col-12 col-md-2 content">
					<a class="view-more dark-color" href="{{route('villas.show',['villa'=>$l->slug])}}">Informasi Lebih lanjut</a>
				</div>
			</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
@endif
@stop