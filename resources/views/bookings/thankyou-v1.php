@extends('layouts.app')
@push('styles')
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
	body{
		background-color: rgb(230,230,230);
	}
	.navbar{
		background-color: rgb(400,4000,400);
		padding-bottom: 25px;
	}
	.container{
		margin-top:30px;
	}
	.box {
		margin-top:10px;
		margin-bottom:10px;
		width:100%;._1E0yz
    margin-right: 20px;
    position: relative;
    padding: 40px 48px;
    background-color: #fff;
    -webkit-box-sizing: border-box;
	box-sizing: border-box;
	}
	.box2{

	margin-top:85px;
	margin-bottom:10px;
	width:100%;
    margin-right: 20px;
    position: relative;
    background-color: #fff;
    -webkit-box-sizing: border-box;
	box-sizing: border-box;
	
	}
.payment_ins {
    border: 1px solid #1ba0e2;
    background-color: #f9f9f9;
    padding: 15px 0 15px 48px;
    margin-bottom: 20px;
    position: relative;
    border-radius: 3px;
}
{{--  ._3TrGl {
    position: relative;
    padding: 16px 24px;
    border-radius: 3px;
    border: 1px solid #dadada;
}  --}}

.trf_to {
    position: relative;
    padding: 16px 48px 16px 56px;
    border: 1px solid #dadada;
    border-radius: 3px;
    background-color: #fff;
    color: #434343;
	font-size: 14px;
    vertical-align: middle;	
}

._314C4, .trf_to {
    vertical-align: middle;
}
._2RTtS {
    display: inline;
    vertical-align: middle;
    line-height: 1.5;
}
._314C4 {
    position: absolute;
    top: 50%;
    left: 16px;
    margin-top: -12px;
}
._3Zc6G {
    position: absolute;
    top: 50%;
    right: 1px;
    margin-top: -8px;
    cursor: pointer;
}
._1wFJT {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 5;
    word-break: break-word;
}
._3TrGl {
    position: relative;
    padding: 16px 24px;
    border-radius: 3px;
    border: 1px solid #dadada;
}

._3TrG {
        position: relative;
    border-radius: 3px;
    border: 1px solid #dadada;
    clear: 10px;
    clear: both;
    margin-right: 14px;
    margin-left: 14px;
}


._2LrN1 {
    background-color: #f9f9f9;
}
._1c3x3, ._1S7Yo, ._1ukFt, ._1z2Uw, ._2LrN1, ._3AbjF, ._3RnOV, ._36efF, .IKLLK {
    position: relative;
    display: inline-block;
    padding: 16px 24px;
    clear: both;
    width: 100%;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}
._1G8V9, ._3FyQC {
    display: block;
    padding-top: 5px;
    position: relative;
    display: table-cell;
    vertical-align: top;
}
.XM8ft {
    display: table;
    width: 100%;
}
._2LrN1 ._104ZX, ._2LrN1 h2 {
    display: table-cell;
    vertical-align: middle;
}


._2LrN1 ._104ZX {
    text-align: right;
}
._2LrN1 ._104ZX, ._2LrN1 h2 {
    display: table-cell;
    vertical-align: middle;
}
._36efF {
    border-bottom: 1px solid #dadada;
}

._1G8V9, ._3FyQC {
    display: block;
    padding-top: 5px;
    position: relative;
    display: table-cell;
    vertical-align: top;
}

._1G8V9 {
    font-size: 16px;
    line-height: 22px;
}
._1G8V9, ._3FyQC {
    color: #434343;
    letter-spacing: 0;
}
._3FyQC {
    margin-right: 16px;
    width: 170px;
    font-size: 14px;
    line-height: 16px;
}
._71uFh, .AYI8S {
    font-weight: 500;
}

.AYI8S {
    position: relative;
    display: inline-block;
    margin-right: 16px;
    width: 25px;
    height: 25px;
    font-size: 14px;
    color: #fff;
    line-height: 25px;
    text-align: center;
    border-radius: 100%;
    background-color: #073e68;
}
._71uFh, .AYI8S {
    display: inline-block;
    vertical-align: middle;
}

._71uFh {
	margin-bottom:20px;
	margin-top:20px;
    font-size: 18px;
    color: #434343;
    letter-spacing: 0;
    line-height: 28px;
}

._71uFh, .AYI8S {
    font-weight: 500;
}
._71uFh, .AYI8S {
    display: inline-block;
    vertical-align: middle;
}


._2nwH5 .ENvKU {
    display: inline-block;
    padding: 16px 24px;
    font-size: 16px;
    font-weight: 500;
    color: #0770cd;
    line-height: 16px;
    text-decoration: none;
    background-color: #f6f6f6;
	border-radius: 3px;
	border-color:#fff;
	margin-left:20px;
}
.iYqZe {
    width: 100%;
    background: #fff;
    -webkit-box-shadow: 0 1px 3px 0 rgba(27,27,27,.1);
    box-shadow: 0 1px 3px 0 rgba(27,27,27,.1);
    border-radius: 4px;
    font-weight: 500;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
._20hnf {
    padding: 16px;
    border-bottom: 1px solid #dadada;
}
._84pte {
    line-height: 24px;
    font-size: 14px;
    padding: 16px 0;
    color: #434343;
}
._2E78J {
    font-size: 14px;
    padding: 16px 0;
}
._2E78J table tr {
    border-bottom: 8px solid #fff;
    line-height: 1.3;
    color: #696969;
    position: relative;
}
._37z4N {
    font-size: 12px;
    font-weight: 700;
    letter-spacing: .5px;
    line-height: 24px;
    text-transform: uppercase;
    color: #8f8f8f;
    padding: 0 16px;
    margin-bottom: 8px;
}
._2E78J table .aWV-S td:first-child {
    padding-left: 14px;
    border-left: 2px solid #235D9F;
}

._3caZy:nth-child(odd) {
	background: #f6f6f6;
}
._3caZy {
    padding: 8px 16px;
}
.eRf9U {
    font-size: 12px;
    font-weight: 700;
    letter-spacing: .5px;
    line-height: 24px;
    text-transform: uppercase;
    color: #8f8f8f;
    padding: 0 16px;
    margin-bottom: 8px;
}
.a82n5 {
	list-style: none;
	padding:0 !important;
	
}
._qqMq:not(:last-child) {
    border-bottom: 1px solid #dadada;
}
.payment_ins:before {
    content: "!";
    display: block;
    width: 25px;
    height: 25px;
    position: absolute;
    background-color: #1ba0e2;
    border: 1px solid #f9f9f9;
    border-radius: 20px;
    left: 11px;
    top: 11px;
    color: #fff;
    text-align: center;
    padding-top: 4px;
    font-weight: 700;
}
._1GDl9 {
    font-weight: 500;
    margin-top: 30px;
    margin-bottom: 30px;
    font-size: 22px;
    line-height: 26px;
}
</style>
@endpush
@section('content')
{{--  {{ Auth::user()}}  --}}
<div class="container" >
	<div class="row">
	<div class="col-md-8">
		<h3 class="_1GDl9">Payment Instruction</h3>		
		<div class="box">                    
		<div class="row">
			<div class="col-md-12">
				{{--  <center><img src="{{ env('BACKEND_HOST').$facility->villa_image }} "></img></center>  --}}
				{{--  <pseudo:before></<pseudo:before>  --}}
				<div class="payment_ins">Payment instructions have been sent to your email.</div>
			</div>
			{{ $order->expiry }}
			<div class="col-md-12">
                {{--  <center><img src="{{ env('BACKEND_HOST').$facility->villa_image }} "></img></center>  --}}
				<div class="PHt0i"><div class="AYI8S">1</div><div class="_71uFh">Make a Payment Before</div></div>
				<div class="_3TrGl">
					<div>
						<div class="_3Uz4S">
							<h2>{{ Carbon\Carbon::now()->addDays(H::globs('expiry'))->format("l, d M Y h:i A")  }}</h2>
							<p id="demo"></p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
                {{--  <center><img src="{{ asset($facility->villa_image) }} "></img></center>  --}}
				<div class="PHt0i"><div class="AYI8S">2</div><div class="_71uFh">Please Transfer to:</div></div>

				<div class="trf_to" text="Please choose ,[object Object], on your transfer type." ><svg width="24" height="24" viewBox="0 0 24 21" class="_314C4" fill="#8F8F8F" stroke="currentColor" stroke-linecap="round" style="left: 16px;"><g fill="none" fill-rule="evenodd"><g transform="translate(1 1)"><path stroke="#8F8F8F" stroke-width="1.5" d="M11.782.439a.916.916 0 0 0-1.564 0L.136 16.939a.917.917 0 0 0 .782 1.394h20.164a.917.917 0 0 0 .782-1.394L11.782.439z"></path><rect width="1.833" height="7.333" x="10.083" y="5.5" stroke="none" fill="#8F8F8F" rx=".917"></rect><rect width="1.833" height="1.833" x="10.082" y="13.75" stroke="none" fill="#8F8F8F" rx=".917"></rect></g></g></svg>
					<div class="_2RTtS">Please choose <strong>Online Transfer</strong> on your transfer type.</div>
					<div class="_3Zc6G">
						<div class="_1wFJT">
							{{--  <div class="_10n6e _3RHzj _2VyPv" style="min-width: 350px;">Please make sure to choose <strong>Online/Real Time transfer</strong> as your payment type. We do not accept payment by LLG/Kliring/SKBI and RTGS. If you choose to transfer via ATM, please ignore this message.  --}}
							</div>
						</div>
						<a href="#" data-toggle="tooltip" data-placement="bottom" title="Please make sure to choose BCA, as your payment type. We do not accept payment by LLG/Kliring/SKBI and RTGS. If you choose to transfer via ATM, please ignore this message."><svg stroke-width="0" width="16" height="16" viewBox="0 0 16 16" fill="#0080C7" stroke="currentColor" stroke-linecap="round"><path fill="#0080C7" fill-rule="evenodd" d="M8,16 C3.581722,16 0,12.418278 0,8 C0,3.581722 3.581722,0 8,0 C12.418278,0 16,3.581722 16,8 C16,12.418278 12.418278,16 8,16 Z M8,14 C8.55228475,14 9,13.5522847 9,13 C9,12.4477153 8.55228475,12 8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 Z M12,6 C12,3.790861 10.209139,2 8,2 C6.92557447,2 5.91622834,2.42691741 5.17157288,3.17157288 C4.80069616,3.54244959 4.50524612,3.98305357 4.30388793,4.46864929 C4.09234236,4.97881291 4.33442012,5.5638738 4.84458374,5.77541937 C5.35474736,5.98696494 5.93980825,5.74488718 6.15135382,5.23472356 C6.251828,4.99241988 6.39975256,4.77182032 6.58578644,4.58578644 C6.95908733,4.21248555 7.46145959,4 8,4 C9.1045695,4 10,4.8954305 10,6 C10,7.1045695 9.1045695,8 8,8 C7.44771525,8 7,8.44771525 7,9 L7,10 C7,10.5522847 7.44771525,11 8,11 C8.55228475,11 9,10.5522847 9,10 L9,9.87398251 C10.7252272,9.42993972 12,7.86383943 12,6 Z">
							</path></a>
						
						</svg>
					</div>
				</div>
				<div class="_3TrG">
					<div>
						<div class="_7xep0">
							<div class="_2LrN1">
								<div class="XM8ft">
									<h2>Bank Central Asia (BCA)</h2>
									<div class="_104ZX"><img class="_3bVOZ" src="https://tvlk.imgix.net/imageResource/2017/01/06/1483707671328-03b97002d826e482d58654eff5aee566.png?auto=compress%2Cformat&amp;cs=srgb&amp;fm=png&amp;ixlib=java-1.1.1&amp;q=75">
									</div>
								</div>
							</div>
							<div class="_36efF">
								<div class="vjIcY">
									<div class="_3FyQC">Account Number:
										</div>
										<div class="_1G8V9">
											<p><b>084 999 1100</b></p>
											<p></p>
										</div>
									</div>
									<div class="vjIcY">
										<div class="_3FyQC">
											Account Holder Name:</div>
											<div class="_1G8V9">
												<p><b>PT. Trinusa Travelindo</b></p><p>KCP Wisma Asia</p>
											</div>
										</div>
									</div>
									<div>
										<div class="_1c3x3">
											<div class="vjIcY">
												<div class="_3FyQC">
													Transfer Amount:
												</div>
												<div class="_1G8V9">
													<p><b>{{ H::rupiah($order->totalLabel+$order->taxLabel) }}<div></div></span></span></span></b></p></div></div></div></div></div></div></div>
{{--  tooltiptext="Please make sure to choose ,[object Object], as your payment type. We do not accept payment by LLG/Kliring/SKBI and RTGS. If you choose to transfer via ATM, please ignore this message."  --}}
	
		
				

			<br>
			<div class="col-md-12">
				<div class="PHt0i"><div class="AYI8S">3</div><div class="_71uFh">Completed Your Payment?</div></div>
                <div class="_3TrGl"><div><div class="_1r-NZ"><p>Once your payment is confirmed, we will send your hotel voucher to your email address.</p><div class="_2nwH5"><button class="ENvKU _3LLHb _2V6Hh _2ooMH" type="button">I Have Completed Payment</button></div></div></div></div>
            </div>
        
            
		</div>
		</div>
		</div>


	<div class="col-md-4">
		<div class="box2">
		<div class="row">
			<div class="col-md-12">
				<div class="iYqZe">
					<div class="_20hnf">
						<div>
							<div class="_2up-n">Booking ID</div>
								<div class="_3gYXq _3sN6_ tvat-bookingId">
									{{$order->order_code}}
								</div>
							</div>
						</div>
						<div class="_16bOI">
							<div class="_qqMq">
								<div class="_2E78J">
									<div class="_37z4N">Booking details</div>
									<table>
										<tbody>
											<tr class="aWV-S">
												<td class="szrWc tvat-hotelName">{{$facility->name}}</td>
											</tr>
											<tr>
												<td class="szrWc tvat-hotelNights">
													<ul>
														<li class="tvat-checkInDate">{{ $order->date->format("l, d M Y h:i A")}}</li>
														<li class="tvat-hotelNights">{{ $value['duration'] }} Night</li>
														<li class="tvat-hotelRooms">{{ $value['person'] }} Person</li>
													</ul>
												</td>
											</tr>
								l		</tbody>
									</table>
								</div>
							</div>
							<div class="_84pte">
								<div class="eRf9U">Guest</div>
								<ol class="a82n5">
									<li class="_3caZy  tvat-passengerName">
										<span>{{ $order->guest_name }}</span>
									</li>
								</ol>
							</div>
						</div>
					</div>
			</div>
		</div>
		</div>
	</div>
	<br>
	<br>


	</div>
</div>
@stop

@push('scripts')
<script>
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 

// Set the date we're counting down to
var countDownDate = new Date("{{ Carbon\Carbon::now()->addDays(H::globs('expiry'))->format("M j, Y H:i:s") }}").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = "Complete your payment within "+days + " days " + hours + " hours "
    + minutes + " minutes " + seconds + " seconds ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }
}, 1000);
});
</script>
@endpush