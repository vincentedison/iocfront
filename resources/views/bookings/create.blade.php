	@extends('layouts.app')
	@push('styles')
	<style>
	.detail{
margin-bottom: -10px !important;
font-size: 0.9rem !important;
color: rgb(168, 222, 48);
	}	
	</style>
	@endpush
	@section('content')
	{{--  {{ Auth::user()}}  --}}
	<section class="title-section bg-light-grey0">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-9">
					<h2 class="title dark-color">Proses Booking</h2>
				</div>  
			</div>
		</div>
	</section>
	<section class="content-section booking-order">
		<div class="container">
			<div class="row mb-4 justify-content-md-center">

				<div class="col-md-8 ">
					{!! Form::model($customer, ['route' => ['orders.store'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
					<h3 class="mt-4">Data Booking</h3>
				
					<div class="form-group row">
						<div class="col-md-6">
							<label for="guest_name">Nama</label>
							{!! Form::text('guest_name', $customer->name ? $customer->name :Auth::user()->name , ['class'=> 'form-control','required' => 'required']) !!}
							<div class="info-booking">Nama sesuai Passport/KTP (Tanpa Gelar / Karakter Khusus)</div>
						</div>
						<div class="col-md-6">
							<label for="guest_phone">Nomor Telepon</label>
							{!! Form::number('guest_phone', $customer->phone ? $customer->phone :Auth::user()->phone, ['class'=> 'form-control','required' => 'required']) !!}
							<div class="info-booking">e.g. +62812345678, untuk Kode Negara (+62) dan Nomor Telepon 08112345678</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<label  class="detail">Durasi</label>
							<p>{{ $value['duration']}} hari</p>
						</div>
						<div class="col-md-3">
							<label  class="detail">Check In </label>
							<p>{{ H::tanggal(Carbon\carbon::parse($value['startdate'])->format("w-d-n-Y")) }}</p>
						</div>
						<div class="col-md-3">
							<label class="detail">Check Out </label>
							<p>{{ H::tanggal($enddate->format("w-d-n-Y")) }}</p>
						</div>
						<div class="col-md-3">
							<label  class="detail">Jumlah Tamu / Peserta </label>
							<p>{{$value['person']}} orang</p>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<label for="description">Catatan</label>
							{{ Form::text('description',"", ['class' => 'form-control']) }}
							<div class="info-booking">Tolong menggunakan bahasa Inggris atau Indonesia</div>
						</div>
					</div>

					<h3 class="mt-4">Pesanan Anda</h3>
					@if ($errors->has('name'))
					<p style="color: red">{{ $errors->first('name') }}</p>
							@endif
					<table class="table table-bordered table-order">
						<thead>
							<th class="text-uppercase">Pesanan</th>
							<th class="text-uppercase">jumlah</th>
							<th class="text-uppercase text-right">Total</th>
						</thead>
						<tbody>
							@php($total=0)
							@foreach ($floops as $i=>$facilitys)
							<tr>
								<td>{{ $facilitys['facility']->name." (".$value['duration']." hari)"}}</td>
								@php($subtotal=$facilitys['facility']->calPrice($value['startdate'],$value['duration'],$facilitys['person'])*$facilitys['unit'])
								@php($total+=$subtotal)
								<td>{{ $facilitys['unit']." Unit x ".$facilitys['person']." Orang" }}</td>
								<td class="text-right">{{ H::rupiah($subtotal) }}</td>
							</tr>
							@endforeach
							@foreach ($afloops as $i=>$food)
							<tr>
								@if(count($food['food'])>1)
									<td>{{ $food['food'][0]->addon->name." - ".$food['food'][0]->name.", ".$food['food'][1]->name}}</td>
									@php($subtotal=$food['food'][0]->addon->price*$food['amount'])
									@php($total+=$subtotal)
									<td>{{ $food['amount']." Porsi" }}</td>	
								@elseif(count($food['food'])>0)
									@if( strpos($food['originidfood'],'|') === false)
									<td>{{ $food['food']->addon->name." - ".$food['food']->name }}</td>
									@else
									<td>{{ $food['food']->addon->name." - ".$food['food']->name."(2)" }}</td>
									@endif
									@php($subtotal=$food['food']->addon->price*$food['amount'])
									@php($total+=$subtotal)
									<td>{{ $food['amount']." Porsi" }}</td>
								@endif
							

								<td class="text-right">{{ H::rupiah($subtotal) }}</td>
							</tr>
							@endforeach
							@foreach ($aloops as $i=>$facilitys)
							<tr>
								<td>{{ $facilitys['addon']->name}}</td>
								@php($subtotal=$facilitys['addon']->price*$facilitys['amount'])
								@php($total+=$subtotal)
								<td>{{ $facilitys['amount']." Buah" }}</td>
								<td class="text-right">{{ H::rupiah($subtotal) }}</td>
							</tr>
							@endforeach
							<tr style="background-color:#efefef;"> 
								<td class="font-weight-bold" colspan="2">Total</td>
								<td class="text-right font-weight-bold">{{ H::rupiah($total) }}</td>
							</tr>
							
						</tbody>
					</table>
					<div class="payment-method">
						<h6 class="payment-ins">Silahkan memilih metode pembayaran yang ingin digunakan.</h6>
						<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
							Transfer Bank
						</label>
						</div>
						<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
							Kartu Kredit via DOKU
						</label>
						</div>
						<div class="form-check">
						<label class="form-check-label">
							<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3">
							Alfamart via DOKU
						</label>
						</div>
					</div>
					
				</div>
				{{-- <div class="col-md-4 booking-detail">
					<h5>Detail Booking</h5>
					<div class="box">
					<img width="100%" class="img-fluid" src="{{ env('BACKEND_HOST').$facility->villa_image }}"/>
					<h5 class="mt-2">{{ $facility->name}}</h5>
					<div class="row">
						
						<div class="col-8 col-md-7">
							<p>Durasi :</p>
						</div>
						<div class="col-4 col-md-5 text-right">
							<p>{{$value['duration']}} hari</p>
						</div>
						<div class="col-8 col-md-7">
							<p>Check In : </p>
						</div>
						<div class="col-4 col-md-5 text-right">
							<p>{{$value['startdate']}}</p>
						</div>
						<div class="col-8 col-md-7">
							<p>Check Out : </p>
						</div>
						<div class="col-4 col-md-5 text-right">
							<p>{{$enddate->format("Y-m-d")}}</p>
						</div>
						@if(session()->get('type', 'default') == "Facility")
						<div class="col-8 col-md-7">
							<p>Tipe : </p>
						</div>
						<div class="col-4 col-md-5 text-right">
							<p>{{$facility->facilityType->name}}</p>
						</div>
						@endif
						<div class="col-8 col-md-7">
							<p>Jumlah Tamu / Peserta : </p>
						</div>
						<div class="col-4 col-md-5 text-right">
							<p>{{$value['person']}} orang</p>
						</div>
						
					</div>
					</div>
				</div> --}}
			</div>
			
			<div class="row justify-content-end">
				<div class="col-md-2 col-lg-2 col-8 selanjutnya ">				
					{{ Form::submit('Selanjutnya', ['class' => 'btn btn-block btn-accent btn-small']) }}
				</div>
			</div>
				{{ Form::close() }}
			<div class="row justify-content-start" style="margin-top: -39px;">
				<div class="col-md-2 col-lg-2 col-8" >	
					<a href="{{ route('addon.create') }}" class="btn btn-block btn-small btn-warning goback">< Kembali</a>
				</div>
			</div>
		
			</div>
		</div>
	</section>
	@stop

@push('scripts')
<script>
</script>
@endpush