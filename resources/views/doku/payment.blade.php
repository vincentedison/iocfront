@extends('layouts.app')
@push('styles')
  
<style>
	
</style>
@endpush
@section('content')
    <section class="payment-to-doku">
    <div class="container">
      <div class="row">
        <div class="col text-center">
          <p class="h5">Anda akan melakukan pembayaran melalui DOKU Payment Gateway...</p>
        </div>
      </div>
    </div>
    </section>
    @php
        $inv_total_amount = number_format($invoice->total_label, 2, '.','');
    @endphp
    <FORM id="doku-form" NAME="order" METHOD="POST" style="display: none" ACTION="{{ env('DOKU_URL') }}" >
        <input type="hidden" name="MALLID" value="{{ env('DOKU_MALL_ID') }}"/>
        <input type="hidden" name="CHAINMERCHANT" value="NA"/>
        <input type=hidden name="AMOUNT" value="{{ $inv_total_amount }}">
        <input type=hidden name="PURCHASEAMOUNT" value="{{ $inv_total_amount }}">
        <input type=hidden name="TRANSIDMERCHANT" value="{{ $invoice->invoice_code }}">
        
        <input type=hidden name="WORDS" value="{{ sha1($inv_total_amount . env('DOKU_MALL_ID') . env('DOKU_SHARED_KEY') . $invoice->invoice_code) }}">
        
        <input type="hidden" name="REQUESTDATETIME" value="{{ now()->format('YmdHis') }}">
        <input type="hidden" name="CURRENCY" value="360">
        <input type="hidden" name="PURCHASECURRENCY" value="360">
        <input type="hidden" name="SESSIONID" value="{{ session()->getId() }}">
        
        <input type=hidden name="NAME" value="{{ $order->customer->name }}">
        <input type=hidden name="EMAIL" value="{{ $order->customer->email }}">
        
        <input type=hidden name="BASKET" value="Ubaya IOC Payment #{{ $invoice->invoice_code }},{{ $inv_total_amount }},1,{{ $inv_total_amount }}">
        
        {{-- <input type="hidden" name="PAYMENTCHANNEL" value="36"> --}}
    </FORM>

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $('#doku-form').submit();
        }, 200);
    });
  </script>
@endpush