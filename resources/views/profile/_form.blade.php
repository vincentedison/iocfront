@extends('profile.profile')

@section('side-nav')
   <div class="navmen"> <a href="{{ route('profile') }}">Profil Utama</a></div>
   <div class="navmen"><a href="{{ route('password') }}">Ubah Kata Sandi</a></div>
   <div class="diff">Ubah Profil</div>
@endsection
@section('titleP')
Ubah Profil
@endsection

@section('box')
	{!! Form::model($customer, ['route' => ['upd.cus'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
						
						<div class="form-group  col-md-12">
							<label>Nama</label>
							{!! Form::text('name', $customer->name ? $customer->name :Auth::user()->name , ['class'=> 'form-control','required' => 'required']) !!}
							<div class=""><div class="_3DvyZ uOf_z"></div><div class="uOf_z">Nama sesuai Passport/KTP (Tanpa Gelar / Karakter Khusus)</div></div>
						</div>
						<div class="form-group col-md-12">
							<label>Nomor Hp</label>
							{!! Form::number('phone', $customer->phone ? $customer->phone :Auth::user()->phone, ['class'=> 'form-control','required' => 'required']) !!}
							<div class=""><div class="_3DvyZ uOf_z"></div><div class="uOf_z">e.g. +62812345678, untuk Kode Negara (+62) dan Nomor Telepon 08112345678</div></div>
						</div>
						<div class="form-group col-md-12">
                            {{ Form::label('email','Email') }}
                            {{ Form::email('email', $customer->email, ['class' => 'form-control']) }}
                            @if ($errors->has('email'))
                                <div class="help-block text-red">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-12">
                            {{ Form::label('card_id','Nomor KTP') }}
                            {{ Form::number('card_id', $customer->card_id, ['min'=>16,'class' => 'form-control']) }}
                            @if ($errors->has('card_id'))
                                <div class="help-block text-red">
                                    {{ $errors->first('card_id') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-12">
                            {{ Form::label('address','Alamat') }}
                            {{ Form::text('address', $customer->address, ['class' => 'form-control']) }}
                            @if ($errors->has('address'))
                                <div class="help-block text-red">
                                    {{ $errors->first('address') }}
                                </div>
                            @endif
                        </div>
@endsection
@section('btn')		
<div class="col-md-3">				
	{{ Form::submit('Simpan', ['class' => 'btn btn-block btn-accent','style'=>'width:100%']) }}
    {{ Form::close() }}                            
</div>
@endsection