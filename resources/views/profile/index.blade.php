@extends('profile.profile')

@section('side-nav')
   <div class="diff">Profil Utama</div>
   <div class="navmen"><a href="{{ route('password') }}">Ubah Kata Sandi</a></div>       
   <div class="navmen"><a href="{{ route('load.cus') }}">Ubah Profil</a></div>
@endsection

@section('titleP')
Data Diri
@endsection

@section('box')
	{{--  {!! Form::model($customer, ['route' => ['upd.cus'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}  --}}
						
						<div class="col-md-12">
                            <label>Nama</label>
                            <h5>{{ Auth::user()->name }}</h5>
							{{--  {!! Form::text('name', $customer->name ? $customer->name :Auth::user()->name , ['class'=> 'form-control','required' => 'required']) !!}  --}}
							{{--  <div class=""><div class="_3DvyZ uOf_z"></div><div class="uOf_z">As in Passport/Official ID Card (without title/special characters)</div></div>  --}}
                        </div>
                        <br>
						<div class="col-md-12">
							<label>Nomor Hp</label>
                            <h5>{{ Auth::user()->phone }}</h5>
                            
                            {{--  <div class=""><div class="_3DvyZ uOf_z"></div><div class="uOf_z">e.g. +62812345678, for Country Code (+62) and Mobile No. 0812345678</div></div>  --}}
                        </div>
                        <br>
                        
						<div class="form-group col-md-12">
                            {{ Form::label('email','Email') }}
                            <h5>{{ Auth::user()->email }}</h5>                            
                            {{--  {{ Form::email('email', $customer->email, ['class' => 'form-control']) }}  --}}
                 
                        </div>
                        <br>
                        
                        <div class="form-group col-md-12">
                            {{ Form::label('card_id','Nomor KTP') }}
                            <h5>{{ Auth::user()->card_id }}</h5>                            
                            {{--  {{ Form::text('card_id', $customer->card_id, ['class' => 'form-control']) }}  --}}
                     
                        </div>
                        <br>
                        
                        <div class="form-group col-md-12">
                            {{ Form::label('address','Alamat') }}
                            <h5>{{ Auth::user()->address }}</h5>                            
                            {{--  {{ Form::text('address', $customer->address, ['class' => 'form-control']) }}  --}}
                         
                        </div>
@endsection
