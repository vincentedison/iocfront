@extends('layouts.app')
@push('styles')
<style>
	.navbar{
		background-color: rgb(400,4000,400);
	}
    .side-nav {
		margin-bottom:10px;
		width:100%;
    background: #fff;
    border-radius: 4px;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    display: inline-block;
	position: relative;
    border-radius: 3px;
    border: 1px solid #dedede;
    border-left-width: 2px;
    }
    
	.box {
		margin-bottom:10px;
		width:100%;
    background: #fff;
    border-radius: 4px;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    display: inline-block;
	position: relative;
	    padding: 27px 25px 34px;
    border-radius: 3px;
    border: 1px solid #dedede;
    border-left-width: 2px;
	}
	.pr{
		margin-bottom:0px;
	}
	.uOf_z {
    line-height: 1.5;
    font-weight: 300;
    margin-top: 7px;
    font-size: 12px;
    color: #bbbaba;
}
._1Hwni {
    background-color: rgba(0,0,0,.12);
    border: none;
    height: 1px;
    margin: 0;
    margin-top: -1px;
}
.cqAhQ {
    padding: 16px 32px;
    background-color: #fafafa;
}

.chk{
	margin-left:20px;
	margin-top:30px;
}
._1wPv_ {
	text-align: right;
    vertical-align: top;
	
}
._1TPg2 ._1wPv_, ._1TPg2 .Xnbsb {
    vertical-align: top;
    font-weight: 500;
}
.diff {
    padding: 16px 32px;
    border-left: 2px solid rgb(168, 222, 48);
}
.navmen{
    padding: 16px 32px;    
}
.side-nav a{
    text-decoration: none;
    color: #444;    
}
.side-nav a:hover{
    color: #235D9F;    
}
.text-red{
    color: #dc3545;
}
label{
	font-size: 14px;
    margin: 0px;
    color: grey;
}
</style>
@endpush
@section('content')
{{--  {{ Auth::user()}}  --}}
<section class="section bg-light-grey login">
	<div class="container h-100">
		<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
	            <h3>Profile Navigation</h3>
					<div class="side-nav">
	                    @yield('side-nav')                    

					</div>
				</div>
				<div class="col-md-8">
					<h3>@yield('titleP')  </h3>
					
					<div class="box" style="border-left-color: rgb(168, 222, 48);">
					{{--  {{ json_encode(Auth::user()) }}  --}}
						<div class="box-header">
						</div>
						<div class="box-body">
				
	                    @yield('box')                    
	                        
						
						</div>
							
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="row">
			<div class="col-md-8 offset-md-4 mt-3">
						<div class="row justify-content-center">
							@yield('btn')
						</div>
					</div>
			</div>
	</div>
</section>
@stop
