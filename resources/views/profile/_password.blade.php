@extends('profile.profile')

@section('side-nav')
   <div class="navmen"> <a href="{{ route('profile') }}">Profil Utama</a></div>
   <div class="diff">Ubah Kata Sandi</div>
   <div class="navmen"><a href="{{ route('load.cus') }}">Ubah Profil</a></div>
@endsection
@section('titleP')
Ubah Password
@endsection
@section('box')
	{!! Form::model(Auth::user(), ['route' => ['upd.pass'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
						
						<div class="form-group  col-md-12">
							<label>Kata Sandi Lama</label>
							<input id="old_pass" type="password" class="form-control{{ $errors->has('old_pass') ? ' is-invalid' : '' }}" name="old_pass" required>
                             @if ($errors->has('old_pass'))
                                <div class="help-block text-red">
                                    {{ $errors->first('old_pass') }}
                                </div>
                            @endif
						</div>
						<div class="form-group col-md-12">
							<label>Kata Sandi Baru</label>
							<input id="new_pass" type="password" class="form-control{{ $errors->has('new_pass') ? ' is-invalid' : '' }}" name="new_pass" required>                            
                             @if ($errors->has('new_pass'))
                                <div class="help-block text-red">
                                    {{ $errors->first('new_pass') }}
                                </div>
                            @endif
						</div>
						<div class="form-group col-md-12">
                            {{ Form::label('conf_new','Konfirmasi Kata Sandi Baru') }}
							<input id="conf_pass" type="password" class="form-control{{ $errors->has('conf_pass') ? ' is-invalid' : '' }}" name="conf_pass" required>                                
                            @if ($errors->has('conf_pass'))
                                <div class="help-block text-red">
                                    {{ $errors->first('conf_pass') }}
                                </div>
                            @endif
                        </div>
@endsection
@section('btn')			
<div class="col-md-3">				
	{{ Form::submit('Ganti Kata Sandi', ['class' => 'btn btn-block btn-accent','style'=>'width:100%']) }}
    {{ Form::close() }}                            
</div>
@endsection