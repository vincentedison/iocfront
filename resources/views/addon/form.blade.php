	@extends('layouts.app')
	@push('styles')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
	<style>
	#facilityBody{
		max-height: 80%;

	}
	.myfacilitytable th{
		text-align:center;
	}
	td.price{
		text-align:end;
	}
	.modal-dialog {
		max-width: 700px;
		margin: 1.75rem auto;
		
	}
	.overlay {
		background: #000; 
		display: none;        
		position: fixed; 
		height: 100%;
		width:100%;
		top: 0;                  
		right: 0;                
		bottom: 0;
		left: 0;
		opacity: 0.5;
		z-index: 10000;
		
	}

	#loader {
	position: absolute;
	left: 50%;
	top: 50%;
	width: 150px;
	height: 150px;
	margin: -75px 0 0 -75px;
	border: 16px solid #f3f3f3;
	border-radius: 50%;
	border-top: 16px solid #3498db;
	width: 120px;
	height: 120px;
	-webkit-animation: spin 2s linear infinite;
	animation: spin 2s linear infinite;
	}

	@-webkit-keyframes spin {
	0% { -webkit-transform: rotate(0deg); }
	100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
	0% { transform: rotate(0deg); }
	100% { transform: rotate(360deg); }
	}

	/* Add animation to "page content" */
	.animate-bottom {
	position: relative;
	-webkit-animation-name: animatebottom;
	-webkit-animation-duration: 1s;
	animation-name: animatebottom;
	animation-duration: 1s
	}

	@-webkit-keyframes animatebottom {
	from { bottom:-100px; opacity:0 } 
	to { bottom:0px; opacity:1 }
	}

	@keyframes animatebottom { 
	from{ bottom:-100px; opacity:0 } 
	to{ bottom:0; opacity:1 }
	}

	#tablefacilitytofill {
	display: none;
	overflow-y: scroll;
	text-align: center;
	}

	</style>
	@endpush
	@section('content')
	<div class="overlay">
		<div id="loader"></div>
	</div>

	{{--  {{ Auth::user()}}  --}}
	<section class="title-section bg-light-grey0">
		<div class="container">
			<div class="row justify-content-md-center text-center">
				<div class="col-md-9">
					<h2 class="title dark-color">Booking Anda</h2>
				</div>  
			</div>
		</div>
	</section>
	<section class="content-section booking-order">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				@if (Session::has('error'))
					<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-times"></i> Error!</h4>
					{!! session('error') !!}
					</div>
				@endif
				</div>
			</div>
	{!! Form::model($customer, ['route' => ['orders.cart'], 'method'=>'POST','enctype'=>'multipart/form-data', 'role' => 'form']) !!}
			<div class="row">
				<div class="col-md-12">
					<h3 class="mt-4">Akomodasi</h3>
					<table class="table table-bordered table-order table-facility">
						<thead>
							<th></th>
							<th class="text-uppercase" colspan="2" width="30%">Fasilitas</th>
							<th class="text-uppercase">Check-in</th>
							<th class="text-uppercase">Check-out</th>
							<th class="text-uppercase">Jumlah Tamu / Peserta</th>
							<th class="text-uppercase">Jumlah Unit</th>
							<th class="text-uppercase">Harga/unit</th>
							<th class="text-uppercase text-right">Total</th>
						</thead>
						<tbody>
							<tr class="rowfacility" style="display:none">
								{{ Form::hidden('facilid[]','N-0', ['id'=>'fhid-0',  'class' => 'form-control', 'required']) }}							
								<td id="fdelete-0">
									<a href="" class="btn btn-default delete_line"><span class="fa fa-times" aria-hidden="true"></span></a>
								</td>
								<td id="fimage-0"><img width="100%" class="img-fluid" src="{{ env('BACKEND_HOST') }}"/></td>
								<td id="fname-0" width="15%">{{ " (".$value['duration']." hari) "}} </td>
								<td id="fstart-0">{{$value['startdate']}}</td>
								<td id="fend-0">{{$enddate->format("Y-m-d")}}</td>
								<td id="fperson-0">{{$value['person']}} orang</td>
								{{ Form::hidden('facil_person[]',0, ['class' => 'form-control','id'=>"fhperson-0" ,'required']) }}							
								<td id="funit-0">{{$value['unit']}} unit</td>
								{{ Form::hidden('facil_unit[]',0, ['class' => 'form-control','id'=>"fhunit-0" ,'required']) }}							
								{{ Form::hidden('facil_amount[]',0, ['class' => 'form-control faciltot subtot','id'=>"fsub-0" ,'required']) }}
								<td id="fprice-0">0</td>
								<td class="text-right" id="ftotal-0"></td>
							</tr>
							@php($alltotal=0)
							@foreach ($facilitytoform as $key=>$facility)
							@php($i=$key+1)
							@php($facility=(Object) $facility)
							<tr class="rowfacility">
								{{ Form::hidden('facilid[]',$facility->facility->id_morph, ['id'=>'fhid-1',  'class' => 'form-control', 'required']) }}							
								<td id="fdelete-1">
									<a href="" class="btn btn-default delete_line"><span class="fa fa-times" aria-hidden="true"></span></a>
								</td>
								<td id="fimage-1"><img width="100%" class="img-fluid" src="{{ env('BACKEND_HOST').$facility->facility->villa_image }}"/></td>
								<td width="15%" id="fname-1">{{ $facility->facility->name." (".$value['duration']." hari) "}} </td>
								<td id="fstart-1">{{$value['startdate']}}</td>
								<td id="fend-1">{{$enddate->format("Y-m-d")}}</td>
								<td id="fperson-1">{{$facility->person}} orang</td>
								{{ Form::hidden('facil_person[]',$facility->person, ['class' => 'form-control','id'=>"fhperson-0" ,'required']) }}							
								{{ Form::hidden('facil_unit[]', $facility->unit, ['class' => 'form-control','id'=>"fhunit-0" ,'required']) }}											
								@php($total=$facility->facility->calPrice($value['startdate'],$value['duration'],$facility->person)*$facility->unit)
								@php($perday=$total/$value['duration']/$facility->unit)
								@php($alltotal+=$total)
								<td id="funit-1">{{$facility->unit}} unit</td>
								<td id="fprice-1">{{ H::rupiah($perday) }},00</td>
								{{ Form::hidden('facil_amount[]',$total, ['class' => 'form-control faciltot subtot','id'=>"fsub-1", 'required']) }}							
								<td class="text-right" id="ftotal-1">{{ H::rupiah($total) }},00</td>
							</tr>
							@endforeach
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="9">
								<a href="" class="btn btn-success searchfacil" data-toggle="modal" data-target="#myModalFacility"> <span> <i class="fa fa-plus-circle"></i></span>Tambah Fasilitas</a>
								</td>
							</tr>
							<tr style="background-color: #eee;">
								<th class="font-weight-bold" colspan="8">Total</th>
								{{-- {{ Form::hidden('alltot[]',0, ['min'=>0,"id"=>"alltamtot", 'class' => 'form-control total', 'required']) }} --}}
								<th id="ftotal" class="text-right font-weight-bold">{{ H::rupiah($alltotal) }},00</th>
							</tr>
						</tfoot>
					</table>
					
					
				</div>
			</div>
			

			<div class="row">
				<div class="col-md-12">
					<h3 class="mt-4">Makanan</h3>
					<p style="margin-bottom:5px"> Mohon kosongi kolom makanan bila tidak ingin memesan makanan pada jadwal yang tertera</p>
					<p id="noticeMeSenpai" style="color:#d32f2f; font-size:12px;">* Makanan pemesanan setiap jenis menu adalah 10 porsi, untuk info lebih lanjut silahkan hubungi kami.</p>
					<table class="table table-bordered table-order table-food">
						<tbody>
							@php($size=count($menu))
							@php($date=0)
							@php($gtotal=0)
							@php($total=0)
							@php($peritem=0)
							@for ($i=0;$i<$value['duration'];$i++)
							@for ($j=0;$j<$size;$j++)
							@php($id=$i*$size+$j)
							@if(count($foodtoform)>0)
								@php($food=(Object) $foodtoform[$id]['food'])
								@php($total=$foodtoform[$id]['total'] )
								@if($foodtoform[$id]['amount']>0)
									@php($peritem=$total/$foodtoform[$id]['amount'] )
								@endif
								@php($gtotal+=$total)
							@endif
							@if ($date!=Carbon\carbon::parse($value['startdate'])->addDay($i+$menu[$j][2])->format("d/m/Y"))
								<thead>
									<th colspan="6">{{Carbon\carbon::parse($value['startdate'])->addDay($i+$menu[$j][2])->format("d/m/Y")}}</th>
								</thead>
							@php($date=Carbon\carbon::parse($value['startdate'])->addDay($i+$menu[$j][2])->format("d/m/Y"))
							@endif
							<tr class="rowfood">
								<td><a href="" class="btn btn-default delete_line"><span class="fa fa-times" aria-hidden="true"></span></a>
								</td>
								<td>{{ $menu[$j][1] }}</td>
								{{ Form::hidden('fooddate[]',$date, ["id"=>"fooddate-".$id, 'class' => 'form-control fooddate', 'required']) }}
								{{ Form::hidden('foodtype[]',isset($foodtoform[$id]['type']) ? $foodtoform[$id]['type'] : $menu[$j][1], ['min'=>0,"id"=>"foodtype-".$id, 'class' => 'form-control foodtype', 'required']) }}
								<td align="center" width="5%">{{ Form::text('foodtime[]',isset($foodtoform[$id]['time']) ? $foodtoform[$id]['time'] : $menu[$j][3], ['min'=>0,"id"=>"foodtime-".$id, 'class' => 'form-control foodtime','pattern'=>'([01]?[0-9]{1}|2[0-3]{1}):[0-5]{1}[0-9]{1}', 'placeholder'=>'Ex: 23:59','title'=>"Time Format is HH:mm and Between 00:00 - 23:59" , 'required']) }}</td>
								<td align="center">{{ Form::hidden('foods[]',isset($foodtoform[$id]['food']) ? $foodtoform[$id]['id'] : 0, ['min'=>0,"id"=>"foods-".$id, 'class' => 'form-control foods', 'required']) }}
									@if ($i==0&&$j==0&& !isset($foodtoform[$id]['food']))
										<a href=""  class="pickitem" id="info" onclick="pick({{$id}})" data-placement='top' title='Pilih Menu Dengan menekan ini' data-toggle="modal" data-target="#myModal{{$menu[$j][0]}}"><div id="food-{{ $id }}">{{ isset($foodtoform[$id]['food']) ? $foodtoform[$id]['label'] : "Pilih Menu" }}</div></a></td>
									@else
										<a href=""  class="pickitem" onclick="pick({{$id}})" data-toggle="modal" data-target="#myModal{{$menu[$j][0]}}"><div id="food-{{ $id }}">{{ isset($foodtoform[$id]['food']) ? $foodtoform[$id]['label'] : "Pilih Menu" }}</div></a></td>
									@endif
								<td> 
									@php($defood= isset($foodtoform[$id]['amount']) ? $foodtoform[$id]['amount'] : $value['person'])
									@if ($defood<10)
									@php($defood=10)										
									@endif
									{{ Form::number('foodamount[]',$defood, ['min'=>1 , 'max'=>9999999,  'placeholder'=>'Masukan Jumlah Prosi',  'class' => 'form-control foodamount','id'=>'foodamount-'.$id, 'required']) }}
								</td>
								
								{{ Form::hidden('foodprice[]',$peritem, ['min'=>0,"id"=>"foodprice-".$id, 'class' => 'form-control foodprice', 'required']) }}
								{{ Form::hidden('foodtot[]',$total, ['min'=>0,"id"=>"foodtot-".$id, 'class' => 'form-control foodtot subtot', 'required']) }}
								<td class="text-right" id="totalfood-{{$id}}"> {{ H::rupiah($total) }}</td>
							</tr>
							@endfor
							
							@endfor
							@php($size=$id+count($lastdayMenu))						
							@for ($j=$id;$j<$size;$j++)
							@php($i=$j-$id)
							@php($lid=$id+1+$i)
							@if(count($foodtoform)>0)
								@php($food=(Object) $foodtoform[$lid]['food'])
								@php($total=$foodtoform[$lid]['total'] )
								@if($foodtoform[$lid]['amount']>0)
									@php($peritem=$total/$foodtoform[$lid]['amount'] )
								@endif
								@php($gtotal+=$total)
							@endif
							<tr  class="rowfood">
								<td><a href="" class="btn btn-default delete_line"><span class="fa fa-times" aria-hidden="true"></span></a>
								</td>
								<td>{{ $lastdayMenu[$i][1] }}</td>
								{{ Form::hidden('fooddate[]',$date, ["id"=>"fooddate-".$lid, 'class' => 'form-control fooddate', 'required']) }}
								{{ Form::hidden('foodtype[]',isset($foodtoform[$lid]['type']) ? $foodtoform[$lid]['type'] : $lastdayMenu[$i][1], ['min'=>0,"id"=>"foodtype-".$lid, 'class' => 'form-control foodtype', 'required']) }}
								<td align="center" width="12%">{{ Form::text('foodtime[]',isset($foodtoform[$lid]['time']) ? $foodtoform[$lid]['time'] : $lastdayMenu[$i][3], ['min'=>0,"id"=>"foodtime-".$lid, 'class' => 'form-control foodtime', 'required']) }}</td>
								<td align="center">{{ Form::hidden('foods[]',isset($foodtoform[$lid]['food']) ?  $foodtoform[$lid]['id'] : 0, ['min'=>0,"id"=>"foods-".$lid, 'class' => 'form-control foods', 'required']) }}
								<a href=""  class="pickitem" onclick="pick({{$lid}})" data-toggle="modal" data-target="#myModal{{$lastdayMenu[$i][0]}}"><div id="food-{{ $lid }}">{{ isset($foodtoform[$lid]['food']) ? $foodtoform[$lid]['label'] : "Pilih Menu" }}</div></a></td>
								<td> 
									@php(isset($foodtoform[$lid]['amount']) ? $foodtoform[$lid]['amount'] : $value['person'])
									@if ($defood<10)
									@php($defood=10)										
									@endif
									{{ Form::number('foodamount[]', $defood, ['min'=>1 , 'max'=>9999999,  'placeholder'=>'Masukan Jumlah Prosi',  'class' => 'form-control foodamount','id'=>'foodamount-'.$lid, 'required']) }}
								</td>
								{{ Form::hidden('foodprice[]',$peritem, ['min'=>0,"id"=>"foodprice-".$lid, 'class' => 'form-control foodprice', 'required']) }}
								{{ Form::hidden('foodtot[]',$total, ['min'=>0,"id"=>"foodtot-".$lid, 'class' => 'form-control foodtot subtot', 'required']) }}
								<td class="text-right" id="totalfood-{{$lid}}"> {{ H::rupiah($total) }}</td>
							</tr>
							@endfor
							
							<thead>
								<th class="font-weight-bold" colspan="5">Total</th>
								{{-- {{ Form::hidden('alltot[]',0, ['min'=>0,"id"=>"allfoodtot", 'class' => 'form-control total', 'required']) }} --}}

								<th id="aftotal" class="text-right font-weight-bold fatotal">{{ H::rupiah($gtotal) }},00</th>
							</thead>
						</tbody>
					</table>
					
					
				</div>
			</div>
		
			<div class="row">
				<div class="col-md-12">
					<h3 class="mt-4">Tambahan</h3>
					<table class="table table-bordered table-order tambahan-table">
						<thead>
							<th class="text-uppercase text-right" width="10%"></th>

							<th class="text-uppercase" width="10%">Tanggal</th>
							<th class="text-uppercase" width="35%">Item</th>
							<th class="text-uppercase" width="10%">Jumlah</th>
							<th class="text-uppercase text-right" width="35%">Total</th>
						</thead>
						<tbody>
							@php($gtotal=0)
							<tr class="rowtambahan" style="display:none">
								<td align="center">
									<a href="" class="btn btn-default delete_line"><span class="fa fa-times" aria-hidden="true"></span></a>
								</td>
								<td>{{ Form::date('tdate[]', $value['startdate'], [ 'min'=>$value['startdate'],'max'=>$enddate->format("Y-m-d") ,'class' => 'form-control tdate','id'=>'tdate-0', 'required']) }}</td>
								<td>
									{{ Form::select('tid[]',empty($addons) ? array() : $addons->pluck('name','id') ,0, ['class' => 'form-control select2 tid','id'=>'tid-0', 'required']) }}
								</td>
								<td>{{ Form::number('tamount[]', $value['person'], ['min'=>1,  'class' => 'form-control tamount','id'=>'tamount-0', 'required']) }}</td>
								{{ Form::hidden('tamtot[]',0, ['min'=>0,"id"=>"tamtot-0", 'class' => 'form-control tamtot subtot', 'required']) }}
								
								<td class="text-right" id="tamtotal-0">{{ H::rupiah(0) }}</td>
							</tr>
							@if (count($sessaddons) >0 )
								@foreach($sessaddons['id'] as $key => $addon)
								@if($key>0)
								@php($i=$key+1)
								<tr class="rowtambahan">
									<td align="center">
										<a href="" class="btn btn-default delete_line"><span class="fa fa-times" aria-hidden="true"></span></a>
									</td>
									<td>{{ Form::date('tdate[]', $sessaddons['date'][$key], [ 'min'=>$value['startdate'],'max'=>$enddate->format("Y-m-d") ,'class' => 'form-control tdate','id'=>'tdate-'.$i, 'required']) }}</td>
									<td>
										{{ Form::select('tid[]',empty($addons) ? array() : $addons->pluck('name','id') ,$addon, ['class' => 'form-control select2 tid thistory','id'=>'tid-'.$i, 'required']) }}
									</td>
									<td>{{ Form::number('tamount[]', $sessaddons['amount'][$key], ['min'=>1,  'class' => 'form-control tamount','id'=>'tamount-'.$i, 'required']) }}</td>
									{{ Form::hidden('tamtot[]',0, ['min'=>0,"id"=>"tamtot-".$i, 'class' => 'form-control tamtot subtot', 'required']) }}
									@php($total=$sessaddons['total'][$key] )
									@php($peritem=$total/$sessaddons['amount'][$key] )
									@php($gtotal+=$total)
									<td class="text-right" id="tamtotal-{{$i}}">{{ H::rupiah($total) }}</td>
								</tr>
								@endif
							@endforeach
							@endif
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5">
								<button class="btn btn-success add_tam"> <span> <i class="fa fa-plus-circle"></i></span> Tambahan</button>
								</td>
							</tr>
							<tr style="background-color: #eee;">
								<th class="font-weight-bold" colspan="4">Total</th>
								{{-- {{ Form::hidden('alltot[]',0, ['min'=>0,"id"=>"alltamtot", 'class' => 'form-control total', 'required']) }} --}}
								<th id="atotal" class="text-right font-weight-bold">{{ H::rupiah($gtotal) }},00</th>
							</tr>
						</tfoot>
					</table>
					
					
				</div>
			</div>
		
			<div class="row justify-content-end">
				<div class="col-md-2 col-lg-2 col-8 selanjutnya ">				
					{{ Form::submit('Selanjutnya', ['class' => 'btn btn-block btn-accent btn-small']) }}
				</div>
			</div>
				{{ Form::close() }}
			
			</div>
		</div>



		@include('addon.modals')

	</section>
	@stop
	@push('scripts')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script type="text/javascript">
		var price= @json($addons->pluck('price','id'))

		var snackprice= @json($foods[1]->pluck('price','id'))
		


		var that=this

		function total(){
			var tots=0;
			var subtots=0;
			$('.table-facility > tbody  > tr').each(function() {
			subtots+= isNaN(parseInt($(this).find(".subtot").val())) ? 0: parseInt($(this).find(".subtot").val());
			tots+= isNaN(parseInt($(this).find(".subtot").val())) ? 0: parseInt($(this).find(".subtot").val());
		});
		{{-- alert(subtots); --}}

			var 	number_string = subtots.toString(),       
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
			
			if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
			}
			
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		$("#myModal1").on("shown.bs.modal", function () {
			$('.snackselect').val(null).trigger('change'); 

			$('.foption').hide();
			$('#snackprice').html(priceformat(0));
			$('.picksnackmenu').prop('disabled', true);

		});
		$('.foodamount').on('blur', function(){
			if($(this).val()!=0){
				if($(this).val()<10){
					$('#noticeMeSenpai').effect( "shake", "fast" );
					$(this).val(10)
					$(this).trigger('change')
				}
			}
		});

		$("#ftotal").html(rupiah ? 'Rp. ' + rupiah + ",00": '')

			subtots=0;

			$('.table-food > tbody  > tr').each(function() {
			subtots+= isNaN(parseInt($(this).find(".subtot").val())) ? 0: parseInt($(this).find(".subtot").val());
			tots+= isNaN(parseInt($(this).find(".subtot").val())) ? 0: parseInt($(this).find(".subtot").val());
		});
				number_string = subtots.toString(),       
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
			
			if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
			}
			
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			
		$("#aftotal").html(rupiah ? 'Rp. ' + rupiah + ",00": '')
			subtots=0;

		$('.tambahan-table > tbody  > tr').each(function() {
			subtots+= isNaN(parseInt($(this).find(".subtot").val())) ? 0: parseInt($(this).find(".subtot").val());
			tots+= isNaN(parseInt($(this).find(".subtot").val())) ? 0: parseInt($(this).find(".subtot").val());
		});
				number_string = subtots.toString(),       
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
			
			if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
			}
			
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			
		$("#atotal").html(rupiah ? 'Rp. ' + rupiah + ",00": '')
		

		}

		function facilityclick(id,name,price,person,unit,image){
				{{-- alert(id+", "+price+", "+person+", "+unit+image); --}}
				var pelanggaran_tr = $('.table-facility tbody tr:first').clone();
				var next=parseInt($('.table-facility tbody tr:last').find('td').attr('id').split('-')[1])+1;
				{{-- alert(next); --}}
				pelanggaran_tr.find('#fdelete-0').attr("id","fdelete-"+next);
				pelanggaran_tr.find('#fimage-0').attr("id","fimage-"+next);
				pelanggaran_tr.find('#fname-0').attr("id","fname-"+next);
				pelanggaran_tr.find('#fstart-0').attr("id","fstart-"+next);
				pelanggaran_tr.find('#fend-0').attr("id","fend-"+next);
				pelanggaran_tr.find('#fperson-0').attr("id","fperson-"+next);
				pelanggaran_tr.find('#funit-0').attr("id","funit-"+next);
				pelanggaran_tr.find('#ftotal-0').attr("id","ftotal-"+next);
				pelanggaran_tr.find('#fsub-0').attr("id","fsub-"+next);
				pelanggaran_tr.find('#fhunit-0').attr("id","fhunit-"+next);
				pelanggaran_tr.find('#fhperson-0').attr("id","fhperson-"+next);
				pelanggaran_tr.find('#fhid-0').attr("id","fhid-"+next);
				pelanggaran_tr.find('#ftotal-0').attr("id","ftotal-"+next);
				pelanggaran_tr.find('#fprice-0').attr("id","fprice-"+next);

				pelanggaran_tr.find('#fimage-'+next).html("<img width=\"100%\" class=\"img-fluid\" src=\"{{ env('BACKEND_HOST')}}"+image+"\"/>")
				pelanggaran_tr.find('#fname-'+next).html(name+" ({{ $value['duration'] }} hari)");
				pelanggaran_tr.find('#fhid-'+next).val(id);
				pelanggaran_tr.find('#fperson-'+next).html(person+" orang");
				pelanggaran_tr.find('#fhperson-'+next).val(person);
				pelanggaran_tr.find('#funit-'+next).html(unit+" unit");
				pelanggaran_tr.find('#fhunit-'+next).val(unit);
				pelanggaran_tr.find('#fprice-'+next).html(priceformat(price));
				
				var total= price*unit*{{ $value['duration']}};
				pelanggaran_tr.find('#fsub-'+next).val(total);

				pelanggaran_tr.find('#fsub-'+next).val(total);
				pelanggaran_tr.find('#ftotal-'+next).html(priceformat(total));
				pelanggaran_tr.show();
				$('.table-facility tbody').append(pelanggaran_tr);
				this.total()
		}

		function priceformat(price){
			price=Math.ceil(price);
			var 	number_string = price.toString(),
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
			if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
			}
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return rupiah ? 'Rp. ' + rupiah+",00" : '';
		}

		

		
		$('.finput').on('change, keyup',function(e) {
				$("#tablefacilitytofill").html("<tr><td colspan='9'><center>Tekan tombol \'<span class='fa fa-search'></span>\' untuk memulai pencarian </center></td></tr>");		
			});
		
		$('.searchfacil').click(function(e) {
				e.preventDefault();

				$(".overlay").show();
				$("#tablefacilitytofill").hide();
				
				$.get("{{ route('api.get.facility')}}",{ startdate:'{{ $value['startdate'] }}', duration:{{ $value['duration'] }}, person:(isNaN(parseInt($("#jumlOrg").val())) ? 1 :  parseInt($("#jumlOrg").val()) ), unit:(isNaN(parseInt($("#jumlKam").val())) ? 0 :  parseInt($("#jumlKam").val()) ) }, function(data){
					var person=isNaN(parseInt($("#jumlOrg").val())) ? 1 :  parseInt($("#jumlOrg").val());
					var unit =isNaN(parseInt($("#jumlKam").val())) ? 1 :  parseInt($("#jumlKam").val()) ;
					$(".overlay").hide();
					$("#tablefacilitytofill").show();
					{{-- alert(data['Fasilitas']==0) --}}
					if(data['Fasilitas'].length==0){
						$("#tablefacilitytofill").html("<tr><td colspan='9'><center><i>Tidak ada unit tersedia untuk tanggal {{ $value['startdate'] }} selama {{ $value['duration'] }} hari dengan "+unit+" unit berisi "+person+" orang  </i></center></td></tr>");

					}
					else{
						var str="";
						$.each(data.Fasilitas, function( index, value ) {
							var capacity=value.capacity==null ? '-'  : value.capacity;
							str+="<tr><td><img width='100%'' class='img-fluid' src='{{ env('BACKEND_HOST')}}"+value.villa_image+"'/></td><td width='15%'>"+value.name+"</td><td class='price'>"+priceformat(value.price)+"</td><td>"+value.minimum+"</td><td>"+capacity+"</td><td><button class='btn btn-accent btn-smaller-font'  data-dismiss='modal' onclick='facilityclick(\""+value.id_morph+"\",\""+value.name+"\","+value.price+","+person+","+unit+",\""+value.villa_image+"\")'>Book</button></td></tr>";
						});
						$("#tablefacilitytofill").html(str);
						
					}
				
				}).fail(function() {
					$(".overlay").hide();
					$("#tablefacilitytofill").show();
					$("#tablefacilitytofill").html("<tr><td colspan='9'><center><i>Pengunjung harus lebih banyak dari unit yang dipesan</i></center></td></tr>");

				});
		});

		function pick(id){
				{{-- alert(id);  --}}
				var next=id;
				$(".id_pick").val(id);
				{{--  $(".pick").attr("onclick","pick("++")");  --}}
				
			}
		$(document).ready(function() {
			$("[data-toggle='modal']").tooltip({placement: 'top',trigger: 'manual'}).tooltip('show');
			$("[data-toggle='modal']").on('click',function(){$("#info").tooltip('hide');});

			$(' .foodamount').bind('keyup change', function (e){
			var disid=$(this).attr("id").split("-")[1];  
			{{-- alert($('#foodprice-'+disid).val()); --}}
			var tot=$('#foodprice-'+disid).val(); 
			var amount=(isNaN(parseInt($("#foodamount-"+disid).val())) ? 1 : parseInt($("#foodamount-"+disid).val()))
			if(amount==0){
				amount=1;
			}
			tot =  tot * amount;
			$('#foodtot-'+disid).val(tot);       
			
			var 	number_string = tot.toString(),
			split	= number_string.split(','),
			sisa 	= split[0].length % 3,
			rupiah 	= split[0].substr(0, sisa),
			ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
			
			if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
			}
			
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			
			$("#totalfood-"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
			total();
		});
		$('.picksnackmenu').on('click',function(e){
				var main_id=$('.snackselect').val();

				var data=$('.snackselect').select2('data');
				var fsid1=$('#foodsnack1-'+main_id).val();
				var fsid2=$('#foodsnack2-'+main_id).val();
				var f1name=$('#foodsnack1-'+main_id).select2('data')[0].text;
				var f2name=$('#foodsnack2-'+main_id).select2('data')[0].text;
				var finalname="";
				if(f1name!="undefined"){
					if(f1name==f2name){
						finalname=f1name+"(2)"
					}
					else{
						finalname=f1name+", "+f2name

					}
				}
				
				var pick_id=fsid1+"|"+fsid2;
				var pick_price=snackprice[main_id];
				
				var pick_name=data[0].text+" - "+finalname;
						$("#foods-"+$(".id_pick").val()).val(pick_id);
						$("#food-"+$(".id_pick").val()).html(pick_name);
						$("#foodprice-"+$(".id_pick").val()).val(pick_price);
						var disid=$(".id_pick").val();
						var amount=(isNaN(parseInt($("#foodamount-"+disid).val())) ? 1 : parseInt($("#foodamount-"+disid).val()));
						if(amount==0){
							amount=1;
						}
						var tot = pick_price*amount;
						
						$("#foodtot-"+$(".id_pick").val()).val(tot); 
						var 	number_string = tot.toString(),
						split	= number_string.split(','),
						sisa 	= split[0].length % 3,
						rupiah 	= split[0].substr(0, sisa),
						ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
						if (ribuan) {
							separator = sisa ? '.' : '';
							rupiah += separator + ribuan.join('.');
						}
						rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
						$('#totalfood-'+$(".id_pick").val()).html(rupiah ? 'Rp. ' + rupiah : '');
				total();
		});
		$('.tambahan-table tbody').on('click', '.delete_line', function (e) {
				e.preventDefault();
				$(this).closest('.rowtambahan').remove();
				total();
		});

		$('.table-food tbody').on('click', '.delete_line', function (e) {
				e.preventDefault();
				$(this).closest('.rowfood').find('.foods').val(0)
				var ids= $(this).closest('.rowfood').find('.foods').attr("id").split("-")[1];
				$(this).closest('.rowfood').find('#food-'+ids).html("Pilih Menu");
				$(this).closest('.rowfood').find('.foodprice').val(0);
				$(this).closest('.rowfood').find('.foodtot').val(0);
				var tot = 0;
				var 	number_string = tot.toString(),
				split	= number_string.split(','),
				sisa 	= split[0].length % 3,
				rupiah 	= split[0].substr(0, sisa),
				ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				$(this).closest('.rowfood').find('.foodtot').val(0);
				
				$('#totalfood-'+ids).html(rupiah ? 'Rp. ' + rupiah : '');
				total();
		});

		$('.table-facility tbody').on('click', '.delete_line', function (e) {
				e.preventDefault();
				$(this).closest('.rowfacility').remove();
				total();			  
		});
		
		$('.cancel-selection').click(function(e){
				$("#foods-"+$(".id_pick").val()).val(0);
				$("#food-"+$(".id_pick").val()).html('Pilih Menu');
				$("#foodprice-"+$(".id_pick").val()).val(0);
				var tot = 0;
				$("#foodtot-"+$(".id_pick").val()).val(tot); 
				var 	number_string = tot.toString(),
				split	= number_string.split(','),
				sisa 	= split[0].length % 3,
				rupiah 	= split[0].substr(0, sisa),
				ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
				if (ribuan) {
					separator = sisa ? '.' : '';
					rupiah += separator + ribuan.join('.');
				}
				rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
				$('#totalfood-'+$(".id_pick").val()).html(rupiah ? 'Rp. ' + rupiah : '');
				total();
				
		});
		$('.snackselect').select2({'width': '100%','placeholder':'Pilih Paket'});
		$('.foodsnack1').select2({'width': '100%','placeholder':'Pilih Snack'});
		$('.foodsnack2').select2({'width': '100%','placeholder':'Pilih Snack'});
		
		$('.snackselect').on("select2:select", function(e) {
			$('.foption').hide();
			$("#foodoption-"+$(this).val()).show();
			$('#snackprice').html(priceformat(snackprice[$(this).val()]));
			var fsid1=$('#foodsnack1-'+$(this).val()).val();
			if(fsid1!=null)
				$('.picksnackmenu').prop('disabled', false);
			else
				$('.picksnackmenu').prop('disabled', true);

		});
		$('.snackselect').val(null).trigger('change');
				$('.thistory').select2({
					'width': '100%'});
				$('.thistory').on("select2:select", function(e) {
						var disid=$(this).attr("id").split('-')[1];  
					
					var tot = price[$(this).val()]* (isNaN(parseInt($("#tamount-"+disid).val())) ? 1 : parseInt($("#tamount-"+disid).val()));
					{{--  alert(tot)  --}}
					$("#tamtot-"+disid).val(tot);
						var 	number_string = tot.toString(),
							split	= number_string.split(','),
							sisa 	= split[0].length % 3,
							rupiah 	= split[0].substr(0, sisa),
							ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
							
						if (ribuan) {
							separator = sisa ? '.' : '';
							rupiah += separator + ribuan.join('.');
						}
						
						rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
						
					$("#tamtotal-"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
				total();
						
				});
				$('.thistory').trigger("select2:select");
				$('.tamount').on('change keyup',function (e){
					var disid=parseInt($(this).closest('.rowtambahan').find('select.select2').attr('id').split('-')[1]);     
					var disval=parseInt($(this).closest('.rowtambahan').find('select.select2').val());  
					if($(this).val()<=0){
					var tot = price[disval]*1;
					} 
					else{
					var tot = price[disval]*$(this).val();
					}
					$("#tamtot-"+disid).val(tot);
						var 	number_string = tot.toString(),
							split	= number_string.split(','),
							sisa 	= split[0].length % 3,
							rupiah 	= split[0].substr(0, sisa),
							ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
							
						if (ribuan) {
							separator = sisa ? '.' : '';
							rupiah += separator + ribuan.join('.');
						}
						
						rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
						
						$("#tamtotal-"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
				total();
					
				});    


		$('.add_tam').click(function(e) {
				e.preventDefault();     
				var pelanggaran_tr = $('.tambahan-table tbody tr:first').clone();
				pelanggaran_tr.show();
				pelanggaran_tr.find('.delete_line').show();
				var next=parseInt($('.tambahan-table tbody tr:last').find('select.select2').attr('id').split('-')[1])+1;
				pelanggaran_tr.find('.select2-container').remove();
				pelanggaran_tr.find('select.select2').attr("id","tid-"+next);
				pelanggaran_tr.find('.tamount').val(1);
				pelanggaran_tr.find('#tamtotal-0').attr("id","tamtotal-"+next);
				pelanggaran_tr.find('.tamtot').attr("id","tamtot-"+next);
				pelanggaran_tr.find('.tamount').attr("id","tamount-"+next);
				
				pelanggaran_tr.find('.tdate').attr("id","tdate-"+next);
				pelanggaran_tr.find('select.select2').select2({
					'width': '100%'});
				
				$('.tambahan-table tbody').append(pelanggaran_tr);
				
				$('.tid').on("select2:select", function(e) {
						var disid=$(this).attr("id").split('-')[1];  
					
					var tot = price[$(this).val()]* (isNaN(parseInt($("#tamount-"+disid).val())) ? 1 : parseInt($("#tamount-"+disid).val()));

					$("#tamtot-"+disid).val(tot);
						var 	number_string = tot.toString(),
							split	= number_string.split(','),
							sisa 	= split[0].length % 3,
							rupiah 	= split[0].substr(0, sisa),
							ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
							
						if (ribuan) {
							separator = sisa ? '.' : '';
							rupiah += separator + ribuan.join('.');
						}
						
						rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
						
					$("#tamtotal-"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
				total();
						
				});
				$('.tid').trigger("select2:select");
				$('.tamount').on('change keyup',function (e){
					var disid=parseInt($(this).closest('.rowtambahan').find('select.select2').attr('id').split('-')[1]);     
					var disval=parseInt($(this).closest('.rowtambahan').find('select.select2').val());  
					if($(this).val()<=0){
					var tot = price[disval]*1;
					} 
					else{
					var tot = price[disval]*$(this).val();
					}
					$("#tamtot-"+disid).val(tot);
						var 	number_string = tot.toString(),
							split	= number_string.split(','),
							sisa 	= split[0].length % 3,
							rupiah 	= split[0].substr(0, sisa),
							ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
							
						if (ribuan) {
							separator = sisa ? '.' : '';
							rupiah += separator + ribuan.join('.');
						}
						
						rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
						
						$("#tamtotal-"+disid).html(rupiah ? 'Rp. ' + rupiah : ''); 
				total();
					
				});    
			});

			$('.add_line').click(function(e) {
				e.preventDefault();               
					var pick_id=$(this).attr("id").split("-")[1];
				var pick_price=$(this).attr("data-price");
				var pick_name=$(this).attr("data-name");
						$("#foods-"+$(".id_pick").val()).val(pick_id);
						$("#food-"+$(".id_pick").val()).html(pick_name);
						$("#foodprice-"+$(".id_pick").val()).val(pick_price);
						var disid=$(".id_pick").val();
						var amount=(isNaN(parseInt($("#foodamount-"+disid).val())) ? 1 : parseInt($("#foodamount-"+disid).val()));
						if(amount==0){
							amount=1;
						}
						var tot = pick_price*amount;
						
						$("#foodtot-"+$(".id_pick").val()).val(tot); 
						var 	number_string = tot.toString(),
						split	= number_string.split(','),
						sisa 	= split[0].length % 3,
						rupiah 	= split[0].substr(0, sisa),
						ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
						if (ribuan) {
							separator = sisa ? '.' : '';
							rupiah += separator + ribuan.join('.');
						}
						rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
						$('#totalfood-'+$(".id_pick").val()).html(rupiah ? 'Rp. ' + rupiah : '');
				total();
						
				});
		});
	</script>
	@endpush

