
	<!-- Modal -->
	<div id="myModal1" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title pull-left">Pilih Snack</h4>

					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			{{-- <input type="text" id="Search_All"> --}}
				{!! Form::hidden('', '', ['class'=>'id_pick']) !!}
				<div class="modal-body">
				<br>
				<div class="row">
					<div class="form-group col-md-6">
					{{ Form::label('paket','Paket Snack') }}
					{{ Form::select('paket', empty($foods[1]) ? array() : $foods[1]->pluck('name','id') ,0, ['class' => 'form-control select2 select2class snackselect', 'required']) }}
					
				</div>
				<div class="form-group col-md-6">
					{!! Form::hidden('', '', ['class'=>'snackprice']) !!}
					<h4><div style="margin-top:20px" id="snackprice">Rp.0,00</div></h4>
				</div>
				</div>
				
				@foreach($foods[1] as $f => $food)  
				@if (count($food->menu)>0)
				<br>
				<div class="row foption"  id="foodoption-{{ $food->id }}" style='display:none'>
					<div class="col-md-12">
					<h4>{{ $food->name }}</h4>
					</div>
						<div class="form-group col-md-6">
							{{ Form::label('col_name','Snack 1') }}
							{{ Form::select('col_name', empty($food->menu) ? array() : $food->menu->pluck('name','id') ,0, ['class' => 'form-control select2 select2class foodsnack1','id'=>'foodsnack1-'.$food->id, 'required']) }}
							@if ($errors->has('col_name'))
								<div class="help-block text-red">
									{{ $errors->first('col_name') }}
								</div>
							@endif
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('col_name','Snack 2') }}
							{{ Form::select('col_name', empty($food->menu) ? array() : $food->menu->pluck('name','id') ,0, ['class' => 'form-control select2 select2class foodsnack2', 'id'=>'foodsnack2-'.$food->id, 'required']) }}
							@if ($errors->has('col_name'))
								<div class="help-block text-red">
									{{ $errors->first('col_name') }}
								</div>
							@endif
						</div>
				</div>
				@else
				<br>
				<div class="row foption" id="foodoption-{{ $food->id }}" style='display:none'>
					<div class="col-md-12">
						<h4><i>Menu Tidak Tersedia</i></h4>
					</div>
				</div>
				@endif
				
							@endforeach       
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-accent picksnackmenu" data-dismiss="modal" disabled>Pilih Menu</button>

				</div>
			</div>

		</div>
	</div>

	<div id="myModal0" class="modal fade" role="dialog" >
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title pull-left">Pilih Makanan</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			{{-- <input type="text" id="Search_All">	   --}}
				{!! Form::hidden('', '', ['class'=>'id_pick']) !!}
				<div class="modal-body">
					
				
				@foreach($foods[0] as $f => $food)  
				@if (count($food->menu)>0)

				<br>
				<div class="row">
					<div class="col-md-12">
				<h4>{{ $food->name }}</h4>
					</div>
					<div class="col-md-12">
						<table class="table">
							<thead>
							<tr>
								<th style="width: 20%;">Menu</th>
								{{-- <th style="width: 30%;">Harga</th> --}}
								<th style="width: 20%;">Harga</th>
								<th style="width: 30%;">Makanan</th>
								<th style="width: 30%;">Action</th>
							</tr>
							</thead>
							<tbody>         
								@foreach($food->menu as $k => $v)  
								<tr>
									<td>{{ $v->name }}</td>
									<td>{{ H::rupiah($food->price) }}</td>
									<td>{{ $v->description_label }}</td>
									<td><button data-dismiss="modal" data-price={{ $food->price}} data-name='{{ $food->name }} - {{ $v->name}}' id="btn-{{ $v->id }}" class="add_line btn btn-primary">Pilih Menu</button></td>
								</tr>     
								@endforeach        
							</tbody>
						</table>
					</div>
				</div>
				@endif
							@endforeach       
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="myModalFacility" class="modal fade" role="dialog" style="z-index:1151 !important;">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title pull-left">Tambah Fasilitas</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			{{-- <input type="text" id="Search_All">	   --}}

				<div class="modal-body" id="facilityBody">
				<div class="row">
									<div class="col-md-5">
											<div class="form-group col-md-12">
													{{ Form::label('jumlOrang','Jumlah Orang') }}
													{{ Form::number('jumlOrang',$value['person'], [ 'class' => 'form-control finput','id'=>'jumlOrg']) }}
													@if ($errors->has('jumlOrang'))
															<div class="help-block text-red">
																	{{ $errors->first('juml') }}
															</div>
													@endif
											</div>
											
									</div>   
									<div class="col-md-5">
											<div class="form-group col-md-12">
													{{ Form::label('jumlKam','Jumlah Unit') }}
													{{ Form::number('jumlKam',$value['unit'], [ 'class' => 'form-control finput' ,'id'=>'jumlKam']) }}
													@if ($errors->has('jumlKam'))
															<div class="help-block text-red">
																	{{ $errors->first('juml') }}
															</div>
													@endif
											</div>
									</div>    
									<div class="col-md-2">
											<div class="form-group col-md-12">
											</div>
											<div class="form-group col-md-12">
													<button class="btn btn-success searchfacil"> <span class="fa fa-search"></span></button>
											</div>
									</div>    
							</div>
							<table class="table myfacilitytable">
						<thead>
							<tr>
								<th style="width: 30%;" colspan="2">Fasilitas</th>
								{{-- <th style="width: 30%;">Harga</th> --}}
								<th style="width: 20%;">Harga/Hari</th>
								<th style="width: 5%;">Minimum</th>
								<th style="width: 5%;">Kapasitas</th>
								<th style="width: 10%;">Action</th>
							</tr>
							</thead>
							<tbody style="display:none;" id="tablefacilitytofill" class="animate-bottom">         
								<tr>
								</tr>
							</tbody>
						</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

