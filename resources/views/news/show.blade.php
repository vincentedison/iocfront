@extends('layouts.app')
@push('styles')
<style>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
.container [class*="col-"] img {
    /* max-width: 100%; */
}

.blog-post div img, .blog-post .post img, .blog-post .post iframe, .blog-sizngle-post .post img, .blog-single-post.post iframe {
    /* width: auto; */
}
.blog-post img {
    background: #fff;
}
.blog-single-post p, .blog-post div, .blog-single-post div, .blog-single-post .post img, .blog-single-post p img, .row-wrapper-x div.wp-caption, .row-wrapper-x p, .row-wrapper-x img {
    /* max-width: 100%; */
}
.row-wrapper-x p img, div img, li img, #wrap p img, .wpb_single_image.wpb_content_element img {


    max-width: 100%;
}
img {
    vertical-align: middle;
}


.blgtyp2.blog-post h6 {
    color: #a1a5ad;
}
h6.blog-cat, h6.blog-author, .search-results h6.blog-date, .search-results h6.blog-comments {
    letter-spacing: 0;
    font-size: 12px;
    display: inline;
    float: none;
}
.postmetadata h6 {
    /* letter-spacing: 0; */
    /* font-size: 13px; */
    font-weight: 300;
    display: inline-block;
}
h6.blog-date {
    font-weight: 400;
    font-size: 12px;
    letter-spacing: 0;
}
h6.blog-date {
    padding-left: 4px;
    color: #de2f68;
    
}
.blgtyp2.blog-post h6  {
    color: #a1a5ad;
}
h6.blog-cat a, h6.blog-date a {
    color: #de2f68;
    display: inline-block;
}

.blog-post a, .blog-line a {
    /* color: #000; */
    -webkit-transition: color 0.18s ease;
    -moz-transition: color 0.18s ease;
    -o-transition: color 0.18s ease;
    transition: color 0.18s ease;
}

a, a:visited {
    /* color: #0099ff; */
    text-decoration: none;
    outline: 0;
}
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
    /* font-weight: inherit; */
}
.blog-post.blgtyp2 h3 {
    font-size: 22px;
    line-height: 1.4;
    margin-bottom: 12px;
    padding-top: 5px;
}

.blog-post h3, h3 a {
    font-size: 22px;
    color: #171c24;
    font-weight: 600;
    line-height: 1.3;
    letter-spacing: -1px;
}

a.readmore {
    position: relative;
    text-align: left;
    display: inline-block;
    letter-spacing: 2px;
    font-size: 11px;
    line-height: 10px;
    font-weight: 600;
    text-transform: uppercase;
    padding: 16px 25px 14px;
    border-radius: 60px;
    border: 2px solid #e5e5e5;
    color: #de2f68;
    margin-bottom: 12px;
    z-index: 1;
    transition: all .22s ease-in-out;
}
.news{
    margin-bottom:90px;
}
.subtitle-wrap {
    position: relative;
    margin: 20px 0;
    line-height: 18px;
}
.subtitle-wrap:before {
    content: '';
    width: 100%;
    height: 3px;
    background-color: #e6e6e6;
    display: block;
    margin-bottom: -15px;
}

*:before, *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
 h4.subtitle {
    border: none;
    color: #101010;
    margin: 0;
    padding: 0;
    font-size: 16px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 0;
    line-height: 28px;
    background: #fff;
    padding-right: 5px;
    display: inline-block;
}
.widget  li a, #footer.litex .footer-in .widget ul li a {
    text-decoration: none;
    color: #444;
    padding: 6px 2px 8px 0;
    -webkit-transition: all .07s ease;
    -moz-transition: all .07s ease;
    -ms-transition: all .07s ease;
    -o-transition: all .07s ease;
    transition: all .07s ease;
}

.widget  li {
    padding: 0;
    margin-bottom:10px;
    list-style: none;
    letter-spacing: 0;
}
.widget  a:hover{
    color: #de2f68;
}
.widget ul {
    padding:0px;
    list-style: inside;
    list-style-type: initial;
    list-style-position: inside;
    list-style-image: initial;
}

/*.widget ul li.cat-item a:before {
    font-family: 'FontAwesome';
    color: #0db0ee;
    font-size: 16px;
    content: "\f105";
    padding: 10px;
}*/

*:before, *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.widget ul li.cat-item {
    margin:0px;
    min-height: 40px;
    padding-top:10px;
    padding-bottom:10px;
    border-bottom: 1px solid #f1f1f1;
}

.widget ul li.cat-item:hover {
    background-color: #f7f7f7;
}

.sidebar .widget ul li, #footer.litex .widget ul li {
    /* color: #91979d; */
    /* font-size: 13px; */
    /* padding-top: 5px; */
    /* padding-bottom: 5px; */
}

h1.post-title {
    margin-top: 25px;
    margin-bottom: 30px;
    color: #1f2123;
    font-size: 40px;
    font-weight: 600;
    line-height: 1.3;
    letter-spacing: -1px;
}
p {
    margin: 0 0 20px 0;
    color: #616161;
    font-size: 15px;
    /* line-height: 1.8; */
}
input[type="text"].search-side, input[type="text"].header-saerch {
    background: #fcfcfc url(../images/find-ico1.png) no-repeat right;
}
.sidebar input[type="text"].search-side, .widget input[type="text"].search-side {
    width: 100%;
    padding-left: 15px;
    border: 1px solid #f1f1f1;
    border-radius: 0;
    background-color: #fbfbfb;
    font-size: 12px;
    height: 45px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
</style>
@endpush

@section('content')

{{--  <section class="title-section bg-light-grey2">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9">
       <h2 class="title dark-color">News</h2>
     </div>  
   </div>
 </div>
</section>  --}}


<section class="title-section">
  <div class="container">
      <div class="row justify-content-md-around">
                  <div class="col-md-8 mb-3">
                  <h1 class="post-title">{{$news->title}}</h1>
            <div class="row">
                <div class="col-md-12" >
                    <img src="{{ env('BACKEND_HOST').'image/'.$news->image }}" alt="details of seventh Hotella Hotel branch in Italy." class="img-fluid">                      
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    {!! $news->content !!}                            
                </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="row">
                <div class="col-md-12" >
                    <div class="form-group">
                    <div class="subtitle-wrap"><h4 class="subtitle">Cari Artikel</h4></div>
                        {{ Form::text('title',!empty($filter['title']) ? $filter['title'] : '' , ['Placeholder'=>"Cari Artikel", 'class' => 'search-side live-search form-control']) }}                        
                    </div>
                </div>
                <br>
                <div class="col-md-12 widget" >
                    <div class="subtitle-wrap"><h4 class="subtitle">Artikel Terbaru</h4></div>
                   <ul>
                    @foreach($latest as $new )
                        <li><a href="{{route('news.show',['id'=>$new->slug])}}">{{$new->title}}</a></li>
                    @endforeach
                   </ul>
                </div>
                <div class="col-md-12 widget" >
                    <div class="subtitle-wrap"><h4 class="subtitle">Kategori</h4></div>
                   <ul>
                    @foreach($cats as $cat )
                    <li class="cat-item cat-item-6"><a href="{{ route('news.index') }}?filter[categories]={{$cat}}"><i class="fas fa-angle-right accent-color pr-2"></i>{{ $cat}}</a>
                    </li>
                        {{--  <li><a href="{{route('news.show',['id'=>$new->slug])}}">{{$new->title}}</a></li>  --}}
                    @endforeach
                   </ul>
                </div>
            </div>
          </div>
  
        </div>        
                                    
    {{--  @php($col=3)
    @foreach($news as $key => $facs)
      @if($key%$col==0)                 
      <div class="row text-center">
        @endif
        <div class="col-md-4">
          <a href="{{route($types.'.show',['villa'=>$facs->slug])}}">
            <div class="villa-grid-item">
              <div class="room-grid-item-figure">
                <img class="img-fluid" src="{{asset($facs->villa_image)}}" alt="{{ $facs->name }}">
              </div>
              <div class="entryroom-grid-content">
                <h2 class="title dark-color px-2 mt-0">{{$facs->name}}</h2>
                <p class="caption-content font-weight-light mb-0">{!! H::rupiah($facs->price) !!}/hari</p>
              </div>
            </div>
          </a>
        </div>
      @if($key%$col==$col-1)  --}}
      {{--  </div>
      @endif
    @endforeach  --}}
    </div>
  </div>
</section>

@endsection

@push('scripts')

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script type="text/javascript">
    $('.search-side').on('keyup', function(e){
        if (event.keyCode == 13) {
             window.location.replace("{{ route('news.index')}}?filter[title]="+$(this).val());
        }
    }); 

  </script>
@endpush
