@extends('layouts.app')
@push('styles')
<style>
</style>
@endpush

@section('content')

<section class="title-section bg-light-grey0">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9">
       <h2 class="title dark-color">{{$education_tour->title}}</h2>
     </div>  
   </div>
 </div>
</section>


<section class="content-section">
  <div class='container'>
    <div class="row align-items-center">
      <div class="col-md-7">         
        <div id="myCarousel" class="carousel slide" style="text-align: center" width="100%"  height="550px" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            @php($i=0)      
            @foreach($education_tour->gallery as $gal)    
            <li data-target="#myCarousel" data-slide-to="{{$i}}" class="{{ $i==0 ? 'active' : '' }}"></li>
            @php($i++)
            @endforeach
          </ol>
          <div class="carousel-inner">
            @php($i=0)
            @foreach($education_tour->gallery as $gal)      
            <div class="item {{ $i==0 ? 'active' : '' }}">
              @php($i++)        
              <img class="items img-fluid w-100" src="{{ env('BACKEND_HOST').'image/'.$gal->image_path }}" alt="{{ $education_tour->name }}">
            </div>
            @endforeach
          </div>
          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div class="col-md-5 facility-content">

        <div class="facility-description mb-1" style="height: 100%">
        {!!$education_tour->description !!}
        </div>

        
      </div>
    </div>
  </div>
</section>




@stop
