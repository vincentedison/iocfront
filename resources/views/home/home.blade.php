@extends('layouts.app')
@push('styles')

  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

  <!-- <style>
    .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img{
      height:550px;
    }
    .img{
      width:100%;
    }
    .text{
      margin-left:10px;
    }
    ul {
      list-style-type: none;
    }
    .round {
      border-radius: 50%;
    }
    .margins{
      margin-bottom: 10px;
    }
  </style> -->
@endpush

@section('content')

<div id="carouselIoc" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    @foreach($batas as $i => $ban)
    <div class="carousel-item {{ $i==0? 'active' : ''}}">
      <img class="d-block w-100" src="{{ env('BACKEND_HOST').'image/'.$ban->image_desktop_path }}" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="sub-title playfair-font font-italic accent-color mb-0">{{$ban->subtitle}}</h2>
        <h1 class="title font-weight-bold light-color mt-0">{{$ban->title}}</h1>
        <p class="caption-content roboto-font font-weight-light light-color">{{$ban->description}}</p>
        <a href="{{$ban->link_url}}" class="btn btn-ioc btn-banner btn-accent">{{$ban->link_text}}</a>
      </div>
    </div>
    @endforeach
    <!-- <div class="carousel-item">
      <img class="d-block w-100" src="{{ env('BACKEND_HOST').'/image/banner_swimming_pool.jpg' }}" alt="Second slide">
    </div> -->
  </div>
  <a class="carousel-control-prev" href="#carouselIoc" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselIoc" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<section class="check-booking bg-light-grey">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9">
      <div class="title-plus-text">
        <h3 class="sub-title playfair-font font-italic grey-color">Cara mudah untuk</h3>
        <h2 class="title font-weight-light dark-color">Melakukan <span class="accent-color font-weight-bold ">Booking </span></h2>
        <p class="caption-content roboto-font ">Cukup isi kolom di bawah ini, dan pilih ruangan yang Anda inginkan melalui sistem booking online kami.</p>
      </div>
    </div>
    <div class="col-md-10 mt-5">
      {!! Form::open(['route' => ['facilities.index'], 'method'=>'GET', 'enctype'=>'multipart/form-data','role' => 'form']) !!}

      <div class="row">
        <div class="col-6 col-md-3 px-less">
          {!! Form::date('startdate',date("Y-m-d"), ['placeholder' => 'Mulai Tanggal','class'=> 'form-control margins','required' => 'required']) !!}
        </div>
        <div class="col-6 col-md-2 px-less">
          {!! Form::number('duration',null, ['placeholder' => 'Durasi','class'=> 'form-control margins','required' => 'required']) !!}
        </div>
        <div class="col-6 col-md-2 px-less margins" style="margin-bottom: 10px;">
          {!! Form::number('person',null, ['placeholder' => 'Jumlah Tamu/Peserta','class'=> 'form-control personformfilter','required' => 'required']) !!}
        </div>
        <div class="col-6 col-md-2 px-less  margins">
          {!! Form::number('unit',null, ['placeholder' => 'Jumlah Unit','class'=> 'form-control unitformfilter margins','required' => 'required']) !!}
        </div>
        <div class="col-6 col-md-3 px-less">
          {!! Form::select('type', $type, null, ['placeholder' => 'Tipe','class'=> 'form-control select2 select2-check-booking','required' => 'required']) !!}
        </div>
      </div>
      <div class="row justify-content-md-center btn-cek-ketersediaan">        
        <div class="col-md-4">
          <button type="submit " class="btn btn-accent btn-big searchbutton">Cek Ketersediaan</button>
        </div>
      </div>
      {!! Form::close() !!}

    </div>
  </div>
</div>
</section>

<section class="value">
  <div class="container">
    <div class="row justify-content-md-center text-center">
      <div class="col-md-9">
        <div class="title-plus-text">
          <h3 class="sub-title playfair-font font-italic grey-color">Sekilas tentang</h3>
          <h2 class="title font-weight-light dark-color">Integrated Outdoor <span class="accent-color font-weight-bold ">Campus</span></h2>
          <p class="caption-content roboto-font ">Integrated Outdoor Campus(IOC) Universitas Surabaya terletak di di Kecamatan Trawas Mojokerto. Sebagai bagian yang tak terpisahkan dari IOC, secara khusus telah dibangun Ubaya Training Centre (UTC). UTC menyediakan berbagai akomodasi yang dibanguin di tengah-tengah alam dengan lingkungan yang sejuk untuk berbagai kebutuhan Anda.</p>
          <!-- <p class="caption-content roboto-font ">Integrated Outdoor Campus(IOC) Universitas Surabaya terletak di di Kecamatan Trawas Mojokerto. Sebagai bagian yang tak terpisahkan dari IOC, secara khusus telah dibangun Ubaya Training Centre (UTC). UTC adalah sebuah kompleks hunian yang lengkap guna memfasilitasi berlangsungnya berbagai pelatihan berupa: barak dan cottage, ruang diskusi, camping ground, multi-function hall, canteen, amphitheatre, sarana outward bond, kebun tanaman TOGA dan rumah composting.</p> -->
          <!-- <h3 class="sub-title playfair-font font-italic grey-color">Sebuah</h3>
          <h2 class="title font-weight-light dark-color">Tempat untuk <span class="accent-color font-weight-bold ">bertumbuh</span></h2> -->
          <!-- <p class="caption-content roboto-font ">Dilengkapi dengan lingkungan dan fasilitas yang mendukung, IOC akan membantu kelangsungan event Anda.</p> -->
          
        </div>
      </div>
      <div class="col-md-12 mt-5 icon-box">
        <div class="row">         
          <div class="col-md-4">                       
            <h2 class="title dark-color">Lingkungan Sejuk</h2>
            <p class="roboto-font font-weight-light px-4">Perubahan suasana yang menyenangkan dari kesibukan sehari-hari Anda.</p>
          </div>

          <div class="col-md-4">                       
            <h2 class="title dark-color">Edukasi Alam</h2>
            <p class="roboto-font font-weight-light px-4">Kenali alam lebih dalam dengan berjalan-jalan melalui kebun dan koleksi tanaman kami.</p>
          </div>

          <div class="col-md-4">                       
            <h2 class="title dark-color">Fasilitas Lengkap</h2>
            <p class="roboto-font font-weight-light px-4">Tidak perlu khawatir, kami menyediakan berbagai ruang dan fasilitas untuk memenuhi kebutuhan Anda.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<section class="villas bg-light-grey">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9 mb-5">
      <div class="title-plus-text">
        <h3 class="sub-title playfair-font font-italic grey-color">Temukan berbagai</h3>
        <h2 class="title font-weight-light dark-color">Pilihan <span class="accent-color font-weight-bold ">Akomodasi</span></h2>
        <p class="caption-content roboto-font ">Cocok bahkan untuk rombongan besar, IOC siap untuk memenuhi kebutuhan Anda dengan barrack-style cottage kami.</p>
      </div>
    </div>  
  </div>                                    
    @php($col=3)
    @foreach($villas as $key => $facs)
      @if($key%$col==0)                 
      <div class="row text-center">
        @endif
        <div class="col-md-4">
          <a href="{{route('villas.show',['villa'=>$facs->slug])}}">
            <div class="villa-grid-item">
              <div class="room-grid-item-figure">
                <img class="img-fluid" src="{{ env('BACKEND_HOST').$facs->villa_image }}" alt="{{ $facs->name }}">
              </div>
              <div class="entryroom-grid-content">
                <h2 class="title dark-color px-2 mt-0">{{$facs->name}}</h2>
                <p class="caption-content font-weight-light mb-0">{!! H::rupiah($facs->weekend_price) !!}/hari</p>
              </div>
            </div>
          </a>
        </div>
      @if($key%$col==$col-1)
      </div>
      @endif
    @endforeach
    </div>
  </div>
</section>

<section class="testimonial dark-section">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9 mb-5">
      <div class="title-plus-text">
        <h3 class="sub-title playfair-font font-italic white-color">Beberapa</h3>
        <h2 class="title font-weight-light white-color">Kesan dan  <span class="accent-color font-weight-bold ">Testimoni</span></h2>
        <p class="caption-content roboto-font white-color">Berbagai pengalaman menyenangkan yang didapat oleh tamu kami.</p>
      </div>
     </div> 
    </div>                                       
    @php($col=2)
    @foreach($testi as $key => $testimoni)
      @if($key%$col==0)                 
        <div class="row text-center justify-content-around">
      @endif
        <div class="col-md-5 tc-col text-left">
          <p class="tc-content roboto-font font-weight-light white-color mb-0">{!!$testimoni->content!!}</p>

          <div class="tc-footer">
            <p class="tc-name mb-0">{{$testimoni->name}}</p>
          </div>
        </div>
      @if($key%$col==$col-1)
        </div>
      @endif
    @endforeach
      </div>
    </div>
  </div>
</div>
</section>

<section class="facility">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9 mb-5">
      <div class="title-plus-text">
        <h3 class="sub-title playfair-font font-italic grey-color">Lengkap dengan</h3>
        <h2 class="title font-weight-light dark-color">Berbagai macam <span class="accent-color font-weight-bold ">fasilitas</span></h2>
        <p class="caption-content roboto-font ">IOC memiliki banyak fasilitas untuk mendukung aktivitas Anda. Cocok untuk gathering perusahaan, training anggota, dan rekreasi keluarga.</p>
      </div>
    </div>  
  </div>      
  <div class='row justify-content-center'>
    <div class='col-md-10'>
      <div id="carouselIocFacility" class="carousel slide vertical" data-ride="carousel">
        <div class="parents">
        <ol class="child carousel-indicators">
          <?php $counter = 0; ?>
          @foreach($facility as $i => $fa)            
            @if ($fa->gallery()->get()->first())
            <li data-target="#carouselIocFacility" data-slide-to="{{ $counter }}" class="{{ $counter==0 ? 'active' : '' }}"></li>
            <?php $counter++; ?>
            @endif
            
          @endforeach
          @if($sp)
            <li data-target="#carouselIocFacility" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>
          @endif
        </ol>
        </div>
        <div class="carousel-inner">
          @foreach($facility as $i => $fa)  
            @if ($gal = $fa->gallery()->get()->first())
            <div class="carousel-item {{ $i==0 ? 'active' : '' }}">
              <img class="img-fluid d-block w-100" src="{{ env('BACKEND_HOST').'image/'.$gal->image_path }}" alt="{{ $fa->name }}">
              <div class="carousel-caption d-none d-md-block">
                
                <h2 class="sub-title sub-title-inside  mb-0">{{ $fa->name }}</h2>
                <a href="{{route('facilities.show',$fa->slug)}}" class="btn btn-ioc btn-banner smaller-btn-banner btn-accent mt-2">Lebih Lanjut</a>
              </div>
            </div>
            @endif
          @endforeach
          @if($sp)
          <div class="carousel-item {{ $facility->count() == 0 ? 'active' : '' }}">
            <img class="img-fluid d-block w-100" src="{{ env('BACKEND_HOST').'image/'.$sp->image_path }}" alt="{{ $sp->title }}">
            <div class="carousel-caption d-none d-md-block">
              <h2 class="sub-title sub-title-inside  mb-0">{{ $sp->title }}</h2>
              <a href="{{route('home.swimming_pool')}}" class="btn btn-ioc btn-banner smaller-btn-banner btn-accent mt-2">Lebih Lanjut</a>
            </div>
          </div>
          @endif
        </div>
        <a class="carousel-control-prev d-none" href="#carouselIocFacility" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next d-none" href="#carouselIocFacility" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-md-10 text-justify">
      <div class="row facility-list mb-4">
        <div class='col-facility col-md-3'>
          <i class="far fa-clock"></i>
          <h5 class="content-title">Ruang Meeting</h5>
          <p class="content">Berbagai tipe ruang meeting yang dekat dengan alam dan menghadirkan suasana nyaman untuk berbagai kebutuhan.</p>
        </div>
        <div class='col-facility col-md-3'>
          <i class="fab fa-free-code-camp"></i>
          <h5 class="content-title">Area Camping</h5>
          <p class="content">Tempat camping yang luas, dikelilingi tanaman yang asri dengan udara pegunungan yang segar.</p>
        </div>
        <div class='col-facility col-md-3'>
          <i class="fas fa-taxi"></i>
          <h5 class="content-title">Danau / Perahu Karet</h5>
          <p class="content">Pada area danau ini disewakan perahu karet untuk pengunjung dan di atasnya dilalui lintasan flying fox.</p>
        </div>
        <div class='col-facility col-md-3'>
          <i class="fas fa-anchor"></i>
          <h5 class="content-title">Kolam Renang</h5>
          <p class="content">Rasakan sensasi berenang dan bersantai dengan melihat pemandangan pegunungan yang menyejukkan.</p>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


<section class="villas latest-news bg-light-grey"> <!-- Ganti jadi latest news-->
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9 mb-5">
      <div class="title-plus-text">
        <h3 class="sub-title playfair-font font-italic grey-color">Dapatkan gambaran lebih melalui</h3>
        <h2 class="title font-weight-light dark-color">Artikel dan <span class="accent-color font-weight-bold ">Berita Terbaru</span></h2>
        <p class="caption-content roboto-font ">Berbagai informasi dan ulasan tentang IOC dan event-event di dalamnya.</p>
      </div>
    </div>  
  </div>                                    
    @php($col=3)
    @foreach($news as $key => $latest_news)
      @if($key%$col==0)                 
      <div class="row">
        @endif
        <div class="col-md-4">
          <a href="{{route('news.show',['id'=>$latest_news->slug]) }}" title="{{ $latest_news->title }}">
            <div class="villa-grid-item">
              <div class="room-grid-item-figure">
                <img class="img-fluid" src="{{ env('BACKEND_HOST').'image/'.$latest_news->image }}" alt="{{ $latest_news->name }}">
              </div>
              <div class="entryroom-grid-content">
                <h5 class="news-category accent-color">
                @foreach($latest_news->category as $key=>$cat)
                <a class="cats accent-color" href="{{route('news.index')}}?filter[categories]={{$cat->name}}" rel="category tag">{{ $cat->name }}</a> 
                @if($key < count($latest_news->category)-1) , @endif
                @endforeach
                </h5>
                
                <a href="{{route('news.show',['id'=>$latest_news->slug]) }}" title="{{ $latest_news->title }}">
                <h2 class="news-title title dark-color mt-0">{{$latest_news->title}}</h2>
                </a>
                <hr class="news-separator"/>
                <p class="caption-content font-weight-light mb-0">{{ $latest_news->cutDescLabel }}...</p>

                <p class="news-release-date">{{ $latest_news->created_at->format("F d, Y")}}</p>
              
              </div>
            </div>
          </a>
        </div>
      @if($key%$col==$col-1)
      </div>
      @endif
    @endforeach
    </div>
  </div>
</section>

<section class="promotion dark-section d-none"> <!-- Ganti jadi Promo-->
  <div class="container my-5">
   <div class="row justify-content-end">
     <div class="col-md-6 align-self-end">
      <div class="title-plus-text">
        <h3 class="sub-title playfair-font font-italic white-color">Hotel's hot offer</h3>
        <h2 class="title font-weight-bold white-color">Get <span class="accent-color font-weight-bold ">30% discount</span> in your 3rd night</h2>
        <a href="#" class="btn btn-ioc btn-banner btn-accent mt-4">Offer Detail</a>
      </div>
     </div> 
    </div>                                       
    
  </div>
</section>
@endsection

@push('scripts')
@include('layouts._script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script type="text/javascript">
    $('.select2').select2();
  </script>
@endpush

