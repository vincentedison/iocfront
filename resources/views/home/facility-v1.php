@extends('layouts.app')
@push('styles')
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
    
{{--  .img{
  width:100%;
}  --}}
  /* 100% Image Width on Smaller Screens */
    .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img{
        height:550px;
    }
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
  .centered {
    position: absolute;
    top: 70%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .fade-carousel {
    position: relative;
    height: 100%;
  }
  .fade-carousel .carousel-inner .item {
    height: 60vh;
  }
  .fade-carousel .carousel-indicators > li {
    margin: 0 2px;
    background-color: #f39c12;
    border-color: #f39c12;
    opacity: .7;
  }
  .fade-carousel .carousel-indicators > li.active {
    width: 10px;
    height: 10px;
    opacity: 1;
  }

  /********************************/
  /*          Hero Headers        */
  /********************************/
  .hero {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 3;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
    text-shadow: 1px 1px 0 rgba(0,0,0,.75);
    -webkit-transform: translate3d(-50%,-50%,0);
    -moz-transform: translate3d(-50%,-50%,0);
    -ms-transform: translate3d(-50%,-50%,0);
    -o-transform: translate3d(-50%,-50%,0);
    transform: translate3d(-50%,-50%,0);
  }
  .hero h1 {
    font-size: 6em;    
    font-weight: bold;
    margin: 0;
    padding: 0;
  }

  .fade-carousel .carousel-inner .item .hero {
    opacity: 0;
    -webkit-transition: 2s all ease-in-out .1s;
    -moz-transition: 2s all ease-in-out .1s; 
    -ms-transition: 2s all ease-in-out .1s; 
    -o-transition: 2s all ease-in-out .1s; 
    transition: 2s all ease-in-out .1s; 
  }
  .fade-carousel .carousel-inner .item.active .hero {
    opacity: 1;
    -webkit-transition: 2s all ease-in-out .1s;
    -moz-transition: 2s all ease-in-out .1s; 
    -ms-transition: 2s all ease-in-out .1s; 
    -o-transition: 2s all ease-in-out .1s; 
    transition: 2s all ease-in-out .1s;    
  }

  /********************************/
  /*            Overlay           */
  /********************************/
  .overlay {
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 2;
    background-color: #080d15;
    opacity: .7;
  }

  /********************************/
  /*          Custom Buttons      */
  /********************************/
  .btn.btn-lg {padding: 10px 40px;}
  .btn.btn-hero,
  .btn.btn-hero:hover,
  .btn.btn-hero:focus {
    color: #f5f5f5;
    background-color: #1abc9c;
    border-color: #1abc9c;
    outline: none;
    margin: 20px auto;
  }

  /********************************/
  /*       Slides backgrounds     */
  /********************************/
  .fade-carousel .slides .slide-1, 
  .fade-carousel .slides .slide-2,
  .fade-carousel .slides .slide-3 {
    height: 100%;
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
  }

  @foreach($facility->gallery as $key => $gallery)
  .fade-carousel .slides .slide-{{ $key+1}} {
    background-image: url('{{ env('BACKEND_HOST').'image/'.$gallery->image_path }}'); 
  }
  @endforeach


  /********************************/
  /*          Media Queries       */
  /********************************/
  @media screen and (min-width: 980px){
    .hero { width: 980px; }    
  }

  .hero h1 { font-size: 4em; }    
}
li {
    list-style-position: inside;
}
</style>
@endpush
@section('content')
<div class="container" style="text-align: center">
  <center>
        <div class='row'>
          <div class='col-md-12'>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    @php($i=0)      
      @foreach($facility->gallery as $gal)    
    <li data-target="#myCarousel" data-slide-to="{{$i}}" class="{{ $i==0 ? 'active' : '' }}"></li>
    @php($i++)
    
@endforeach

  </ol>

  <div class="carousel-inner">
    @php($i=0)
    @if(count($facility->gallery )==0)
      <img height="550px" class="img" src="{{ env('BACKEND_HOST').'image/default-picture.png' }}" alt="{{ $facility->name }}" alt="Los Angeles">    
    @endif
  @foreach($facility->gallery as $gal)      
    <div class="item {{ $i==0 ? 'active' : '' }}">
    @php($i++)        
      <img class="img" src="{{ env('BACKEND_HOST').'image/'.$gal->image_path }}" alt="{{ $facility->name }}" alt="Los Angeles">
      <div class="carousel-caption">
        <h3>{{$gal->title}}</h3>
        <h4>{{$gal->description}}</h4>
      </div>
    </div>
@endforeach
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

  </div>
  </div>
  <br>
    <div class="row">
          <div class='col-md-12'>      
      <h1>{{$facility->name}}</h1>
          </div>
    </div>
    <div class="row">
          <div class='col-md-12'>      
      <h4>{!!$facility->description !!}</h4>
          </div>
    </div>
      
    <div class="row">
      <div class="col-sm-12">
        <div class="col-sm-4">Minimum</div>
        <div class="col-sm-8">{{$facility->minimum}}</div>
      </div>
      <div class="col-sm-12">
        <div class="col-sm-4">Kapasitas</div>
        <div class="col-sm-8">{{$facility->capacity}}</div>
      </div>
    </div>
      <br>
      <br>
      
      <div class="row">
      <div class="col-sm-12">Fasilitas</div>
      
      <div class="col-sm-12">
        @foreach($facility->facilityDetail as $a)
        <div class="col-sm-3">
          {{ $a->facility_attribute->name }}
        </div>
        @endforeach
      </div>
      
    </div>

    <br>
    <br>
    @if($facility->type_id == 3)
      <div class="row">
      <div class="col-sm-12">
        
        SUSUNAN KURSI
      </div>
      </div>
      @endif
    <br>
    <br>
    <div class="row">
          <div class="col-md-12">
       <center> <h1>Check Availability</h1></center>
       <br>
        {!! Form::open(['route' => ['facilities.index'], 'method'=>'GET', 'enctype'=>'multipart/form-data','role' => 'form']) !!}
        <div class="col-md-3">
            {!! Form::date('startdate',date("Y-m-d"), ['placeholder' => 'Start Date','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::number('duration',null, ['placeholder' => 'Duration','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-md-2">
            {!! Form::number('person',null, ['placeholder' => 'Person','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-md-2">
            {!! Form::select('type',$type, $facility->type_id,['placeholder' => 'Type','class'=> 'form-control select2','required' => 'required', 'disabled'=>'true']) !!}
        </div>
        <div class="col-md-2">
            {!! Form::hidden('id',$facility->id,['class'=> 'form-control']) !!}
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary">Check Facility</button>
        </div>
        {!! Form::close() !!}
    </div>
    </div>

    </center>
  </div>
  <br>
  <br>
  <br>


  @stop
