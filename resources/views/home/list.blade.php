@extends('layouts.app')


@section('content')
<div class="container" style="text-align: center">
    <div class='row'>
        <h1>Carousel</h1>
    </div>
    <div class="row">
        <h1>Book</h1>
        {!! Form::open(['route' => ['home.getbooking'], 'method'=>'GET', 'enctype'=>'multipart/form-data','role' => 'form']) !!}
        <div class="col-md-3">
            {!! Form::date('startdate',null, ['placeholder' => 'Start Date','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::number('duration',null, ['placeholder' => 'Duration','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-md-2">
            {!! Form::number('person',null, ['placeholder' => 'Person','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-md-2">
            {!! Form::select('type',$type, null,['placeholder' => 'Type','class'=> 'form-control select2','required' => 'required']) !!}
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
        {!! Form::close() !!}
    </div>
    <div class='row'>
        <h1>Value</h1>
        <div class="col-md-4">
            <img src="{{asset('image/Service1.png')}}">
        </div>
        <div class="col-md-4">
            <img src="{{asset('image/Service2.png')}}">
        </div>
        <div class="col-md-4">
            <img src="{{asset('image/Service3.png')}}">
        </div>
    </div>
    
    <div class='row'>
        <h1>Penginapan</h1>
        @for($i=0; $i<6; $i++)
        <div class="col-md-4">
            <h4>{{$villa[$i]->name}}</h4>
            <a href="{{route('villas.show',['villa'=>$villa[$i]->slug])}}">
                <img alt="" title="" style="height:100%;width:100%;" src="{{ env('BACKEND_HOST').'image/'.$villa[$i]->gallery[0]->image_path }}">    
            </a>
        </div>
        @endfor
    </div>
    
    <div class='row'>
        <h1>Testimony</h1>
        <div class="col-md-6">
            Testimoni1
        </div>
        <div class="col-md-6">
            Testimoni2
        </div>
    </div>
    <div class="row">
        <div class='container-fluid'>
            <h1>Facility</h1>
            <div id="carousel-demo" class="carousel vertical slide" data-ride="carousel" data-interval="2000" style="height:50vmin!important">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    @for($i=0; $i<3; $i++)

                    @if($i==0)
                    <li data-target="#carousel-demo" data-slide-to={{$i+1}} class="active"></li>
                    @else
                    <li data-target="#carousel-demo" data-slide-to={{$i+1}}></li>
                    @endif
                    @endfor
                </ol>

                <!-- Sliding images statring here --> 
                <div class="carousel-inner" > 
                    @for($i=0; $i<3; $i++)
                    @if($i==0)
                    <div class="item active">
                        <img src="{{ env('BACKEND_HOST').'image/'.$facility[$i]->gallery[0]->image_path }}" alt="banana">
                        <div class="carousel-caption">
                            <p>{!!$facility[$i]->description!!}</p>
                        </div>
                    </div> 
                    @else
                    <div class="item"> 
                        <img src="{{ env('BACKEND_HOST').'image/'.$facility[$i]->gallery[0]->image_path }}" alt="banana">
                        <div class="carousel-caption">
                            <p>{!!$facility[$i]->description!!}</p>
                        </div>
                    </div> 
                    @endif
                    @endfor
                </div> 

                <!-- Next / Previous controls here -->
                <a class="left carousel-control" href="#carousel-demo" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-up"></span>
                </a>
                <a class="left carousel-control" style="margin-top:40vmin!important" href="#carousel-demo" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
            </div>
        </div>
    </div>
</div>
@stop