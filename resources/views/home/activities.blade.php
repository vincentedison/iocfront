@extends('layouts.app')
@push('styles')
<style>
</style>
@endpush

@section('content')

<section class="title-section bg-light-grey0">
	<div class="container">
		<div class="row justify-content-md-center text-center">
			<div class="col-md-9">
				<h2 class="title dark-color">Aktivitas</h2>
			</div>  
		</div>
	</div>
</section>
<section class="content-section activities">
	<div class="container">
		<div class="card-columns"> <!-- Ini yang membuat jadi masonry, bawa an nya bootstrap 4-->
			@foreach($activities as $activity)
			<div class="card">
				@if ($activity->image)
				<img class="card-img-top" src="{{ env('BACKEND_HOST').'image/'.$activity->image }}" alt="Card image cap">
				@endif
				@if ($activity->title || $activity->description )
				<div class="card-body">
					@if ($activity->title)<h5 class="card-title">{{ $activity->title }}</h5>@endif
					@if ($activity->description)<p class="card-text">{!! $activity->description !!}</p>@endif
				</div>
				@endif
			</div>
			@endforeach
		</div>
	</div>



</section>

@endsection