@extends('layouts.app')
@push('styles')
<style>
    .img{
        width:100%;
    }

</style>
@endpush

@section('content')

<div id="carouselIoc" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ env('BACKEND_HOST').'/image/banner_default.jpg' }}" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="sub-title playfair-font font-italic accent-color mb-0">New era of</h2>
        <h1 class="title font-weight-bold dark-color mt-0">Hospitality</h1>
        <p class="caption-content roboto-font font-weight-light darker-color">Providing guests unique and enchanting views from their rooms with its exceptional amenities, makes Star Hotel one of bests in its kind.</p>
        <a href="" class="btn btn-ioc btn-banner btn-accent">Understand More</a>
    </div>
</div>
        <!-- <div class="carousel-item">
          <img class="d-block w-100" src="{{ env('BACKEND_HOST').'/image/banner_swimming_pool.jpg' }}" alt="Second slide">
      </div> -->
  </div>
  <a class="carousel-control-prev" href="#carouselIoc" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselIoc" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
</div>

@foreach($facility as $facs)
<div class="section bg-light-grey">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-8 text-center">
                <img class="img" src="{{ env('BACKEND_HOST').$facs->path_label }}" alt="{{ $facs->title }}">
                
                <h3 class="sub-title playfair-font font-italic grey-color"><a href="{{route($types.'.show',['villa'=>$facs->slug])}}">Check Detail<a></h3>
                <h2 class="title font-weight-bold font-weight-light dark-color">{{$facs->title}}<span class=""></span></h2>
                <p class="caption-content roboto-font font-weight-light darker-color">{!!$facs->description!!}</p>
            </div>
        </div>
    </div>
</div>
@endforeach


@stop