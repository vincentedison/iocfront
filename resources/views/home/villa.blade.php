@extends('layouts.app')
@push('styles')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<style>

</style>
@endpush

@section('content')

<section class="title-section bg-light-grey0">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9">
       <h2 class="title dark-color">{{ $villa->name }}</h2>
     </div>  
   </div>
 </div>
</section>


<section class="content-section">
  <div class='container'>
    <div class="row">
      <div class="col-md-7">         
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            @php($i=0)      
            @foreach($villa->gallery as $gal)    
            <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{ $i==0 ? 'active' : '' }}"></li>
            @php($i++)
            @endforeach
          </ol>
          <div class="carousel-inner">
            @php($i=0)
            @foreach($villa->gallery as $gal)      
            <div class="carousel-item {{ $i==0 ? 'active' : '' }}">
              @php($i++)        
              <img class="d-block w-100" src="{{ env('BACKEND_HOST').'image/'.$gal->image_path }}" alt="{{ $villa->name }}">
            </div>
            @endforeach
            </div> 
            <a class="carousel-control-prev d-lg-none" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next d-lg-none "  href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        @if($villa->link_embed_src_label!="")
          <iframe class="mt-5" src="{{ $villa->link_embed_src_label }}" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>    
        @endif
        </div>
      

      <div class="col-md-5 facility-content">
        <div class="facility-detail mb-1">
          <h5 class="detail-title accent-color">Fasilitas yang Tersedia</h5>
          <ul class="ul-facility-detail">
          @php($i=0)
          @foreach($villa->facilityDetail as $fd)
              @php($i++)
              <li>
                <i class="{{$fd->facility_attribute->icon}} accent-color pr-2" data-toggle="tooltip" data-placement="top" title="{{$fd->facility_attribute->name}}"></i> <!-- Jangan lupa ganti dengan font awesome dari db -->
                <span class="facility-include">{{$fd->facility_attribute->name}} : {{$fd->value}}</span>
              </li>
          @endforeach
          </ul>
        </div>
        <div class="facility-price mb-1">
          <p><span class="strong">{!! H::rupiah($villa->weekend_price) !!}</span>/hari</p>
        </div>
        <div class="facility-description mb-1">
        <h4>{!!$villa->description!!}</h4>
        <br/>
            @if($villa->minimum > 0 && $villa->maximum > 0)
            <h4>Minimum order untuk akomodasi ini adalah {{$villa->minimum }} dan kapasitas maksimumnya adalah {{$villa->capacity}} orang.</h4>
            @elseif($villa->maximum > 0)
            <h4>Tida ada minimum order untuk akomodasi ini dan kapasitas maksimumnya adalah {{$villa->capacity}} orang.</h4>
            @endif
            <h5>Waktu Check-in pk 14.00 </br>Waktu Checkout pk 12.00 <br/>(<em>berlaku untuk semua akomodasi dan fasilitas</em>)</h5>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="check-booking bg-light-grey">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9">
      <div class="title-plus-text">
        <h3 class="sub-title playfair-font font-italic grey-color">Cara mudah untuk</h3>
        <h2 class="title font-weight-light dark-color">Melakukan <span class="accent-color font-weight-bold ">Booking </span></h2>
        <p class="caption-content roboto-font ">Cukup isi kolom di bawah ini, dan pilih ruangan yang Anda inginkan melalui sistem booking online kami.</p>
      </div>
    </div>
    <div class="col-md-10 mt-5">
      {!! Form::open(['route' => ['facilities.index'], 'method'=>'GET', 'enctype'=>'multipart/form-data','role' => 'form']) !!}

      <div class="row">
        <div class="col-6 col-md-2 px-less"  style="margin-bottom: 10px;">
          {!! Form::date('startdate',date("Y-m-d"), ['placeholder' => 'Mulai Tanggal','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-6 col-md-2 px-less"  style="margin-bottom: 10px;">
          {!! Form::number('duration',null, ['placeholder' => 'Durasi (hari)','class'=> 'form-control','required' => 'required']) !!}
        </div>
        <div class="col-6 col-md-2 px-less"  style="margin-bottom: 10px;">
          {!! Form::number('person',null, ['placeholder' => 'Jumlah Tamu/Peserta','class'=> 'form-control personformfilter','required' => 'required']) !!}
        </div>

        <div class="col-6 col-md-2 px-less"  style="margin-bottom: 10px;">
          {!! Form::number('unit',null, ['placeholder' => 'Jumlah Unit','class'=> 'form-control unitformfilter','required' => 'required']) !!}
        </div>

        <div class="col-6 col-md-3 px-less"  style="margin-bottom: 10px;">
          {!! Form::select('type',$type, $villa->type_id,['placeholder' => 'Tipe','class'=> 'form-control select2','required' => 'required', 'disabled'=>'true']) !!}

          {!! Form::hidden('id',$villa->id,['class'=> 'form-control']) !!}
        </div>
      </div>
      <div class="row justify-content-md-center btn-cek-ketersediaan">        
        <div class="col-md-4">
          <button type="submit" class="btn btn-accent btn-big searchbutton">Cek Ketersediaan</button>
        </div>
      </div>
      {!! Form::close() !!}

    </div>
  </div>
</div>
</section>


@endsection

@push('scripts')
@include('layouts._script')

  <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

@endpush