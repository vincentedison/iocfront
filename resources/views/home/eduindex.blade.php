@extends('layouts.app')
@push('styles')
<style>
.edu-title{
  font-size: 24px;
  padding-top: 10px;    
  font-weight: 600;
}
</style>
@endpush

@section('content')

<section class="facility">
  <div class="container">
     <div class="row justify-content-md-center text-center">
       <div class="col-md-9 mb-5">
        <div class="title-plus-text">
          <h3 class="sub-title playfair-font font-italic grey-color">Bersantai dan Belajar</h3>
          <h2 class="title font-weight-light dark-color">Mengenal Alam melalui <span class="accent-color font-weight-bold ">Tur Edukasi</span></h2>
          <p class="caption-content roboto-font ">Lengkapi rekreasi Anda dengan berjalan-jalan melewati perkebunan kopi dan salak kami.</p>
        </div>
      </div>  
    </div>   
    @foreach($facility as $facs)   
    <div class='row my-5'>
      <div class="col-md-6 text-center">
          <img class="img-fluid" src="{{ env('BACKEND_HOST').$facs->path_label }}" alt="{{ $facs->title }}">
      </div>
      <div class="col-md-6">
          <a href="{{route($types.'.show',['villa'=>$facs->slug])}}">
          <h4 class="edu-title dark-color text-uppercase">{{$facs->title}}<span class=""></span></h4>
          </a>
          <p class="caption-content roboto-font font-weight-light darker-color">{!!$facs->description!!}</p>
      </div>
    </div>

  @endforeach

  </div>
</section>





@stop