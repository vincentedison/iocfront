@extends('layouts.app')
@push('styles')
<style>

</style>
@endpush
@section('content')

<section class="title-section bg-light-grey0">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9">
       <h2 class="title dark-color">{{ $sp->title}}</h2>
     </div>  
   </div>
 </div>
</section>


<section class="content-section">
  <div class='container'>
    <div class="row">
      <div class="col-md-7">    
        <img class="items img-fluid w-100" src="{{ asset('image/'.$sp->image_path )}}" alt="{{ $sp->title }}"/>
      </div>
      <!-- right content start-->

      <div class="col-md-5 facility-content">

        <div class="row">
          <div class='col-md-12'>   
            <h4 class="text-justify">Terletak di dataran tinggi dengan pemandangan alam yang mempesona, kolam renang kami akan menambah pengalaman kunjungan Anda. Hilangkan penat pekerjaan Anda dengan berolahraga sambil menikmati udara pegunungan yang segar. Tidak hanya untuk tamu yang menginap saja, namun kami juga menyediakan tiket kolam renang bagi pengunjung lainnya. Kursi yang nyaman diletakkan di tepi kolam akan memberikan sentuhan yang menarik bagi kolam renang outdoor kami.</h4>
          </div>
        </div>
      </div>
      <!-- right content end-->

    </div>
  </div>
</section>

@endsection
