@extends('layouts.app')
@push('styles')
<style>

</style>
@endpush

@section('content')

<!-- <div id="carouselIoc" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ env('BACKEND_HOST').'/image/banner_casava_durian_cottage.jpg' }}" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h2 class="sub-title playfair-font font-italic accent-color mb-0">New era of</h2>
        <h1 class="title font-weight-bold light-color mt-0">{{ $types }}</h1>
        <p class="caption-content roboto-font font-weight-light light-color">Providing guests unique and enchanting views from their rooms with its exceptional amenities, makes Star Hotel one of bests in its kind.</p>
        <a href="" class="btn btn-ioc btn-banner btn-accent">Understand More</a>
      </div>
</div>
  </div>
  <a class="carousel-control-prev" href="#carouselIoc" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselIoc" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
</div> -->

<section class="title-section bg-light-grey0">
  <div class="container">
   <div class="row justify-content-md-center text-center">
     <div class="col-md-9">
       <h2 class="title dark-color">{{ $type == 'rooms'? 'Penginapan' : $type }}</h2>
     </div>  
   </div>
  </div>
</section>

<section class="villas bg-light-grey2">
  <div class="container">   
    <div class="row text-center">                                 
    @foreach($facility as $key => $facs)
      <div class="col-md-4">
        <a href="{{route($types.'.show',['villa'=>$facs->slug])}}">
          <div class="villa-grid-item">
            <div class="room-grid-item-figure">
              <img class="img-fluid" src="{{ env('BACKEND_HOST').$facs->villa_image }}" alt="{{ $facs->name }}">
            </div>
            <div class="entryroom-grid-content">
              <h2 class="title dark-color px-2 mt-0">{{ html_entity_decode($facs->name) }}</h2>
              <p class="caption-content font-weight-light mb-0">{!! H::rupiah($facs->weekend_price) !!}/hari</p>
            </div>
          </div>
        </a>
      </div>
    @endforeach
      
    @if(count($bundle)>0)
      @foreach($bundle as $key => $facs)
        <div class="col-md-4">
          <a href="{{route('bundles.show',['id'=>$facs->slug])}}">
            <div class="villa-grid-item">
              <div class="room-grid-item-figure">
                <img class="img-fluid" src="{{ env('BACKEND_HOST').$facs->villa_image }}" alt="{{ $facs->name }}">
              </div>
              <div class="entryroom-grid-content">
                <h2 class="title dark-color px-2 mt-0">{{ html_entity_decode($facs->name) }}</h2>
                <p class="caption-content font-weight-light mb-0">{!! H::rupiah($facs->weekend_price) !!}/hari</p>
              </div>
            </div>
          </a>
        </div>
      @endforeach

    @endif
    </div>
  </div>
</section>

@endsection
