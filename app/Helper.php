<?php

namespace App;
use Carbon\carbon;
use App\GlobalSetting;
use App\FacilityType;
class Helper
{
    public static function globs($key,$default=null,$val=null)
    {
        if($val!=null) {
            $glob=GlobalSetting::where("key", "LIKE", "%".$key."%")->first();
            if(!$glob){
                $glob= new GlobalSetting;
                $glob->key=$key;
            }
            $glob->value=$val;
            $glob->save();
            return $glob->value();
        }
        else{
            $glob=GlobalSetting::where("key", "LIKE", "%".$key."%")->first();
             if(!$glob){
                $glob= new GlobalSetting;
                $glob->key=$key;
                $glob->value='';
                if($default!=null){
                    $glob->value=$default;
                }
                $glob->save();
            }
            return $glob->value;
        }
    }

    public static function rupiah($nums)
    {
    return 'Rp. ' . number_format( $nums, 0 , '' , '.' );;
    }

    public static function nonvilla()
    {
        return FacilityType::where("type",2)->get();
    }

      public static function tanggal($date)
    {
        $day=['Min','Sen','Sel','Rab','Kam','Jum','Sab'];
        $month=['GG','Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'];
        $dates=explode("-",$date);
        
        return $day[$dates[0]].", ".$dates[1]." ".$month[$dates[2]]." ".$dates[3];
    }


}
?>