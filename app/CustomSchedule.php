<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CustomSchedule extends Model
{
    public function detailFacility()
    {
        return $this->belongsTo('App\DetailFacility', 'detail_facility_id');
    }
    public function getCarbonstartAttribute()
    {
        $startdate = Carbon::parse($this->startdate)->format('d/m/Y H:i:s');
        return $startdate;
    }
    public function getCarbonendAttribute()
    {
        $enddate = Carbon::parse($this->enddate)->format('d/m/Y H:i:s');
        return $enddate;
    }
    protected $appends =['carbonstart','carbonend'];
    protected $guarded =['rangedate','detail_facility_id'];

    public function scopeBooked($query,$date1, $date2){
        return $query->whereDate("startdate","<=",$date2)->whereDate("enddate",">=",$date1);
    }
}
