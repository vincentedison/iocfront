<?php

namespace App\Http\Controllers;

    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Facility;
    use App\CustomSchedule;
    use App\FacilityType;
    use App\FacilityDetail;
    use App\Gallery;
    use App\Bundle;
    use App\Addon;

    use App\BundleDetail;
    use Auth;

    class FacilityController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            if($request->unit==0){
                $request->unit=1;
            }
            $startdate=Carbon::now()->startOfDay()->format("Y-m-d");
            $enddate=Carbon::now()->startOfDay();
            if(isset($request->startdate)){

            $startdate=Carbon::createFromFormat('Y-m-d',$request->startdate)->startOfDay()->format("Y-m-d");
            $enddate=Carbon::createFromFormat('Y-m-d',$request->startdate)->startOfDay();
            }
            $list=array();
            $nlist=array();
            $duration = $request->duration;
            $person = ceil($request->person/$request->unit);

            $enddate->addDays($duration);
            if($request->id==null){
                $request->id=0;
            }
            else{
                $request->type = Facility::find($request->id)->type_id;
            }



            $facility=Facility::active()->get()->where('capacity','>=',$person)->where('id','=',$request->id)->where('type_id','=',$request->type);
            // return $facility;

            foreach($facility as $f){
                $availablerooms=count($f->available($request->startdate,$request->duration));
                if( $availablerooms >= $request->unit){
                $detail=$f->detailFacility;
                    foreach($detail as $d){
                        $jadwal=$d->customSchedule->where('startdate','>=',$startdate)->where('enddate','<=',$enddate);
                        if(count($jadwal)==0){
                            array_push($list,$f);
                            break;
                        }
                    }
                }

            }
            $facility=Facility::active()->get()->where('capacity','>=',$person)->where('id','!=',$request->id)->where('type_id','=',$request->type);
            foreach($facility as $f){
                $availablerooms=count($f->available($request->startdate,$request->duration));
                if( $availablerooms >= $request->unit){           
                $detail=$f->detailFacility;
                    foreach($detail as $d){
                        $jadwal=$d->customSchedule->where('startdate','>=',$startdate)->where('enddate','<=',$enddate);
                        if(count($jadwal)==0){
                            array_push($nlist,$f);
                            break;
                        }
                    }
                }
            }
            // return $nlist;
            

            if($request->type==4){
                if($request->id==null){
                    $request->id=null;
                    $facility=Bundle::front()->get();
                    
                }
                else{
                    $facility=Bundle::front()->where('id',$request->id)->get(); 
                }

                foreach($facility as $f){
                $bundetail=$f->bundle_detail;
                if(count($bundetail)==0){
                    if(!in_array($f,$list)){
                            array_push($list,$f);       
                        }
                }
                $minavailable=9999;
                foreach($bundetail as $b){
                    if($b->bundl_type == "App\Facility"){
                        $detail = Facility::find($b->bundl_id);
                        $avail = $detail->available($request->startdate,$request->duration);
                            if(count($avail)<=0){
                                $minavailable=0;
                                break;
                            }
                                if(!in_array($f,$list)){
                                    if($minavailable > count($avail)){
                                        $minavailable=count($avail);
                                    }     
                                    if($minavailable==0)                    
                                        break;                            
                                }
                            
                    }
                    if($b->bundl_type == "App\Addons"){
                        $detail = Addon::find($b->bundl_id);
                        $avail = $detail->available();
                            if($avail<=0){
                                $minavailable=0;
                                break;  
                            }
                            if(!in_array($f,$list)){
                                if($minavailable > $avail){
                                        $minavailable=$avail;
                                    }     
                                if($minavailable==0)                    
                                    break;                            
                            }
                            
                    }
                }
                if($minavailable>0){
                    if(!in_array($f,$list)){
                        $f->frontend=$minavailable;   
                        array_push($list,$f);  
                    }

                }
                
            }

            // return $list;
            if($request->id!=null){
                $facility=Bundle::where('id','!=',$request->id)->active()->get(); 
            } 
            foreach($facility as $f){
                $bundetail=$f->bundle_detail;
                if(count($bundetail)==0){
                    if(!in_array($f,$nlist)){
                            array_push($nlist,$f);       
                        }
                }
                $minavailable=9999;
                
                foreach($bundetail as $b){
                    if($b->bundl_type == "App\Facility"){
                        $detail = Facility::find($b->bundl_id);
                        $avail = $detail->available($startdate,$duration);
                          if(count($avail)<=0){
                                $minavailable=0;
                                break;
                            }
                                if(!in_array($f,$list)){
                                    if($minavailable > count($avail)){
                                        $minavailable=count($avail);
                                    }     
                                    if($minavailable==0)                    
                                        break;                            
                                }
                            
                    }
                    if($b->bundl_type == "App\Addons"){
                        $detail = Addon::find($b->bundl_id);
                        $avail = $detail->available();
                            if($avail<=0){
                                $minavailable=0;
                                break;  
                            }
                            if(!in_array($f,$nlist)){
                                if($minavailable > $avail){
                                        $minavailable=$avail;
                                    }     
                                if($minavailable==0)                    
                                    break;                            
                            }
                            
                    }
                }
                if($minavailable>0){
                        if(!in_array($f,$nlist)){
                            $f->frontend=$minavailable;   
                            array_push($nlist,$f);  
                        }

                
                }
            }

            }


            $prices= array();
            $nprices= array();
            $id=0;
            if(Auth::user()){
                $id=Auth::user()->customer_type_id;
            }
            foreach($list as $l){
                $prc = $l->calPrice($startdate,$duration,$person,0,1,$id);
                
                array_push($prices,$prc/$duration);
            }

            foreach($nlist as $l){
                $prc = $l->calPrice($startdate,$duration,$person,0,1,$id);

                array_push($nprices,$prc/$duration);
            }
            // return $÷list;
                $ident = "Room/Facility";      
            if($request->type){
                if($request->type<=2){
                    $ident ="Kamar";
                }
                else{
                    $ident="Fasilitas";
                }
            }
            $type="Facility";
            
            $unit=$request->unit;
            // return $prices;
            return view('bookings.index',compact('person','list','nlist','prices','ident','unit','nprices','type','request'));
        }

        public function bundle_index(Request $request)
        {
            // return $request;
            $startdate=Carbon::now()->startOfDay();
            $enddate=Carbon::now()->startOfDay();
            if(isset($request->startdate)){
                $startdate=Carbon::createFromFormat('Y-m-d',$request->startdate)->startOfDay();
                $enddate=Carbon::createFromFormat('Y-m-d',$request->startdate)->startOfDay();
            }
            $list=array();
            $nlist=array();
            $duration = $request->duration;

            if(!isset($request->duration))
                $duration=1;
            $person = $request->person;
            if(!isset($request->person))
                $person=1;
            if(!isset($request->unit))
                $request->unit=1;

            $person=ceil($request->person/$request->unit);
            $enddate->addDays($duration);
            if($request->id==null){
                $request->id=null;
                $facility=Bundle::front()->get();
                
            }
            else{
                $request->type = Bundle::front()->where('id',$request->id)->first()->type_id;
                $facility=Bundle::front()->where('id',$request->id)->get(); 
            }


            // return $facility;        
            foreach($facility as $f){
                $bundetail=$f->bundle_detail;
                if(count($bundetail)==0){
                    if(!in_array($f,$list)){
                            array_push($list,$f);       
                        }
                }
                $minavailable=9999;
                foreach($bundetail as $b){
                    if($b->bundl_type == "App\Facility"){
                        $detail = Facility::find($b->bundl_id);
                        $avail = $detail->available($request->startdate,$request->duration);
                            if(count($avail)<=0){
                                $minavailable=0;
                                break;
                            }
                                if(!in_array($f,$list)){
                                    if($minavailable > count($avail)){
                                        $minavailable=count($avail);
                                    }     
                                    if($minavailable==0)                    
                                        break;                            
                                }
                            
                    }
                    if($b->bundl_type == "App\Addons"){
                        $detail = Addon::find($b->bundl_id);
                        $avail = $detail->available();
                            if($avail<=0){
                                $minavailable=0;
                                break;  
                            }
                            if(!in_array($f,$list)){
                                if($minavailable > $avail){
                                        $minavailable=$avail;
                                    }     
                                if($minavailable==0)                    
                                    break;                            
                            }
                            
                    }
                }
                if($minavailable>0){
                    if(!in_array($f,$list)){
                        $f->frontend=$minavailable;   
                        array_push($list,$f);  
                    }

                }
                
            }

            // return $list;
            if($request->id!=null){
                $facility=Bundle::where('id','!=',$request->id)->active()->get(); 
            } 
            foreach($facility as $f){
                $bundetail=$f->bundle_detail;
                if(count($bundetail)==0){
                    if(!in_array($f,$nlist)){
                            array_push($nlist,$f);       
                        }
                }
                $minavailable=9999;
                
                foreach($bundetail as $b){
                    if($b->bundl_type == "App\Facility"){
                        $detail = Facility::find($b->bundl_id);
                        $avail = $detail->available($startdate->format("Y-m-d"),$duration);
                          if(count($avail)<=0){
                                $minavailable=0;
                                break;
                            }
                                if(!in_array($f,$list)){
                                    if($minavailable > count($avail)){
                                        $minavailable=count($avail);
                                    }     
                                    if($minavailable==0)                    
                                        break;                            
                                }
                            
                    }
                    if($b->bundl_type == "App\Addons"){
                        $detail = Addon::find($b->bundl_id);
                        $avail = $detail->available();
                            if($avail<=0){
                                $minavailable=0;
                                break;  
                            }
                            if(!in_array($f,$nlist)){
                                if($minavailable > $avail){
                                        $minavailable=$avail;
                                    }     
                                if($minavailable==0)                    
                                    break;                            
                            }
                            
                    }
                }
                if($minavailable>0){
                        if(!in_array($f,$nlist)){
                            $f->frontend=$minavailable;   
                            array_push($nlist,$f);  
                        }

                
                }
            }
            // return $nlist;
            $facility=Facility::active()->get()->where('capacity','>=',$person)->where('id','=',$request->id)->where('type_id','=',4);
            // return $facility;

            foreach($facility as $f){
                $availablerooms=count($f->available($request->startdate,$request->duration));
                if( $availablerooms >= $request->unit){
                $detail=$f->detailFacility;
                    foreach($detail as $d){
                        $jadwal=$d->customSchedule->where('startdate','>=',$startdate)->where('enddate','<=',$enddate);
                        if(count($jadwal)==0){
                            array_push($list,$f);
                            break;
                        }
                    }
                }

            }
            $facility=Facility::active()->get()->where('capacity','>=',$person)->where('id','!=',$request->id)->where('type_id','=',4);
            
            foreach($facility as $f){
                $availablerooms=count($f->available($request->startdate,$request->duration));
                if( $availablerooms >= $request->unit){           
                $detail=$f->detailFacility;
                    foreach($detail as $d){
                        $jadwal=$d->customSchedule->where('startdate','>=',$startdate->format("Y-m-d"))->where('enddate','<=',$enddate);
                        if(count($jadwal)==0){
                            array_push($nlist,$f);
                            break;
                        }
                    }
                }
            }
            // return $nlist;

            $prices= array();
            $nprices= array();

            foreach($list as $l){
                if(Auth::user())
                        $prc = $l->calPrice($startdate->format("Y-m-d"),$duration,$person,0,1,Auth::user()->customer_type_id,0);
                    else
                        $prc = $l->calPrice($startdate->format("Y-m-d"),$duration,$person);
                array_push($prices,$prc/$duration);
            }
            $id=0;
            if(Auth::user()){
                $id=Auth::user()->customer_type_id;
            }
            foreach($nlist as $l){
                $prc = $l->calPrice($startdate->format("Y-m-d"),$duration,$person,0,1,$id);

                array_push($nprices,$prc/$duration);
            }


            // return $÷list;
                $ident = "Paket";
            $type="Bundle";
            // return $type;
            $unit=$request->unit;
            // return $prices;
            return view('bookings.index',compact('person','list','nlist','prices','unit','ident','nprices','type','request'));
        }
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
    
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($facility)
        {
            $facility = Facility::where('slug' ,'=',$facility)->first();
            if($facility){
            $type = FacilityType::pluck('name','id')->all();
            
            return view('home.facility', compact('facility','type'));
            }
            else{
                $facility = Bundle::where('slug' ,'=',$id)->first();
                if(!$facility){
                    return redirect()->route('home');
                }
                $type = FacilityType::pluck('name','id')->all();
                
                $gallery = Gallery::query();
                $gallery = $gallery->where('galleriable_type', '=', 'Bundle');
                $gallery = $gallery->where('galleriable_id', '=', $facility->id);
                $gallery = $gallery->paginate(5, ['*'], 'gallery');


                return view('home.bundle', compact('facility','type','gallery'));
            }

        }

        public function bundle($id)
        {
            $facility = Bundle::where('slug' ,'=',$id)->first();
            $type = FacilityType::pluck('name','id')->all();
            
            $gallery = Gallery::query();
            $gallery = $gallery->where('galleriable_type', '=', 'Bundle');
            $gallery = $gallery->where('galleriable_id', '=', $facility->id);
            $gallery = $gallery->paginate(5, ['*'], 'gallery');


            return view('home.bundle', compact('facility','type','gallery'));

        }
        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }

        public function getFacility(Request $request){
            if($request->unit==0){
                $request->unit=1;
            }
            if($request->person<$request->unit){
                return response([
                    'Jumlah pengunjung Lebih sedikit dari jumlah unit!'
                ],400);
            }

            $startdate=Carbon::now()->startOfDay()->format("Y-m-d");
            $enddate=Carbon::now()->startOfDay();
            if(isset($request->startdate)){
            $startdate=Carbon::createFromFormat('Y-m-d',$request->startdate)->startOfDay()->format("Y-m-d");
            $enddate=Carbon::createFromFormat('Y-m-d',$request->startdate)->startOfDay();
            }
            $list=array();
            $nlist=array();
            $duration = $request->duration;
            
            $person = ceil($request->person/$request->unit);
            $enddate->addDays($duration);
        

            $facility=Facility::active()->get()->where('capacity','>=',$person)->where('id','!=',$request->id);
            foreach($facility as $f){
                $availablerooms=count($f->available($request->startdate,$request->duration));
                if( $availablerooms >= $request->unit){
                $detail=$f->detailFacility;
                    foreach($detail as $d){
                        $jadwal=$d->customSchedule->where('startdate','>=',$startdate)->where('enddate','<=',$enddate);
                        if(count($jadwal)==0){
                            array_push($nlist,$f);
                            break;
                        }
                    }
                }
            }

            $id=0;
            if(Auth::user()){
                $id=Auth::user()->customer_type_id;
            }
            $nprices= array();
            foreach($nlist as $l){
                $prc = $l->calPrice($startdate,$duration,$person,0,1,$id);
                $l->price=$prc/$duration;
                $available= count($l->available($request->startdate,$request->duration));
                $l->unit=$available;
            }

            $facility=Bundle::front()->get();
            // return $facility;        
            foreach($facility as $f){
                $bundetail=$f->bundle_detail;
                $minavailable=9999;
                foreach($bundetail as $b){
                    if($b->bundl_type == "App\Facility"){
                        $detail = Facility::find($b->bundl_id);
                        $avail = $detail->available($request->startdate,$request->duration);
                            if(count($avail)<=0){
                                $minavailable=0;
                                break;
                            }
                                if(!in_array($f,$list)){
                                    if($minavailable > count($avail)){
                                        $minavailable=count($avail);
                                    }     
                                    if($minavailable==0)                    
                                        break;                            
                                }
                            
                    }
                    if($b->bundl_type == "App\Addons"){
                        $detail = Addon::find($b->bundl_id);
                        if(!$detail){
                            $minavailable==0;
                            break;  
                        }
                        $avail = $detail->available();
                            if($avail<=0){
                                $minavailable=0;
                                break;  
                            }
                            if(!in_array($f,$list)){
                                if($minavailable > $avail){
                                        $minavailable=$avail;
                                    }     
                                if($minavailable==0)                    
                                    break;                            
                            }
                            
                    }
                }
                if($minavailable>0){
                    $prc = $f->calPrice($startdate,$duration,$person,0,1,$id);
                    $f->price=ceil($prc/$duration);
                    $f->frontend=$minavailable;   
                    array_push($nlist,$f);  

                }
                
            }
            // return $nlist;
            
            return response([
                    'Fasilitas' =>$nlist
                ],200);
            
        }
}
