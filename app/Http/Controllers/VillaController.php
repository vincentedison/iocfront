<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facility;
use App\FacilityType;
use App\Bundle;
use App\Gallery;
class VillaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type,Request $request)
    {
        if($type=="rooms"){
            $tmp=$type;
            $type=FacilityType::where('type','1')->pluck("id");            
             $types = "villas";
            $opr = "=";
            $facility = Facility::whereIn('type_id',$type)->where("front_end",1)->get();        
            $bundle = [];
            $type=$tmp;           
        }
        else{
            $tmp=$type;
            
            $type = FacilityType::where("slug","LIKE","%".$type)->first();
            if(!$type)
                return redirect('home');
            if($type->type==2){
                $types="facilities";
                $opr="=";
            }
            $bundle = [];
            if($type->id==4){
                $bundle = Bundle::where('frontend',1)->get();
            }
                     
            $facility = Facility::where('type_id',$opr,$type->id)->where("front_end",1)->get();            
            $type=$type->name;           
  
        }
        
     
        $list=array();
        // return $facility[0]->gallery;
        return view('home.index',compact('list','type','villas','facility'))
        ->with("types",$types)
        ->with("bundle",$bundle);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($villasl)
    {        $villa = Facility::where('slug','=',$villasl)->first();
        
            if($villa){
                $type = FacilityType::pluck('name','id')->all();

                if($villa->type_id==3){
                    $facility=$villa;
                    return view('home.facility', compact('facility','type'));
                }
                return view('home.villa',compact('villa','type'));
            }
            else{

                $facility = Bundle::where('slug' ,'=',$villasl)->first();
                if(!$facility){
                    return redirect()->route('home');
                }
                $type = FacilityType::pluck('name','id')->all();
                
                $gallery = Gallery::query();
                $gallery = $gallery->where('galleriable_type', '=', 'Bundle');
                $gallery = $gallery->where('galleriable_id', '=', $facility->id);
                $gallery = $gallery->paginate(5, ['*'], 'gallery');


                return view('home.bundle', compact('facility','type','gallery'));
            }

    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
