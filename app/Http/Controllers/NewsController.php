<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Category;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $newss = News::query();
        $filter = [];
        if(isset($request->filter)) {
            $filter = $request->filter;
            foreach ($filter as $key => $value) {
                if(!empty($value)) {
                    if($key=="categories") {
                        $selected = Category::where("name",$value)->first();
                        $newss = $newss->whereIn('id',$selected->news->pluck("id"));
                    }
                    else if($key=="date") {
                        $newss = $newss->whereDate('created_at',$value);
                    }
                    else
                        $newss  = $newsswhereRaw("CAST($key as TEXT) ilike '%" . strtolower($value) . "%'");
                }
            }
        }
        $newss=$newss->orderBy('id','DESC')->paginate(10);
        $cats = Category::all()->pluck("name","id");
        $latest = News::orderBy("id","DESC")->limit(5)->get();
        return view('news.index')
        ->with('news', $newss)
        ->with('latest', $latest)
        ->with('cats', $cats)
        ->with('filter',$filter);
    }
    public function create(){
        $news = new News;
        return view('news.create')->with('news', $news);
    }
    public function store(Request $request){
        $news = new News;
        $news->fill($request->all());
        $news->save();
        $request->session()->flash('toast', 'Berita berhasil ditambahkan!');
        return redirect()->route('news.index');
    }
    public function edit($id){
        $news = News::find($id);
        return view('news.edit')->with('news',$news);
    }
    public function update($id, Request $request){
        $news = News::find($id);
        $news->fill($request->all());
        $news->save();
        $request->session()->flash('toast', 'Berita  berhasil diubah!');
        return redirect()->route('news.index');
    }
    public function show($id){
        
        $news = News::where("slug",$id)->first();
        // return $news;
        $cats = Category::all()->pluck("name","id");
        $latest = News::orderBy("id","DESC")->limit(5)->get();
        return view('news.show')
        ->with('news', $news)
        ->with('latest', $latest)
        ->with('cats', $cats);
    }
    public function destroy(Request $request, News $news){
        $news->delete();
        $request->session()->flash('toast', 'Berita berhasil dihapus!');
        return redirect()->route('news.index');
    }   

    public function getNews(Request $req){
        $result = News::where('title','LIKE',"%".$req->q."%")->limit(10)->pluck('title','id');
        return $result;
    }
}
