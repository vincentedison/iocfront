<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Facility;
use App\Customer;
use App\Order;
use App\DetailOrder;
use Auth;
use H;
use App\Folio;
use App\Bundle;
use App\DetailFacility;
use App\Addon;
use App\Menu;
use App\Invoice;
use App\DetailInvoice;

use DB; 
use Illuminate\Validation\ValidationException; 
use Illuminate\Support\Facades\Validator;
class AddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sessfacil=$request->session()->get('fasilitas',[]);
        $sessfoods=$request->session()->get('foods',[]);
        $sessaddons=$request->session()->get('addons', []);
        $facilitytoform=[];
        foreach($sessfacil as $facil){
            $facil=(object) $facil;
            $base_id=explode("-",$facil->id);
            if($base_id[0]=="B"){
            $facility = Bundle::find($base_id[1]);

            }
            else{
                $facility = Facility::find($base_id[1]);
                
            }
            $facilitytoform[]=['facility'=>$facility,'person'=>$facil->person,'unit'=>$facil->units,'total'=>$facil->total];
        }
        
        $foodtoform=[];
        if(count($sessfoods) >0){
             $sessfoods=(Object) $sessfoods;
            foreach($sessfoods->id as $i=>$foods){
                $flabel = "Pilih Menu";
                 if(strpos($foods,'|') === false){
                    $fmenu = Menu::find($foods);
                    if($fmenu)
                        $flabel = $fmenu->addon->name." - ".$fmenu->name;
                }
                else{
                    $ids=explode('|',$foods);
                    $fmenu = Menu::whereIn('id',$ids)->get();
                    if($ids[0]==$ids[1]){
                        $fmenu = Menu::find($ids[0]);
                        $flabel = $fmenu->addon->name." - ".$fmenu->name."(2)";
                    }
                    else{
                        $flabel = $fmenu[0]->addon->name." - ".$fmenu[0]->name.", ".$fmenu[1]->name;
                        $fmenu = Menu::find($ids[0]);
                    }
                }

                $foodtoform[]=['id'=>$foods,'label'=>$flabel,'food'=>$fmenu,'amount'=>$sessfoods->amount[$i],'time'=>$sessfoods->time[$i],'type'=>$sessfoods->type[$i],'total'=>$sessfoods->total[$i]];
            }
        }
       

        //  return $request->session()->get('bookings', 'default');
        $addons = Addon::addon()->active()->get();
        $meal = Addon::food()->with("menu")->active()->get();
        $snack = Addon::snack()->with("menu")->active()->get();

        $foods=[$meal,$snack];
        $customer = new Customer;
        $value = $request->session()->get('data', 'default');
        // return $value;
        $facility = $request->session()->get('facility', 'default');
        $type = $request->session()->get('type', 'default');
        $price = $request->session()->get('price', 'default');
        // return $type;
        // return $facility;
        if($type=="Bundle"){
            $facility = Bundle::find($facility);
        // return $facility;
            
        }
        else{
            $facility = Facility::find($facility);
            
        }
        // return $foods;

        $enddate = Carbon::createFromFormat('Y-m-d',$value['startdate'])->startOfDay();
        $enddate->addDays($value['duration']);
        //jenis(snack/makananan),Nama yang nampil di depan,sama hari atau esoknya( klo sama hari isi kosong klo hari besok nya isi 1), jam default

        $defaultmenu= array([0,'Lunch',0,'12:00',0],[1,'Coffe Break',0,'15:30',0],[0,'Dinner',0,'18:00',0],[0,'Breakfast',1,'07:00',0],[1,'Snack',1,'10:00',0]);
        $menu=json_decode(H::globs('menu',json_encode($defaultmenu)));
        $defaultlastdayMenu=array([0,'Lunch',1,'12:00',1]);
        $lastdayMenu=json_decode(H::globs('last_menu',json_encode($defaultlastdayMenu)));
        $price= array_fill(0, count($menu) * $value['duration'], 0);
        $tax = H::globs('tax','10')/100;
        $id=0;
        if(Auth::user()){
            $id=Auth::user()->customer_type_id;
        }
        $total = $facility->calPrice($value['startdate'],$value['duration'],$value['person'],0,1,$id);
        $tax *= $total;
        return view('addon.form',compact('facilitytoform','lastdayMenu','foodtoform','sessfoods','sessaddons','value','menu','addons','foods','tax','customer','enddate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
