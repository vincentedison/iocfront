<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EducationTour;

class EducationTourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $type = FacilityType::pluck('name','id')->all();
        // $villas = Facility::where('type_id','=',1)->get();
        $facility = EducationTour::all();
        $list=array();
        // return $facility[0]->gallery;
        return view('home.eduindex',compact('list','type','villas','facility'))->with("types","education_tours");;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($education_tour)
    {
        $slug = $education_tour;
        $education_tour = EducationTour::where('slug','=',$slug)->first();
        if($education_tour == null){
            return redirect ('/home');
        }
        else{
            return view('home.educationtour',compact('education_tour'));

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
