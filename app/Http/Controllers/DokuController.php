<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Invoice;

class DokuController extends Controller
{
    public function payment(Invoice $invoice)
    {
        // return $invoice->total_label;
        $order = Order::find($invoice->order_id);
        return view('doku.payment')->with('invoice', $invoice)->with('order', $order);
    }

    public function notify(Request $request)
    {
        activity()->withProperties($request->all())->log('DOKU notify called');
        $invoice = Invoice::where('invoice_code',$request->TRANSIDMERCHANT)->first();
        if($invoice) {
            if($request->RESPONSECODE == "0000") {
                $invoice->status = 1;
                $invoice->save();
                $order = Order::find($invoice->order_id);
                if($order->status==0)
                    $order->status=1;        
                if($order->lunas==0){
                    $order->lunas=1;
                }
                if($order->payableLabel <=0){
                    $order->lunas=2;
                }
                $order->save();


            }
        }

        return "CONTINUE";
    }


    public function redirect(Request $request)
    {
        return redirect()->route('home');
    }
}
