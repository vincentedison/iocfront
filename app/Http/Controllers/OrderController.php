<?php

namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Carbon\Carbon;
    use App\Facility;
    use App\Customer;
    use App\Order;
    use App\DetailOrder;
    use Auth;
    use H;
    use App\Folio;
    use App\Bundle;
    use App\DetailFacility;
    use App\Addon;
    use App\Invoice;
    use App\DetailInvoice;
    use App\Menu;
    use DB; 
    use Illuminate\Validation\ValidationException; 
    use Illuminate\Support\Facades\Validator;
    // use Session;

    class OrderController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $value = $request->session()->pull('course_id', 'default');
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function booknow(Request $request){
            // return $request->facility;
            $request->session()->pull('fasilitas', 'default');
            $request->session()->pull('foods', 'default');

            $sessfacility=[];
            $total=$request->data['person']*$request->data['unit']*$request->price;
            $temp=[];
            $temp=["id"=>$request->facility,'person'=>$request->data['person'],'units'=>$request->data['unit'],"total"=>$total];
            $sessfacility[]=$temp;
            
            $request->session()->put('fasilitas',$sessfacility);

            $base_id= explode("-",$request->facility);
            $typ="Facility";
            if($base_id[0]=="B"){
            $typ="Bundle";
            }
            
            $request->session()->put('data',$request->data);
            $request->session()->put('facility',$base_id[1]);
            $request->session()->put('type',$typ);
            $request->session()->put('price',$request->price);
            return redirect('/addon/create');
        }

        public function cart(Request $request)
        {
            // return $request->all();
            // Session::put('booking', $request);
            $bookings=$request;
            $customer = new Customer;
            $value = $request->session()->get('data', 'default');
            // return $value;
            $facility = $request->session()->get('facility', 'default');
            $type = $request->session()->get('type', 'default');
            $price = $request->session()->get('price', 'default');
            // return $type;
            if($type=="Bundle"){
                $facility = Bundle::find($facility);
            // return $facility;
            }
            else{
                $facility = Facility::find($facility);
                
            }
            $floops=[];
            $maxperson=0;
            foreach($bookings->facilid as $i => $facs){
                if($i==0){
                    continue;
                }
                $temp=[];
                $base_id= explode("-",$facs);
                if($base_id[0]=="B"){
                    $facilit = Bundle::find($base_id[1]);
                }
                else{
                    $facilit = Facility::find($base_id[1]);
                }
                $temp=array_add($temp,'facility',$facilit);
                $temp=array_add($temp,'person',$bookings->facil_person[$i]/$bookings->facil_unit[$i]);
                if($maxperson<$bookings->facil_person[$i]/$bookings->facil_unit[$i]){
                    $maxperson=$bookings->facil_person[$i]/$bookings->facil_unit[$i];
                }
                $temp=array_add($temp,'unit',$bookings->facil_unit[$i]);
                $floops[]=$temp;
            }
            $value['person']=$maxperson;
            $request->session()->put('data', $value);
            
            

            $enddate = Carbon::createFromFormat('Y-m-d',$value['startdate'])->startOfDay();
            $enddate->addDays($value['duration']);
            $tax = H::globs('tax','10')/100;
            $id=0;
            if(Auth::user()){
                $id=Auth::user()->customer_type_id;
            }
            $total = $facility->calPrice($value['startdate'],$value['duration'],$value['person'],0,1,$id);
            $tax *= $total;
            $fasilitas=[];
            foreach ($request->facilid as $i => $fasil) {
                if($i==0){
                    continue;
                }
                $fasilitas[]=['id'=>$fasil,'person'=>$request->facil_person[$i],'units'=>$request->facil_unit[$i],"total"=>$request->facil_amount[$i]];
            }
            $request->session()->put('fasilitas',$fasilitas);
            $request->session()->put('facilities',["id"=>$request->facilid,'person'=>$request->facil_person,'units'=>$request->facil_unit,"total"=>$request->facil_amount]);
            $request->session()->put('foods',["date"=>$request->fooddate,'id'=>$request->foods,'amount'=>$request->foodamount,"total"=>$request->foodtot,"time"=>$request->foodtime,"type"=>$request->foodtype]);
            $request->session()->put('addons', ["date"=>$request->tdate,'id'=>$request->tid,'amount'=>$request->tamount,"total"=>$request->tamtot]);
            $request->session()->put('last_requests',$request->all());

            return redirect('/orders/create');
            // return view('bookings.create',compact('value','afloops','aloops','floops','facility','tax','customer','enddate','requests'));
        }

        public function create(Request $request)
        {
            
            $bookings=(Object) session()->get('last_requests',[]);
            $customer = new Customer;
            $value = $request->session()->get('data', 'default');
            // return $value;
            $facility = $request->session()->get('facility', 'default');
            $type = $request->session()->get('type', 'default');
            $price = $request->session()->get('price', 'default');
            // return $type;
            if($type=="Bundle"){
                $facility = Bundle::find($facility);
            // return $facility;
            }
            else{
                $facility = Facility::find($facility);
                
            }
            $floops=[];
            foreach($bookings->facilid as $i => $facs){
                if($i==0){
                    continue;
                }
                $temp=[];
                $base_id= explode("-",$facs);
                if($base_id[0]=="B"){
                    $facilit = Bundle::find($base_id[1]);
                }
                else{
                    $facilit = Facility::find($base_id[1]);
                }
                $temp=array_add($temp,'facility',$facilit);
                $temp=array_add($temp,'person',$bookings->facil_person[$i]/$bookings->facil_unit[$i]);
                $temp=array_add($temp,'unit',$bookings->facil_unit[$i]);
                $floops[]=$temp;
            }
            
            $afloops=[];
            foreach($bookings->foods as $i => $facs){
                if($facs==0){
                    continue;
                }
                $temp=[];
                if(strpos($facs,'|') === false){
                    $facilit = Menu::find($facs);                    
                }
                else{
                    $ids=explode('|',$facs);
                    $facilit = Menu::whereIn('id',$ids)->get();
                    if($ids[0]==$ids[1]){
                        $facilit = Menu::find($ids[0]);
                    }
                }
                if($bookings->foodamount[$i]>0){
                    $temp=array_add($temp,'originidfood',$facs);
                    $temp=array_add($temp,'food',$facilit);
                    $temp=array_add($temp,'amount',$bookings->foodamount[$i]);
                    $temp=array_add($temp,'date',$bookings->fooddate[$i]);
                    $afloops[]=$temp;
                }
            }
                
            $aloops=[];
            foreach($bookings->tid as $i => $facs){
                if($i==0){
                    continue;
                }
                $temp=[];
                $facilit = Addon::find($facs);
                $temp=array_add($temp,'addon',$facilit);
                $temp=array_add($temp,'amount',$bookings->tamount[$i]);
                $temp=array_add($temp,'date',$bookings->tdate[$i]);
                $aloops[]=$temp;
            }


            $enddate = Carbon::createFromFormat('Y-m-d',$value['startdate'])->startOfDay();
            $enddate->addDays($value['duration']);
            $tax = H::globs('tax','10')/100;
            $total = $facility->calPrice($value['startdate'],$value['duration'],$value['person']);
            $tax *= $total;
            return view('bookings.create',compact('value','afloops','aloops','floops','facility','tax','customer','enddate'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            // $bookings=Session::get('booking', 'default');
            
            DB::beginTransaction();
            $errors = [];
            $notavailfacil="";
            $facility = $request->session()->get('facility', 'default');
            $type = $request->session()->get('type', 'default');
            // return $price;
            if($type=="Bundle"){
                $facility = Bundle::find($facility);
            // return $facility;
            }
            else{
                $facility = Facility::find($facility);
            }
            // return $facility;
            
            $value = $request->session()->get('data', 'default');
            $price = $request->session()->get('price', 'default');
            
            $order = new Order;
            $order->id = Order::orderBy('id','DESC')->first()->id+1;
            $order->status=0;
            $order->customer_id=Auth::user()->id;
            $cst=Auth::user();
            $order->adult=$value['person'];
            $order->children = 0;
            $order->description = $request->description;
            $order->guest_name = $request->guest_name;
            $order->guest_phone = $request->guest_phone;
            $order->date= Carbon::now();
            $order->save();
            $date_start = $order->date->startOfMonth();
            $date_end = $order->date->endOfMonth();
            $order_counter = Order::where('customer_id', $order->customer_id)->whereBetween('date', [$date_start,$date_end])->count();
            $order->order_code = "ORD/".str_pad($order->customer_id,3,"0",STR_PAD_LEFT)."/".$order->date->format("d/m/y")."/".str_pad($order_counter,3,"0",STR_PAD_LEFT);
            $order->save();                
            $floops=[];
            $maxperson=0;
            //looping fasilitas
            $jsonfacil= $request->session()->get('facilities',[]);
            $jsonfacil=(Object) $jsonfacil;
            $jsons=[];
            $amountsjson=[];
            $tar=[];
            foreach($jsonfacil->id as $i => $facs){
                if($i==0){
                    continue;
                }
                $temp=[];
                $base_id= explode("-",$facs);
                
                if($base_id[0]=="B"){
                    $facility = Bundle::find($base_id[1]);
                    $type="Bundle";
                }
                else{
                    $facility = Facility::find($base_id[1]);
                    $type="Facility";

                }
                for($j=0;$j<$jsonfacil->units[$i];$j++){
                $detail_order = new DetailOrder;
                $detail_order->id = DetailOrder::orderBy('id','DESC')->first()->id+1;

                $detail_order->order_id = $order->id;
                $detail_order->item_id = $base_id[1];
                $person=$jsonfacil->person[$i]/$jsonfacil->units[$i];
            
                $detail_order->item_type = "App\\".$type;
                $detail_order->amount =$value['duration'];
                $detail_order->amount2=$person;      
                $detail_order->price = $price;
                $detail_order->disc = 0;
                $detail_order->status = 0;
                // $tax = H::globs('tax')/100; 

                $tmps = $facility->calDetprice($value['startdate'],$value['duration'],$person ,0,1,Auth::user()->customer_type_id);   
                $total = $facility->calPrice($value['startdate'],$value['duration'],$person,0,1,Auth::user()->customer_type_id);
                $tax=0;
                $detail_order->subtotal = $total+$tax;
                $tojson= $person;
                    if($facility->per_person==1 && $person < $facility->minimum){
                        $tojson= $facility->minimum;
                    }
                    if($facility->per_person==0){
                        $tojson=1;
                    }
                $jsons=$this->Json($jsons,$tmps,$cst->name,$facility->name,$tojson,0);


                $detail_order->save();
                $item = $facility;

                if($base_id[0]=="B"){
                    foreach ($item->bundle_detail as $bundl) {
                    $fol= new Folio;
                    $fol->id = Folio::orderBy('id','DESC')->first()->id+1;

                    if($bundl->bundl_type=="App\\Addons"){
                        $itm= Addon::find($bundl->bundl_id); 
                        // $fol->price= $itm->price; 
                        $fol->folio_type = "App\\Addon";  
                        $itm->borrowed = $itm->borrowed + 1;
                        if($itm->borrowed > $itm->stock && $itm->limited==1){
                            if(strpos($notavailfacil,$itm->name) !== false){}
                                else{
                                    $notavailfacil=$itm->name.", ";

                                }
                            
                        }
                        $itm->save();    
                        $fol->amount = 1;

                    }
                    else{
                        $fol->folio_type = "App\\DetailFacility"; 
                        $facs = Facility::find($bundl->bundl_id);
                        $thiid=1;
                        if(count($facs->available($value['startdate'],$value['duration'])) > 0){
                            $thiid= $facs->available($value['startdate'],$value['duration'])[0]->id;
                        }
                        else{
                            if(strpos($notavailfacil, $facs->name) !== false){}
                            else{
                                $notavailfacil.=$facs->name.", ";

                            }
                        }
                        $facid =$thiid;   
                        $itm= DetailFacility::find($facid);
                        // $fol->price= $itm->facility->price;   
                        $fol->amount =  $value['duration'];

                    }  
                    $fol->folio_id= $itm->id;            
                    $fol->check_in=$value['startdate'];                        
                    $fol->check_out= Carbon::createFromFormat("Y-m-d", $value['startdate'])->adddays($value['duration'])->format("Y-m-d");                        
                    $fol->detail_order_id = $detail_order->id;
                    $fol->save();
                    }
                }
                else{
                    $fol= new Folio;
                    $fol->id = Folio::orderBy('id','DESC')->first()->id+1;
                    
                    $fol->folio_type = "App\\DetailFacility";
                    $thiid=1;
                    if( count($item->available($value['startdate'],$value['duration'])) > 0){
                        $thiid= $item->available($value['startdate'],$value['duration'])[0]->id;
                    }
                    else{
                            if(strpos($notavailfacil, $item->name) !== false){}
                            else{
                                $notavailfacil.=$item->name.",  ";
                            }
                        }
                    $fol->folio_id =$thiid;                
            
                    // $fol->price= $item->facility->price; 
                    $subtot = $item->calPrice($value['startdate'],$value['duration'],$person,0,1,Auth::user()->customer_type_id);                   
                    $persdiv = $detail_order->amount2;
                    if($item->per_person==1 && $persdiv < $item->minimum){
                        $persdiv= $item->minimum;
                    }
                    
                    // $tmps = $item->calDetprice($value['startdate'],$value['duration'],$person ,0,1,Auth::user()->customer_type_id);   
                    // $tojson= $person;
                    //     if($facility->per_person==1 && $person < $facility->minimum){
                    //         $tojson= $item->minimum;
                    //     }
                    //     if($facility->per_person==0){
                    //         $tojson=1;
                    //     }
                    // $jsons=$this->Json($jsons,$tmps,$cst->name,$item->name,$persdiv,0);

                    // $detail_order->price = $subtot/$persdiv/$detail_order->amount;
                    // $detail_order->item_type = "App\\Facility";   
                    // $detail_order->subtotal = $subtot;
                    $fol->check_in=$value['startdate'];
                    $fol->check_out= Carbon::createFromFormat("Y-m-d", $value['startdate'])->adddays($value['duration'])->format("Y-m-d");
            
                    $fol->amount =  $value['duration'];
                    $fol->detail_order_id = $detail_order->id;    
                    // return  $value['duration'];        
                    $fol->save();
                }
                }
            }
            //looping Makananan        
            $jsonfood=$request->session()->get('foods',[]);
            $jsonfood=(Object) $jsonfood;

            // return $request->jsonfood;
            foreach($jsonfood->id as $i => $facs){
                if($facs==0){
                    continue;
                }
                $temp=[];
                $foodid=0;
                $foodsnack=[];
                if(strpos($facs,'|') === false){
                    $foodid=$facs;
                }
                else{
                    $ids=explode('|',$facs);
                    $foodid=$ids[0];
                }
                $facilit = Menu::find($foodid);
                $item = Addon::find($facilit->addon_id);
                $detail= new DetailOrder;
                $detail->disc_type=0;
                $check = DetailOrder::orderBy('id','DESC')->first();
                    if($check)
                        $detail->id = $check->id+1;
                $detail->item_id = $facilit->addon_id;
                // $temp=array_add($temp,'amount',$bookings->foodamount[$i]);
                // $temp=array_add($temp,'date',$bookings->fooddate[$i]);
                $detail->amount= $jsonfood->amount[$i];
                $item->borrowed = $item->borrowed + $jsonfood->amount[$i];
                if($item->borrowed > $item->stock && $item->limited==1){
                    if(strpos($notavailfacil, $facilit->addon->name) !== false){}                    
                    else{
                        $notavailfacil=$facilit->addon->name.", ";
                    }
                }
                $item->save();           
                $detail->price= $item->price;
                $detail->order_id=$order->id;         
                $detail->disc= 0;
                $detail->item_type = "App\\Addon";
                $diskon=0;
                $tmp=[];
                $tmp = array_add($tmp,json_encode(['Addons',$detail->price]),1);
                $jsons=$this->Json($jsons,$tmp,$cst->name,$item->name,$detail->amount,0);

                $detail->subtotal = ($detail->price * $detail->amount) - $diskon;                        
                $detail->save();
                $del= Folio::where("detail_order_id", $detail->id)->delete();
                $fol= new Folio;   
                $check = Folio::orderBy('id','DESC')->first();
                    if($check)
                        $fol->id = $check->id+1;
                $dates=Carbon::createFromFormat('d/m/Y H:i',$jsonfood->date[$i]." ".$jsonfood->time[$i])->format('Y-m-d H:i:s');
                $fol->check_in=$dates;
                $howmanyfoodsnack=count($foodsnack);
                $fol->description= $jsonfood->type[$i]." (".$facilit->name.")";
                if($howmanyfoodsnack>0){
                    if($howmanyfoodsnack>1)
                        $fol->description= $jsonfood->type[$i]." [".$foodsnack[0]->name.", ".$foodsnack[1]->name."]";
                    else
                        $fol->description= $jsonfood->type[$i]." [".$facilit->name."(2)]";
                }
                $fol->check_out=$dates;
                $fol->folio_id= $item->id;
                $fol->folio_type = "App\\Addon";
                $fol->amount =  $jsonfood->amount[$i];
                // $fol->price= $item->price;
                $fol->detail_order_id = $detail->id;            
                $fol->save();
            }
            $aloops=[];

            //looping addons      
            
            $jsonaddon=$request->session()->get('addons',[]);
            $jsonaddon=(Object) $jsonaddon;

            foreach($jsonaddon->id as $i => $facs){
                if($i==0){
                    continue;
                }
                $temp=[];
                $item = Addon::find($facs);
                $detail= new DetailOrder;
                $detail->disc_type=0;
                $check = DetailOrder::orderBy('id','DESC')->first();
                    if($check)
                        $detail->id = $check->id+1;
                $detail->item_id = $facs;
                // $temp=array_add($temp,'amount',$bookings->foodamount[$i]);
                // $temp=array_add($temp,'date',$bookings->fooddate[$i]);
                $detail->amount= $jsonaddon->amount[$i];
                $item->borrowed = $item->borrowed + $jsonaddon->amount[$i];
                if($item->borrowed > $item->stock && $item->limited==1){
                    if(strpos($notavailfacil, $item->name) !== false){}                    
                    else{
                        $notavailfacil=$item->name.", ";
                    }
                    
                }
                $item->save();           
                $detail->price= $item->price;
                $detail->order_id=$order->id;         
                $detail->disc= 0;
                $detail->item_type = "App\\Addon";
                $diskon=0;
                $tmp=[];
                $tmp = array_add($tmp,json_encode(['Addons',$detail->price]),1);
                $jsons=$this->Json($jsons,$tmp,$cst->name,$item->name,$detail->amount,0);
                $detail->subtotal = ($detail->price * $detail->amount) - $diskon;                        
                $detail->save();
                $del= Folio::where("detail_order_id", $detail->id)->delete();
                $fol= new Folio;   
                $check = Folio::orderBy('id','DESC')->first();
                    if($check)
                        $fol->id = $check->id+1;
                $fol->check_in=$jsonaddon->date[$i]." 12:00:00";
                $fol->check_out=$jsonaddon->date[$i]." 12:00:00";
                $fol->folio_id= $facs;
                $fol->folio_type = "App\\Addon";
                $fol->amount = $jsonaddon->amount[$i];
                // $fol->price= $item->price;
                $fol->detail_order_id = $detail->id;            
                $fol->save();
            }
            $order->other=json_encode($jsons);
            $order->save();
            $notavailfacil=rtrim($notavailfacil,", ");

            if($order->totalLabel > 0.0) {
                $invoice = new Invoice;
                $invoice->id = Invoice::orderBy('id','DESC')->first()->id+1;

                $invoice->order_id = $order->id;
                $invoice->date = Carbon::today()->format('Y-m-d');
                $date_start = $invoice->date->startOfMonth();
                $date_end = $invoice->date->endOfMonth();
                $invoice_counter = Invoice::where('order_id', $invoice->order_id)->whereBetween('date', [$date_start,$date_end])->count();        
                $invoice->invoice_code = "INV/".str_pad($invoice->order_id,3, "0", STR_PAD_LEFT)."/".$invoice->date->format("d/m/y")."/".str_pad($invoice_counter,3,"0",STR_PAD_LEFT);
                $invoice->save();

                $din = new DetailInvoice;
                $din->id = DetailInvoice::orderBy('id','DESC')->first()->id+1;

                $din->amount = $order->totalLabel;
                $din->description= 'Pembayaran Order ' . $order->order_code;
                $din->invoice_id = $invoice->id;
                $din->save();
                //  $errors = array_add($errors, 'request_field', 'Terapat field Yang kurang tepat');
                if($notavailfacil!=""){
                    $errors = array_add($errors, 'name', 'Fasilitas '.$notavailfacil.' tidak tersedia sejumlah yang anda inputkan, Mohon coba kembali dengan fasilitas yang lain');

                    DB::rollBack();
                    throw ValidationException::withMessages($errors);
                } 
                DB::commit();
                return redirect()->route('doku.payment', $invoice->id);
            }
            //  $errors = array_add($errors, 'request_field', 'Terapat field Yang kurang tepat');
                if($notavailfacil!=""){
                    $errors = array_add($errors, 'name', 'Fasilitas '.$notavailfacil.' tidak tersedia sejumlah yang anda inputkan, Mohon coba kembali dengan fasilitas yang lain');

                    DB::rollBack();
                    throw ValidationException::withMessages($errors);
                } 
                DB::commit();
                        
            return view('bookings.thankyou')
            ->with('facility',$facility)
            ->with('value',$value)
            ->with('order',$order)
            ->with('tax',$tax);

        }

    public function Json($jsons,$arraytoloop,$customer_name,$prod_name,$amount,$diskon=0)
    {
        foreach($arraytoloop as $key => $li){
            $arrayresult=[];
            $arrayresult=array_add($arrayresult,'pengguna',$customer_name);
            $itemm = json_decode($key);
            $category = $itemm[0];
            $days = $li;
            $price = $itemm[1];
            $prdnm=$prod_name."(".$category.")";
            $arrayresult=array_add($arrayresult,'item',$prdnm);
            $arrayresult=array_add($arrayresult,'jmlitem',$amount);
            $arrayresult=array_add($arrayresult,'jmlhari',$li);
            $arrayresult=array_add($arrayresult,'biaya',$price);
            $arrayresult=array_add($arrayresult,'diskon',$diskon);
            $jsons[]=$arrayresult;
        }
        return $jsons;
    }
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
