<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\CustomSchedule;
use App\Facility;
use App\FacilityType;
use App\FacilityDetail;
use App\Gallery;
use App\Testimony;
use App\News;
use App\Category;
use Auth;
use App\Customer;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Hash;
use App\User;
use App\Activity;
use App\Banner;
use App\BannerLocation;

class HomeController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $req)
    {
        // dd(session()->all());
        $bloc=BannerLocation::where('code','LIKE','atas')->first();
        $batas= Banner::where('banner_location_id',$bloc->id)->get();
        $type = FacilityType::pluck('name','id')->all();
        $allvilla = FacilityType::where('type','1')->pluck("id");            
        $villas = Facility::whereIn('type_id',$allvilla)->active()->limit(6)->get();
        $facility = Facility::where('type_id','>=',3)->active()->get();
        $sp = Gallery::where('kode','SP')->first();
        $testi = Testimony::orderBy("id","DESC")->limit(2)->get();
        $newss = News::query();
        $newss=$newss->orderBy('id','DESC')->limit(3)->get();
        $cats = Category::all()->pluck("name","id");
        $list=array();
        // return $facility[0]->gallery;
        return view('home.home',compact('list','testi','type','villas','facility'))
        ->with("types","facilities")
        ->with('news', $newss)
        ->with('cats', $cats)
        ->with('batas', $batas)
        ->with('sp', $sp);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
    public function load_cus(Request $req)
    {
        $customer = Auth::user();
        return view('profile._form')->with('customer',$customer);
    }
    
    public function upd_cus(Request $req)
    {
        $cus = Customer::find(Auth::user()->id);
        $cus->fill($req->all());
        $cus->save();
        return redirect()->route('profile');    
        // return $cus;
    }
    public function upd_pass(Request $req)
    {
        // return $req;
        
        $errors =[];
        $cus = Customer::find(Auth::user()->id);
        //    return bcrypt($req->old_pass)." AnD ".$cus->password;  
           if (!Hash::check($req->old_pass, Auth::user()->password)) {
            $errors=array_add($errors,"old_pass","Password Lama Tidak sesuai");            
                
            }   

        if(strlen($req->new_pass)<6) {
            $errors=array_add($errors,"new_pass","Panjang minimum password adalah 6");                                    
        }
        if($req->new_pass!=$req->conf_pass) {
            $errors=array_add($errors,"conf_pass","Password baru tidak cocok dengan password konfirmasi");                        
        }

            if(count($errors)>0) {
                throw ValidationException::withMessages($errors);
            }

            $user = User::find(Auth::user()->id)
                ->update(
                    ['password'=> Hash::make($req->new_pass)]
                );
            return redirect()->route('profile');
    }

    public function activities()
    {
        $activities = Activity::all()->sortby('sort');
        // dd($activities);
        return view('home.activities',compact('activities'));
    }


    public function swimming_pool()
    {
        $sp = Gallery::where('kode','SP')->first();
        return view('home.swimming_pool')->with('sp', $sp);
    }

}
