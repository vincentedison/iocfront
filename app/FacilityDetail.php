<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityDetail extends Model
{
    public function facility()
    {
        return $this->belongsTo('App\Facility', 'facility_id');
    }
    public function facility_attribute()
    {
        return $this->belongsTo('App\FacilityAttribute', 'attribute_id');
    }
    protected $guarded =[];


}
