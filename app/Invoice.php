<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailInvoice;

class Invoice extends Model
{
	protected $guarded = [];
    protected $dates = ["date"];
    public function order()
    {
        return $this->BelongsTo('App\Order', 'order_id');
    }

    public function getTotalLabelAttribute()
    { 	$detin = DetailInvoice::where("invoice_id",$this->id)->selectRaw("SUM(amount) as tot")->groupBy("invoice_id")->first();
    	if($detin!=null){
         return $detin->tot;    		
    	}
    }

    public function getdateLabelAttribute()
    {
    	//try
      if($this->date==null){}
      else{
        return $this->date->format('d-m-Y');
       }
    }
}
