<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    public function addon()
    {
        return $this->hasMany('App\Addon', 'unit_id');
    }
    protected $guarded =[];
}
