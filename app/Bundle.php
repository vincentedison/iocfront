<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\carbon;
use App\Gallery;

class Bundle extends Model
{
    protected $guarded =[];
    protected $appends =['villa_image','id_morph','unit'];
    protected $hidden = [
        'id','slug','link_embed', 'type_id','front_end','back_end','unit_name','code','created_at','updated_at','weekend_price','no_stay_weekend_price','no_stay_weekday_price','per_person'
    ];

    public function scopeActive($query)
   {
       return $query->where('active',1)->where('frontend',1);
   }
    public function getUnitAttribute()
    {
        return $this->frontend;
    }
    public function scopeFront($query)
    {
        return $query->where('frontend',1);
    }
    public function getidMorphAttribute()
    {
        return "B-".$this->id;
    }
    public function getVillaImageAttribute()
    {
        $gallery = Gallery::where('galleriable_type', '=', 'Bundle')->where('galleriable_id', '=', $this->id)->first();
        if (!$gallery) {
            return '/image/default-picture.png';
        }
        return '/image/'.$gallery->image_path;
    }
     public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleriable');
    }
    public function bundle_detail()
    {
        return $this->hasMany('App\BundleDetail');
    }
    public function coupon_facility()
    {
        return $this->morphMany('App\CouponFacility', 'facs');
    }
    public function detail_order()
    {
        return $this->morphMany('App\DetailOrder', 'item');
    }
    public function special_price()
    {
        return $this->morphMany('App\SpecialPrice', 'special');
    }
    public function custom_price()
    {
        return $this->morphMany('App\CustomPrice', 'price');
    }

    public function calPrice($date,$dur,$person=1,$disc=0,$stay=1,$custype=0,$disctype=0){
        // return $custype;
        $start= Carbon::createFromFormat("Y-m-d",$date);
        // $end = $start->addDays($dur);
        $total=0;
        // $vl=[];
        for($i=0;$i<$dur;$i++){
            $curdate = $start->format("Y-m-d");
            $total += $this->todayPrice($curdate,$stay,$custype);
            $start->addDays(1);
            // $vl=array_add($vl,$curdate,$this->todayPrice($curdate));
        }
        // return $vl;
        if($this->per_person==1) {
            if($this->minimum>$person){
                $person = $this->minimum;
            }
            $total *= $person;
        }
        if($disctype==0){
            $total -= $total * $disc/100;
        }
        else{
            $total -= $disc;
        }
        return $total;
    }


    public function todayPrice($date,$stay=0,$custtype=0){
        $dt = Carbon::createFromFormat("Y-m-d",$date);
        $prices=0;
        $dayiso = $dt->format('N');
        $cusp = CustomPrice::whereDate("startdate","<=",$date)->whereDate("enddate",">=",$date)->where("price_id",$this->id)->where("price_type","LIKE","%Bundle")->first();
        if($cusp)
            return $cusp->price;
        $cusp = SpecialPrice::where("special_id",$this->id)->where("special_type","LIKE","%Bundle")->get();
        if(count($cusp) > 0){
            foreach($cusp as $cus){
                if($cus->customer_type_id==$custtype){
                    if($stay==0){
                        if($dayiso >= 5)        
                        {return $cus->no_stay_weekend_price;}
                        else        
                        {return $cus->no_stay_weekday_price;}
                    }
                    else if($stay==1){
                        if($dayiso >= 5)        
                            {return $cus->weekend_price;}
                        else        
                            {return $cus->price;}
                    }
                }
            }
        }
        if($stay==0){
            if($dayiso >= 5)        
                {return $this->no_stay_weekend_price;}
            else        
                {return $this->no_stay_weekday_price;}
        }
        if($dayiso >= 5 && $this->weekend_price!=null)        
                {return $this->weekend_price;}
        {return $this->price;}
    }

    //return array of price and amount
     public function calDetPrice($date,$dur,$person=1,$disc=0,$stay=0,$custype=0){
        // return $custype;
        $arra=[];
        $start= Carbon::createFromFormat("Y-m-d",$date);
        $total=0;
        for($i=0;$i<$dur;$i++){
            $curdate = $start->format("Y-m-d");
            $json = json_encode($this->todayDetPrice($curdate,$stay,$custype));
            if(empty($arra[$json])){
                $arra=array_add($arra,$json,0);
            }
            $arra[$json]+=1;
            $start->addDays(1);
        }
        return $arra;
    }

    public function todayDetPrice($date,$stay=0,$custtype=0){
        $dt = Carbon::createFromFormat("Y-m-d",$date);
        $prices=0;
        $dayiso = $dt->format('N');
        $cusp = CustomPrice::whereDate("startdate","<=",$date)->whereDate("enddate",">=",$date)->where("price_id",$this->id)->where("price_type","LIKE","%Bundle")->first();
        if($cusp)
            return $cusp->price;
        $cusp = SpecialPrice::where("special_id",$this->id)->where("special_type","LIKE","%Bundle")->get();
        if(count($cusp) > 0){
            foreach($cusp as $cus){
                if($cus->customer_type_id==$custtype){
                    if($stay==0){
                        if($dayiso >= 5)        
                        {return ["Special Price No Stay Weekend",$cus->no_stay_weekend_price];}
                        else        
                        {return ["Special Price No Stay Weekday",$cus->no_stay_weekday_price];}
                    }
                    else if($stay==1){
                        if($dayiso >= 5 )        
                            {return ["Special Price Weekend",$cus->weekend_price];}
                        else        
                            {return ["Special Price Weekday",$cus->price];}
                    }
                }
            }
        }
        if($stay==0){
            if($dayiso >= 5 )        
                {return ["No Stay Weekend",$this->no_stay_weekend_price];}
            else        
                {return ["No Stay Weekday",$this->no_stay_weekday_price];}
        }
        if($dayiso >= 5 && $this->weekend_price!=null)        
                {return ["Weekend",$this->weekend_price]; }
        return ["Weekday",$this->price];
    }


}
