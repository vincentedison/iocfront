<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $hidden = [
        'galleriable_id', 'id',"galleriable_type","slug","title","description","created_at","updated_at","kode"];
    public function galleriable()
    {
        return $this->morphTo();
    }

    protected $guarded =[];
    protected $galleriable_types = [
    'facility' => \App\Facility::class,
    'educationtour' => \App\EducationTour::class,
    ];
}
