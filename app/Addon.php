<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\carbon;
class Addon extends Model
{
    protected $guarded =[];
    protected $hidden = [
            'active','borrowed','limited','slug'];
    
    public function scopeActive($query)
   {
       return $query->where('active',1)->where('front_end',1);
   }
    public function available()
    {
        if($this->limited==1){
            return $this->stock-$this->borrowed;
        }
        return 99999;
    }
    public function detail_order()
    {
        return $this->morphMany('App\DetailOrder', 'item');
    }
    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }
    public function bundle_detail()
    {
        return $this->morphMany('App\BundleDetail', 'bundl');
    }
    public function folio()
    {
        return $this->morphMany('App\Folio', 'folio');
    }
    public function coupon_facility()
    {
        return $this->morphMany('App\CouponFacility', 'facs');
    }
    public function special_price()
    {
        return $this->morphMany('App\SpecialPrice', 'special');
    }
    public function custom_price()
    {
        return $this->morphMany('App\CustomPrice', 'price');
    }

    public function getActiveNameAttribute()
    {
        return $this->active==1 ? "Aktif" : "Tidak Aktif";
    }

    public function menu()
    {
        return $this->hasMany('App\Menu');
    }

public function getAddonTypeAttribute()
    {
    $text=["Tambahan","Makanan","Snack"];
    return $text[$this->type];
    }

    public function scopeFront($query)
    {
        return $query->where('front_end', '=' ,1);
    }
    public function scopeAddon($query)
    {
        return $query->where('type', '=' ,0);
    }

    public function scopeFood($query)
    {
        return $query->where('type', '=' ,1);
    }

    public function scopeSnack($query)
    {
        return $query->where('type', '=' ,2);
    }

    protected $appends = ['active_name'];

    public function calPrice($ammount=1,$disc=0,$stay=1,$custype=0){
        // return $custype;
        // $start= Carbon::createFromFormat("Y-m-d",$date);
        // $end = $start->addDays($dur);
        $total=0;
        // $vl=[];
        $total += $this->todayPrice($stay,$custype);
        // return $vl;
        
        $total *=$ammount;
        $total -= $total * $disc/100;
        return $total;
    }

    public function todayPrice($stay = 0, $custtype = 0){
        $cusp = SpecialPrice::where("special_id",$this->id)->where("special_type","LIKE","%Addon")->get();
        if(count($cusp) > 0){
            foreach($cusp as $cus){
                if($cus->customer_type_id==$custtype){
                    if($stay==0){
                        // if($dt->isWeekend())        
                        // {return $cus->no_stay_weekend_price;}
                        // else        
                        // {return $cus->no_stay_weekday_price;}
                        return $cus->no_stay_weekday_price;
                    }
                    else if($stay==1){
                        // if($dt->isWeekend())        
                        //     {return $cus->weekend_price;}
                        // else        
                        //     {return $cus->price;}
                            return $cus->price;
                    }
                }
            }
        }
        if($stay==0){
                return $this->no_stay_weekday_price;
        }
        return $this->price;
    }
}
