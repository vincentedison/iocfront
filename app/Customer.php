<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded =[];

     public function scopeActive($query)
    {
        return $query->where('blacklist',0);
    }
    
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }
    //

    public function Order()
    {
        return $this->hasMany('App\Order');
    }

    public function customer_type()
    {
        return $this->BelongsTo('App\CustomerType');
    }
    public function getNameLabelAttribute()
    {
        if($this->company_name=="")
            return $this->name;
        return $this->company_name;
    }
}
