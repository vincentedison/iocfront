<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use  App\CouponCustomer;
use App\CouponFacility;
use App\CouponFacilityType;
use App\CouponCustomerType;


class Coupon extends Model
{
    protected $guarded =[];
    protected $dates=['start','expiry'];

    public function coupon_facility()
    {
        return $this->hasMany('App\CouponFacility');
    }
    public function coupon_facility_type()
    {
        return $this->hasMany('App\CouponFacilityType');
    }   
    public function coupon_customer_type()
    {
        return $this->hasMany('App\CouponCustomerType');
    }
    public function coupon_customer()
    {
        return $this->hasMany('App\CouponCustomer');
    }
    public function getstartLabelAttribute()
    {
        if($this->start==null) {
        }
        else{
            return $this->start->format('d-m-Y');
        }
    }

    public function getexpiryLabelAttribute()
    {
        if($this->expiry==null) {
        }
        else{
            return $this->expiry->format('d-m-Y');
        }
    }

    public function getincludeListAttribute()
    {
        $res=[];
        $arr=[];
        $tmp="";
        $text=['Pelanggan','Kelompok Pelanggan','Fasilitas','Kategori Fasilitas'];
        $arr=array_add($arr, 0, CouponCustomer::join("customers", 'customers.id', '=', 'coupon_customers.customer_id')->where("exclude", 0)->where("coupon_id", $this->id)->get()->pluck("name"));
        
        $arr=array_add($arr, 1, CouponCustomerType::join("customer_types", 'customer_types.id', '=', 'coupon_customer_types.customer_type_id')->where("exclude", 0)->where("coupon_id", $this->id)->get()->pluck("name"));
        $item = CouponFacility::where("coupon_id",$this->id)->where("exclude", 0)->get();
        $tmparr=[];        
        foreach ($item as $fcs) {
            $nm="";

            if($fcs->facs_type == "App\\Facility"){
                $nm=CouponFacility::join("facilities", 'facilities.id', '=', 'coupon_facilities.facs_id');        
            }
            else if($fcs->facs_type == "App\\Addon"){
                $nm=CouponFacility::join("addons", 'addons.id', '=', 'coupon_facilities.facs_id');
            }
            else{
                $nm=CouponFacility::join("bundles", 'bundles.id', '=', 'coupon_facilities.facs_id');                
            }

            $nm = $nm->where("coupon_facilities.id", $fcs->id)->first();        
            array_push($tmparr,$nm->name);
        }
        $arr=array_add($arr, 2,$tmparr);        
        $arr=array_add($arr, 3, CouponFacilityType::join("facility_types", 'facility_types.id', '=', 'coupon_facility_types.facility_type_id')->where("exclude", 0)->where("coupon_id", $this->id)->get()->pluck("name"));
        for ($i=0; $i < 4; $i++) { 
            $j=0;
            $len = count($arr[$i]);
            foreach ($arr[$i] as $ar) {
                if ($j != $len - 1) {
                    $tmp.=$ar.", ";                                        
                }
                else{
                    $tmp.=$ar;
                }
                $j++;
            }          
            $res=array_add($res, $text[$i], $tmp);
            $tmp="";
        }
        
        return $res;
        
    }
    public function getexcludeListAttribute()
    {
        $res=[];
        $arr=[];
        $tmp="";
        $text=['Pelanggan','Kelompok Pelanggan','Fasilitas','Kategori Fasilitas'];
        $arr=array_add($arr, 0, CouponCustomer::join("customers", 'customers.id', '=', 'coupon_customers.customer_id')->where("exclude", 1)->where("coupon_id", $this->id)->get()->pluck("name"));
        
        $arr=array_add($arr, 1, CouponCustomerType::join("customer_types", 'customer_types.id', '=', 'coupon_customer_types.customer_type_id')->where("exclude", 1)->where("coupon_id", $this->id)->get()->pluck("name"));
        $item = CouponFacility::where("coupon_id",$this->id)->where("exclude", 1)->get();
        $tmparr =[];        
        foreach ($item as $fcs) {
            $nm="";

            if($fcs->facs_type == "App\\Facility"){
                $nm=CouponFacility::join("facilities", 'facilities.id', '=', 'coupon_facilities.facs_id');        
            }
            else if($fcs->facs_type == "App\\Addon"){
                $nm=CouponFacility::join("addons", 'addons.id', '=', 'coupon_facilities.facs_id');
            }
            else{
                $nm=CouponFacility::join("bunldes", 'bunldes.id', '=', 'coupon_facilities.facs_id');                
            }

            $nm = $nm->where("coupon_facilities.id", $fcs->id)->first();    
            array_push($tmparr,$nm->name);
        }
        $arr=array_add($arr, 2,$tmparr);   
        $arr=array_add($arr, 3, CouponFacilityType::join("facility_types", 'facility_types.id', '=', 'coupon_facility_types.facility_type_id')->where("exclude", 1)->where("coupon_id", $this->id)->get()->pluck("name"));
        for ($i=0; $i < 4; $i++) { 
            $j=0;
            $len = count($arr[$i]);
            foreach ($arr[$i] as $ar) {
                if ($j != $len - 1) {
                    $tmp.=$ar.", ";                                        
                }
                else{
                    $tmp.=$ar;
                }
                $j++;
            }              
            $res=array_add($res, $text[$i], $tmp);
            $tmp="";
        }
        
        return $res;
        
    }

    public function getdiskonLabelAttribute()
    {
        if($this->type==1) {
            return $this->amount."%";
        }
        else{
            return 'Rp. ' . number_format($this->amount, 0, '', '.') . ',00';
        }
    }

    public function order()
    {
    return $this->hasMany('App\Order');
    }
    
    
}
