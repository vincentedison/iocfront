<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\News;
use Illuminate\Support\Facades\View;
use Exception;


use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            $client = new Client(['base_uri'=>'https://www.google.com']);
            $secret = env('RECAPTCHA_SECRET', '');
            $response = $client->post('recaptcha/api/siteverify', ['form_params' => [
                'secret' => $secret,
                'response' => $value
            ]]);
            if($response->getStatusCode() == 200)
            {
                $result = json_decode($response->getBody());
                return $result->success;
            }
            return true;
        });

        Schema::defaultStringLength(191);
        Relation::morphMap(['Facility'=>'App\Facility','EducationTour'=>'App\EducationTour',]);
        try {
            $latest = News::orderBy("id","DESC")->limit(3)->get();
            View::share('latest',$latest);
        } catch(Exception $ex) {
            View::share('latest',[]);
        }
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
