<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CustomPrice extends Model
{
    protected $dates = ['startdate','enddate'];
    public function facility()
    {
        return $this->belongsTo('App\Facility', 'facility_id');
    }
    public function getCarbonstartAttribute()
    {
        $startdate = Carbon::parse($this->startdate)->format('d/m/Y H:i:s');
        return $startdate;
    }
    public function getCarbonendAttribute()
    {
        $enddate = Carbon::parse($this->enddate)->format('d/m/Y H:i:s');
        return $enddate;
    }
    protected $appends =['carbonstart','carbonend'];
    protected $guarded =['rangedate'];
    public function Bundle()
    {
    return $this->morphTo();
    }

    public function Addon()
    {
    return $this->morphTo();
    }

    public function getNameLabelAttribute()
    {
        $obj = Addon::find($this->price_id);
        if($this->price_type=="App\\Facility") {
            $obj=Facility::find($this->price_id);        
        }
        else if($this->price_type=="App\\Bundle"){
         $obj = Bundle::find($this->price_id);

        }
        return $obj->name;        
        

    }


}
