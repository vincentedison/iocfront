<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Addon;
use App\Facility;
use App\Bundle;
use App\Folio;
use App\DEtailFacility;
use Carbon\carbon;

class DetailOrder extends Model
{
    protected $guarded = [];
    protected $dates = ['check_in','check_out'];

    public function order()
    {
        return $this->BelongsTo('App\Order');
    }

    public function getpriceLabelAttribute()
    {
        return 'Rp. ' . number_format($this->price, 0, '', '.') . ',00';;
    }

    public function item()
    {
        return $this->morphTo();
    }

    public function getNameLabelAttribute()
    {
        $obj = Addon::find($this->item_id);
        if($this->item_type=="App\Facility") {
            $obj=Facility::find($this->item_id);        
        }
        else if($this->item_type=="App\Bundle"){
         $obj = Bundle::find($this->item_id);

        }
        return $obj->name;        
        
    }
    public function getpersonLabelAttribute(){
        if($this->item_type=="App\\Bundle"){
            $pers= Bundle::where("id",$this->item_id)->first();            
        }
        else{
             $pers= Facility::where("id",$this->item_id)->first();            
        }
        if($this->amount2==0 || $pers->per_person==0) {
            return "-";
        }
        else if($pers->per_person==1 && $this->amount < $pers->minimum){
            return "<s style='color:red'>".$this->amount2."</s> ".$pers->minimum;
        }
        return $this->amount2;
    }

    public function gettotalLabelAttribute()
    {
        $prc = $this->price*$this->amount;
        if($this->amount2!=null) {
            $prc=$prc*$this->amount2;
        }
        $prc = $prc - $prc*($this->disc/100);
        return $prc;
    }

    public function getselectLabelAttribute()
    {
        return $this->id." - ".$this->namelabel;
    }


    public function getstatusLabelAttribute()
    {   
        $text=["Booked","Check In","Check Out","Canceled"];

        return $text[$this->status];
    }

    public function getdurationLabelAttribute()
    {
        $cin = Folio::where("detail_order_id", $this->id)->where("folio_type", "LIKE", "%Facility")->orderBy("check_in", "ASC")->first();        
        if($cin)
            return $cin->check_in->diffInDays($cin->check_out);
        return $this->amount;
    }

    public function getidHelperLabelAttribute()
    {
        if($this->item_type=="App\\Facility") {
            return "F-".$this->item_id;   
        }
        else if($this->item_type=="App\\Bundle"){
            return "B-".$this->item_id;               
        }
        return "A-".$this->item_id;
    }

    public function getcheckinLabelAttribute(){
        // return $this->id;
       $cin = Folio::where("detail_order_id", $this->id)->where("folio_type", "LIKE", "%Facility")->orderBy("check_in", "ASC")->first();
        return $cin->check_in;
    }
    public function getcheckoutLabelAttribute(){
       $cin = Folio::where("detail_order_id", $this->id)->where("folio_type", "LIKE", "%Facility")->orderBy("check_out", "DESC")->first();
        return $cin->check_out;
    }

}
