<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }
    //

    public function Order()
    {
        return $this->hasMany('App\Order');
    }

    public function customer_type()
    {
        return $this->BelongsTo('App\CustomerType');
    }
    public function getNameLabelAttribute()
    {
        if($this->company_name=="")
            return $this->name;
        return $this->company_name;
    }

    protected $fillable = [
    'name', 'email', 'password','phone','category','customer_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

}
