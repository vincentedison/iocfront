<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function category()
    {
        return $this->belongsToMany('App\Category');
    }

    public function getStatusNameAttribute()
    {
        if($this->status == 1) {
            return "Publish";
        }
        else if($this->status == 2) {
            return "UnPublish";
        }
    }

    public function getcutDescLabelAttribute()
    {
        $txt = strip_tags($this->content);
        $txt=str_replace("&nbsp;", '', $txt);
        return substr($txt,0,120) ;
    }

    public function getcategoryLabelAttribute()
    {
        $cats =$this->category->pluck("name");
        foreach($cats as $category)
        {
            $new_arr[] = $category;
        }
        return implode(',',$new_arr);
    //    return implode(", ",);
    }
    protected $appends =['status_name'];


    protected $guarded =[];
}
