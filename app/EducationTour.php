<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationTour extends Model
{
    protected $guarded=[];
    public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleriable');
    }

    	public function getPathLabelAttribute()
    {
        if ($this->gallery->count() == 0) {
            return '/image/default-picture.png';
        }
        return '/image/'.$this->gallery[0]->image_path;
    }
}

