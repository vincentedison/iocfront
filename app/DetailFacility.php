<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CustomSchedule;
use Carbon\carbon;
use App\Folio;

class DetailFacility extends Model
{

    protected $hidden = [
        'id','facility_id','created_at','updated_at'
    ];

    public function getnameLabelAttribute()
    {
        return $this->facility->name." - ".$this->name;
    }
    public function facility()
    {
        return $this->belongsTo('App\Facility', 'facility_id');
    }
    public function customSchedule()
    {
        return $this->hasMany('App\CustomSchedule', 'detail_facility_id');
    }
    protected $guarded = [];

    public function folio()
    {
        return $this->morphMany('App\Folio', 'folio');
    }
    
    public function getavailLabelAttribute()
    {
        $dfac = DetailFacility::rentable(date("Y-m-d"),1)->where("id",$this->id)->count();
        if($dfac ==0){
            return "danger";
        }
        return "success";
    }
    public function scopeRentable($query,$date1,$dur)
    {
        $folio=Folio::booked(carbon::createFromFormat("Y-m-d",$date1)->format("Y-m-d"), carbon::createFromFormat("Y-m-d",$date1)->addDays($dur)->format("Y-m-d"))->pluck("folio_id");
        $cs=CustomSchedule::booked(carbon::createFromFormat("Y-m-d",$date1)->format("Y-m-d"), carbon::createFromFormat("Y-m-d",$date1)->addDays($dur)->format("Y-m-d"))->pluck("detail_facility_id");
        return $query->wherenotIn("id",$folio)->wherenotIn("id",$cs);
    }
}
