<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailFacility;
use Carbon\carbon;
use App\CustomPrice;
use App\SpecialPrice;
class Facility extends Model
{
    protected $hidden = [
        'id','slug','link_embed', 'type_id','front_end','back_end','unit_name','code','created_at','updated_at','weekend_price','no_stay_weekend_price','no_stay_weekday_price','per_person'
    ];

    public function getidMorphAttribute()
    {
        return "F-".$this->id;
    }

   public function scopeActive($query)
   {
       return $query->where('back_end',1)->where('front_end',1);
   }
 

    public function gallery()
    {
        return $this->morphMany('App\Gallery', 'galleriable');
    }

    public function facilityType()
    {
        return $this->belongsTo('App\FacilityType', 'type_id');
    }
    public function facilityDetail()
    {
        return $this->hasMany('App\FacilityDetail', 'facility_id');
    }
    public function detailFacility()
    {
        return $this->hasMany('App\DetailFacility', 'facility_id');
    }
    public function special_price()
    {
    return $this->morphMany('App\SpecialPrice', 'special');
    }

    public function custom_price()
    {
    return $this->morphMany('App\CustomPrice', 'price');
    }

    
    protected $guarded =[];
    public function getunitLabelAttribute()
    {
        if($this->unit==1) {
            return "jam";
        }
        return "hari";
    }
    public function getUnitNameAttribute()
    {
        if($this->unit==1) {
            return "Jam";

        }
        else if($this->unit==2) {
            return "Hari";

        }
    }

        //Return array of prices with amount
    public function calDetPrice($date,$dur,$person=1,$disc=0,$stay=0,$custype=0){
        $arra=[];
        $start= Carbon::createFromFormat("Y-m-d",$date);
        $total=0;
        for($i=0;$i<$dur;$i++){
            $curdate = $start->format("Y-m-d");
            $json = json_encode($this->todayDetPrice($curdate,$stay,$custype));
            if(empty($arra[$json])){
                $arra=array_add($arra,$json,0);
            }
            $arra[$json]+=1;
            $start->addDays(1);
        }
        return $arra;
    }

     //Return price and name
    public function todayDetPrice($date,$stay=0,$custtype=0){
        $dt = Carbon::createFromFormat("Y-m-d",$date);
        $prices=0;
        $dayiso = $dt->format('N');
       
        $cusp = SpecialPrice::where("special_id",$this->id)->where("special_type","LIKE","%Facility")->get();
        if(count($cusp) > 0){
            foreach($cusp as $cus){
                if($cus->customer_type_id==$custtype){
                    if($stay==0){
                        if($dayiso >= 5)        
                        {return ["Special Price No Stay Weekend",$cus->no_stay_weekend_price];}
                        else        
                        {return ["Special Price No Stay Weekday",$cus->no_stay_weekday_price];}
                    }
                    else if($stay==1){
                        if($dayiso >= 5 )        
                            {return ["Special Price Weekend",$cus->weekend_price];}
                        else        
                            {return ["Special Price Weekday",$cus->price];}
                    }
                }
            }
        }
        if($stay==0){
            if($dayiso >= 5 )        
                {return ["No Stay Weekend",$this->no_stay_weekend_price];}
            else        
                {return ["No Stay Weekday",$this->no_stay_weekday_price];}
        }
        if(($dayiso >= 5) && $this->weekend_price!=null)        
                {return ["Weekend",$this->weekend_price]; }
        return ["Weekday",$this->price];
         $cusp = CustomPrice::whereDate("startdate","<=",$date)->whereDate("enddate",">=",$date)->where("price_id",$this->id)->where("price_type","LIKE","%Facility")->first();
        if($cusp)
            return ["Custom Price",$cusp->price];
    }

    

    public function calPrice($date,$dur,$person=1,$disc=0,$stay=1,$custype=0){
        // return $custype;
        $start= Carbon::createFromFormat("Y-m-d",$date);
        // $end = $start->addDays($dur);
        $total=0;
        // $vl=[];
        for($i=0;$i<$dur;$i++){
            $curdate = $start->addDays(1)->format("Y-m-d");
            $total += $this->todayPrice($curdate,$stay,$custype);
            // $vl=array_add($vl,$curdate,$this->todayPrice($curdate));
        }
        // return $vl;
        if($this->per_person==1 && $this->minimum>$person){
            $person = $this->minimum;
        }
        if($this->per_person==1)
            $total *=$person;                  
        $total -= $total * $disc/100;
        return $total;
    }

    public function todayPrice($date,$stay=0,$custtype=0){
        $dt = Carbon::createFromFormat("Y-m-d",$date);
        $cusp = CustomPrice::whereDate("startdate","<=",$date)->whereDate("enddate",">=",$date)->where("price_id",$this->id)->where("price_type","LIKE","%Facility")->first();
        if($cusp)
            return $cusp->price;
        $cusp = SpecialPrice::where("special_id",$this->id)->where("special_type","LIKE","%Facility")->get();
        if(count($cusp) > 0){
            foreach($cusp as $cus){
                if($cus->customer_type_id==$custtype){
                    if($stay==0){
                        if($dt->isWeekend())        
                        {return $cus->no_stay_weekend_price;}
                        else        
                        {return $cus->no_stay_weekday_price;}
                    }
                    else if($stay==1){
                        if($dt->isWeekend())        
                            {return $cus->weekend_price;}
                        else        
                            {return $cus->price;}
                    }
                }
            }
        }
                if($stay==0){
                    if($dt->isWeekend())        
                        {return $this->no_stay_weekend_price;}
                    else        
                        {return $this->no_stay_weekday_price;}
                }
        if($dt->isWeekend() && $this->weekend_price!=null)        
                {return $this->weekend_price;}
        return $this->price;
    }
    public function Available($date,$dur){
        $det = DetailFacility::where("facility_id",$this->id)->rentable($date,$dur)->get();
        return $det;
    }

    protected $appends =['unit_name','villa_image','id_morph'];

    public function detail_order()
    {
        return $this->morphMany('App\DetailOrder', 'item');
    }
    public function bundle_detail()
    {
        return $this->morphMany('App\BundleDetail', 'bundl');
    }

    public function coupon_facility()
    {
    return $this->morphMany('App\CouponFacility', 'facs');
	}
	
	public function getVillaImageAttribute()
    {
        if ($this->gallery->count() == 0) {
            return '/image/default-picture.png';
        }
        return '/image/'.$this->gallery[0]->image_path;
    }
	public function getlinkEmbedSrcLabelAttribute()
	{
		if($this->link_embed)
			return explode('"',explode(" ",$this->link_embed)[1])[1];
		return "";
	}
}
