<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model
{
    public function coupon_customer_type()
    {
        return $this->hasMany('App\CouponCustomerType');
    }
    public function customer()
    {
        return $this->hasMany('App\Customer');
    }
    protected $guarded=[];

    public function special_price()
    {
    return $this->hasMany('App\SpecialPrice');
    }
}
