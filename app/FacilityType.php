<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityType extends Model
{
    public function facility()
    {
        return $this->hasMany('App\Facility', 'type_id');
    }
    protected $guarded =[];

    public function coupon_facility_type()
    {
        return $this->hasMany('App\CouponFacilityType');
    }

    public function getTypeLabelAttribute()
    {
        $types = ["","Room","Facility"];
    return $types[$this->type];
    }
}
