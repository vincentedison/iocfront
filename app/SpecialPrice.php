<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecialPrice extends Model
{
    protected $guarded =[];
    public function getNameLabelAttribute()
    {
        $obj = Addon::find($this->special_id);
        if($this->special_type=="App\\Facility") {
            $obj=Facility::find($this->special_id);        
        }
        else if($this->special_type=="App\\Bundle"){
         $obj = Bundle::find($this->special_id);
        }
        return $obj->name;        
    }

    public function Bundle()
    {
    return $this->morphTo();
    }

    public function Addon()
    {
    return $this->morphTo();
    }
    public function Facility()
    {
    return $this->morphTo();
    }

    public function customer_type()
    {
    return $this->BelongsTo('App\CustomerType');
    }
}
