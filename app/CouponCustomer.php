<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCustomer extends Model
{
    public function coupon()
    {
        return $this->BelongsTo('App\Coupon');
    }
    public function customer()
    {
        return $this->BelongsTo('App\Customer');
    }
}
