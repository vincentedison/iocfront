<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailFacility;
use App\Addon;
class BundleDetail extends Model
{
    protected $hidden = [
        'id','bundl_id','bundl_type','created_at','updated_at','bundle_id'
    ]; 
    protected $appends =['name_label'];
    
    
    public function bundle()
    {
        return $this->BelongsTo('App\Bundle');
    }


    public function bundl()
    {
        return $this->morphTo();
    }

    public function getNameLabelAttribute(){
        $obj = Addon::find($this->bundl_id);
        if($this->bundl_type=="App\Facility") {
            $obj=Facility::find($this->bundl_id);
            return $obj->name;        
        }
        else{
            return $obj->name;        
        }
    }

    public function gettypeLabelAttribute(){
        if($this->bundl_type=="App\Facility") {
            return "Fasilitas";
        }
        else{
            return "Add on";        
        }

    }

    public function getidHelperLabelAttribute()
    {
        if($this->bundl_type=="App\\Facility") {
            return "F-".$this->bundl_id;   
        }
        return "A-".$this->bundl_id;
    }
}
