<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});
Route::resource('/home','HomeController');
// Route::resource('/villas','VillaController');

Route::Group(
    ['middleware'=>'auth'], function () {
        Route::resource('/orders','OrderController');  
        Route::resource('/addon','AddonController');
        Route::get('/doku/payment/{invoice}', 'DokuController@payment')->name('doku.payment');
    }
);

Route::post('/orders/doku/notify', 'DokuController@notify')->name('doku.notify');
Route::post('/orders/doku/redirect', 'DokuController@redirect')->name('doku.redirect');
Route::post('/orders/cart', 'OrderController@cart')->name('orders.cart');


Route::resource('/education_tours','EducationTourController');
Route::resource('/facilities','FacilityController');
Route::get('/booknow','OrderController@booknow')->name('orders.booknow');
Route::get('/meeting_room','FacilityController@meeting')->name('facilities.meeting');
Route::get('/camping_area','FacilityController@camping')->name('facilities.camping');
Route::get('/bundles/{id}','FacilityController@bundle')->name('bundles.show');
Route::get('login_out',function(){
    Auth::logout();
    return redirect('home');
})->name('logging');
Auth::routes();

// Route::get('/cus','HomeController@load_cus')->name('index.cus');
Route::get('/update','HomeController@load_cus')->name('load.cus');
Route::get('/bundle','FacilityController@bundle_index')->name('bundle.index');
Route::post('/update','HomeController@upd_cus')->name('upd.cus');

Route::post('/password','HomeController@upd_pass')->name('upd.pass');

Route::get('/password', function() {
    return view('profile._password');    
})->name('password');

Route::get('profile', function() {
    return view('profile.index');
})->name('profile');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/villas/{type}/show', 'VillaController@show')->name('villas.show');
Route::resource('/news','NewsController');
Route::get('/swimming_pool','HomeController@swimming_pool')->name('home.swimming_pool');

//Activies
Route::get('/activities','HomeController@activities')->name('activities');
Route::get('/{type}', 'VillaController@index')->name('villas.index');
